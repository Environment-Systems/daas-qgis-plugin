# -*- coding: utf-8 -*-
"""
/****************************************************************************
 This file is part of EnvSys plugin for QGIS
 Integration with Environment Systems Data Services.
                             -------------------
        begin                : 2017-12-12
        copyright            : (C) 2017 by Environment Systems Ltd.
        email                : dataservices@envsys.co.uk
        git sha              : $Format:%H$
 ****************************************************************************/

/****************************************************************************
 *                                                                          *
 *   EnvSys plugin is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 3 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   EnvSys plugin is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
 *   GNU General Public License for more details.                           *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with EnvSys plugin. If not, see <https://www.gnu.org/licenses/>. *
 *                                                                          *
 ****************************************************************************/
"""


import os

from PyQt4 import uic
from PyQt4.QtCore import pyqtSlot
from PyQt4.QtGui import QWidget

# from log import Log
# debug = Log('Test')


class DevWidget(QWidget):
    def __init__(self, utils,  parent=None):
        """ Constructor.
        :param utils: Common utilities object giving access to network calls etc.
        :param parent: Widget that this widget is a child of.
        """
        QWidget.__init__(self, parent)
        self.ui = uic.loadUi(os.path.expanduser(os.path.join(os.path.dirname(__file__), 'dev.ui')), self)
        self.utils = utils

    # noinspection PyPep8Naming
    @pyqtSlot()
    def on_pushButton_pressed(self):
        """ Test button. """
        # self.main_dlg.show_message(tr('Button pushed.'))
        pass

    # noinspection PyMethodMayBeStatic
    def help(self):
        """ Show help on this widget. """
        dlg = self.parent().parent().parent().parent()
        return dlg.show_help('Dev help', 'dev.html')    # Doesn't actually exist.
