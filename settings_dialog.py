# -*- coding: utf-8 -*-
"""
/****************************************************************************
 This file is part of EnvSys plugin for QGIS
 Integration with Environment Systems Data Services.
                             -------------------
        begin                : 2017-12-12
        copyright            : (C) 2017 by Environment Systems Ltd.
        email                : dataservices@envsys.co.uk
        git sha              : $Format:%H$
 ****************************************************************************/

/****************************************************************************
 *                                                                          *
 *   EnvSys plugin is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 3 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   EnvSys plugin is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
 *   GNU General Public License for more details.                           *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with EnvSys plugin. If not, see <https://www.gnu.org/licenses/>. *
 *                                                                          *
 ****************************************************************************/
"""


from locale import getlocale
from ConfigParser import SafeConfigParser

from qgis.gui import QgsMessageBar
from qgis.utils import *
#try:
#    # For testing.
#    from dummy_main_dialog import QgsMessageBarItem
#except:
from qgis.gui import QgsMessageBarItem

from PyQt4 import QtGui, uic
from PyQt4.QtGui import QMessageBox, QDialogButtonBox, QSizePolicy, QProgressBar
from PyQt4.QtCore import pyqtSlot
from translate import tr

from base_dialog import BaseDialog
from settings import SettingsWidget
from search import SearchWidget
from products import ProductsWidget
# from zonal_stats import ZonalStatsWidget
from free import FreeWidget
from utils import Utils

# from log import Log
# debug = Log('Config')

# This translation is used in testing
dummy = tr('Good morning')

FORM_CLASS, _ = uic.loadUiType(os.path.join(os.path.dirname(__file__), 'envsys_dialog_base.ui'))


class SettingsDialog(BaseDialog):

    def __init__(self, utils, parent=None):
        """Constructor."""
        super(SettingsDialog, self).__init__(utils, parent)

        self.plugin_tabs.addTab(SettingsWidget(utils, self.plugin_version), tr('Settings'))

        self.utils = utils
        self.utils.set_settings_dlg(self)

        self.settings_btn.setVisible(False)
