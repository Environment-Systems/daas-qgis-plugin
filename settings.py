# -*- coding: utf-8 -*-
"""
/****************************************************************************
 This file is part of EnvSys plugin for QGIS
 Integration with Environment Systems Data Services.
                             -------------------
        begin                : 2017-12-12
        copyright            : (C) 2017 by Environment Systems Ltd.
        email                : dataservices@envsys.co.uk
        git sha              : $Format:%H$
 ****************************************************************************/

/****************************************************************************
 *                                                                          *
 *   EnvSys plugin is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 3 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   EnvSys plugin is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
 *   GNU General Public License for more details.                           *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with EnvSys plugin. If not, see <https://www.gnu.org/licenses/>. *
 *                                                                          *
 ****************************************************************************/
"""


import os
from locale import getlocale
from time import sleep
import webbrowser
from ConfigParser import NoSectionError, NoOptionError

from PyQt4 import uic, QtCore
from PyQt4.QtCore import Qt, pyqtSlot
from PyQt4.QtGui import QWidget, QMessageBox, QFileDialog, QLineEdit

from translate import tr
from utils import encrypt, decrypt
import envsys_client
from envsys_client import BackscatterRequestInfo, OpticalRequestInfo 

# from log import Log
# debug = Log('Config')


class SettingsWidget(QWidget):
    LOGIN_SECTION = 'Login'     # These don't need translating.
    USERNAME_LABEL = 'username'
    PASSWORD_LABEL = 'password'

    def __init__(self, utils, plugin_version, parent=None):
        """ Constructor.
        :param utils: Common utilities object giving access to network calls etc.
        :param plugin_version: Plugin version from metadata.txt.  Just to save looking it up again.
        :param parent: Widget that this widget is a child of.
        """
        QWidget.__init__(self, parent)
        self.ui = uic.loadUi(os.path.expanduser(os.path.join(os.path.dirname(__file__), 'settings.ui')), self)
        self.utils = utils
        self.load_settings()
        self.username.setText(self.utils.username)
        self.password.setText(self.utils.password)
        self.password.returnPressed.connect(self.save_btn.click)
        self.homepage = self.utils.homepage
        self.sign_up_link.setText("<a href=\"" + self.homepage
                                  + "\signup\" style=\"text-decoration: underline;\">Sign Up</a>")
        self.downloads_dir_lineEdit.setText(self.utils.downloads_dir)
        self.on_see_password_checkBox_stateChanged(Qt.Unchecked)

        self.check_authentication_btn.setAutoDefault(False)
        self.save_btn.setAutoDefault(False)
        self.about_pushButton.setAutoDefault(False)
        self.downloads_dir_btn.setAutoDefault(False)

        locale = getlocale()
        if not locale or locale[0] is None:
            locale = ('en_GB', 'UTF-8')
        base_dir = os.path.dirname(__file__)
        build_info_file = os.path.join(base_dir, 'envsys_help', locale[0], 'build.html')
        with open(build_info_file, 'r') as f:
            build_info = f.read()
            if '\n' == build_info[-1]:
                build_info = build_info[:-1]
            self.build_textEdit.setText(
                tr('Plugin version: {}\n{}').format(plugin_version, build_info)
            )

    def tab_shown(self):
        """ Initialization each time the tab is selected. """
        # Don't allow the username and password to be changed in mid-download!
        self.username.setEnabled(not self.utils.api_busy)
        self.username_label.setEnabled(not self.utils.api_busy)
        self.password.setEnabled(not self.utils.api_busy)
        self.password_label.setEnabled(not self.utils.api_busy)

    def tab_hidden(self):
        """ Tidy up each time the tab is hidden. """
        # Un-necessary, but just to keep things tidy...
        self.username.setEnabled(True)
        self.username_label.setEnabled(True)
        self.password.setEnabled(True)
        self.password_label.setEnabled(True)

    def load_settings(self):
        """ Retrieve saved configuration data. """
        # This info is not compulsory at this stage so don't worry if it isn't present.
        try:
            try:
                self.utils.username = self.utils.config.get(self.LOGIN_SECTION, self.USERNAME_LABEL)
            except NoOptionError:
                pass

            try:
                self.utils.password = self.utils.config.get(self.LOGIN_SECTION, self.PASSWORD_LABEL)
                self.utils.password = decrypt(self.utils.password)
            except NoOptionError:
                pass
        except NoSectionError:
            pass

    # noinspection PyPep8Naming
    @pyqtSlot(str)
    def on_username_textChanged(self, txt):
        """ Handle alterations to the username by saving them to the utils module.
        Don't save these to the config unless specifically requested by the user.
        :param txt: The new username.
        """
        self.utils.username = txt

    # noinspection PyPep8Naming
    @pyqtSlot(str)
    def on_password_textChanged(self, txt):
        """ Handle alterations to the password by saving them to the utils module.
        Don't save these to the config unless specifically requested by the user.
        :param txt: The new password.
        """
        self.utils.password = txt

    @pyqtSlot()
    def on_check_authentication_btn_pressed(self):
        """ Handle 'Check Authentication' button being pressed. """
        self.utils.check_authentication_ok()

    @pyqtSlot()
    def on_save_btn_pressed(self):
        """ Handle 'Save' button being pressed by saving the values from the utils module
        (where we have already stored the data) to the config, and then writing the config.
        """
        if not self.utils.username:
            self.utils.username = ''

        if not self.utils.password:
            self.utils.password = ''

        if self.password:
            if not self.utils.config.has_section(self.LOGIN_SECTION):
                self.utils.config.add_section(self.LOGIN_SECTION)
            self.utils.config.set(self.LOGIN_SECTION, self.USERNAME_LABEL, self.utils.username)
            self.utils.config.set(self.LOGIN_SECTION, self.PASSWORD_LABEL, encrypt(self.utils.password))
            self.utils.save_config()

    # # noinspection PyPep8Naming
    # @pyqtSlot(str)
    # def on_shapes_dir_lineEdit_textChanged(self, txt):
    #     """ Handle changes to the shapes dir by writing them to the
    #     utils module, then to the config, and then writing the config.
    #     """
    #     self.utils.set_shapes_dir(txt)
    #
    # @pyqtSlot()
    # def on_shapes_dir_btn_pressed(self):
    #     """ Handle pressing the shapes dir button by popping up a file dialog and
    #     letting the user select a directory for our vector files.
    #     """
    #     shapes_dir = self.select_dir(tr('Shapes directory'), self.utils.shapes_dir)
    #     if shapes_dir:
    #         self.utils.set_shapes_dir(shapes_dir)
    #         self.shapes_dir_lineEdit.setText(shapes_dir)

    # noinspection PyPep8Naming
    @pyqtSlot(str)
    def on_downloads_dir_lineEdit_textChanged(self, txt):
        """ Handle changes to the shapes dir by writing them to the
                utils module, then to the config, and then writing the config.
        :param txt:  The new downloads dir path.
        """
        self.utils.set_downloads_dir(txt)

    @pyqtSlot()
    def on_downloads_dir_btn_pressed(self):
        """ Handle pressing the downloads dir button by popping up a file dialog and
        letting the user select a directory for our product files.
        """
        downloads_dir = self.select_dir(tr('Downloads directory'), self.utils.downloads_dir)
        if downloads_dir:
            self.utils.set_shapes_dir(downloads_dir)
            self.utils.set_downloads_dir(downloads_dir)
            self.downloads_dir_lineEdit.setText(downloads_dir)

    # noinspection PyMethodMayBeStatic
    def select_dir(self, title, initial_dir):
        """ Open a dialog to allow user-selection of a directory.
        :param title: Dialog title.  A clue to the user what the dialog is for.
        :param initial_dir: The directory that the dialog will show when first opened.
        :return: The selected directory, or None.
        """
        dlg = QFileDialog()
        dlg.setWindowTitle(title)   # The constructor and getExistingDirectory() don't accept unicode.  This does.
        dlg.setDirectory(initial_dir)
        dlg.setFileMode(QFileDialog.Directory)
        dlg.setOptions(QFileDialog.ShowDirsOnly | QFileDialog.DontResolveSymlinks)
        dlg.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        if dlg.exec_():
            dir_list = dlg.selectedFiles()
            if dir_list:
                return dir_list[0]
        return None

    # noinspection PyPep8Naming
    @pyqtSlot(int)
    def on_see_password_checkBox_stateChanged(self, state):
        """ Change the echo mode of the password box so that the user can see what they are typing is desired.
        :param state: The new state of the checkbox, indicating whether to hide the password or not.
        """
        if Qt.Checked == state:
            self.password.setEchoMode(QLineEdit.PasswordEchoOnEdit)
        else:
            self.password.setEchoMode(QLineEdit.Password)

    # noinspection PyPep8Naming
    def on_password_editingFinished(self):
        self.see_password_checkBox.setChecked(False)

    # noinspection PyPep8Naming
    def on_about_pushButton_pressed(self):
        """ Show T&Cs for the plugin. """
        # First. try to open a proper browser because the rendering will be better than in our help message box.
        locale = getlocale()
        if not locale or locale[0] is None:
            locale = ('en_GB', 'UTF-8')
        base_dir = os.path.dirname(__file__)
        src_file = os.path.join(base_dir, 'envsys_help', locale[0], 'about.html')
        dlg = self.parent().parent().parent().parent()
        dlg.show_message(tr('Opening information in your default browser.'), duration=3)
        sleep(0.01)
        if not webbrowser.open(src_file):
            # If that fails for any reason, fall back to the simple help message box.
            dlg.show_help(tr('About EnvSys plugin'), 'about.html')

    def on_sign_up_link_linkActivated(self):
        webbrowser.open(self.utils.homepage + "\signup")

    # noinspection PyMethodMayBeStatic
    def help(self, question=False):
        """ Show help on this widget.
        :param question: Do we have to show help for the whole page (default) or
        are we checking whether the user really dows want to save the username and password?
        """
        dlg = self.parent().parent().parent().parent()
        if question:
            return dlg.show_help(tr('Authentication help'), 'authentication.html', QMessageBox.Save | QMessageBox.Abort)
        else:
            return dlg.show_help(tr('Settings help'), 'config.html')
