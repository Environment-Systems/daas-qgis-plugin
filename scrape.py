# -*- coding: utf-8 -*-
"""
/****************************************************************************
 This file is part of EnvSys plugin for QGIS
 Integration with Environment Systems Data Services.
                             -------------------
        begin                : 2017-12-12
        copyright            : (C) 2017 by Environment Systems Ltd.
        email                : dataservices@envsys.co.uk
        git sha              : $Format:%H$
 ****************************************************************************/

/****************************************************************************
 *                                                                          *
 *   EnvSys plugin is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 3 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   EnvSys plugin is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
 *   GNU General Public License for more details.                           *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with EnvSys plugin. If not, see <https://www.gnu.org/licenses/>. *
 *                                                                          *
 ****************************************************************************/
"""


import os
import math
from requests import session
from datetime import datetime, timedelta
import base64, pytz
from HTMLParser import HTMLParser

from qgis.gui import QgsMessageBarItem

from PyQt4.QtCore import QObject, pyqtSignal     # , pyqtSlot
from PyQt4.QtGui import QApplication

# from log import Log
# debug = Log('Scrape')


class MapHTMLParser(HTMLParser):
    def __init__(self):
        """ Constructor. """
        HTMLParser.__init__(self)
        self.in_script = False
        self.csrfmiddlewaretoken = ''
        self.layer_names = []

    def extract_layer_names(self, data):
        """ Extract layer names from part of the map java script on the /portal/free page.
        :param data: 
            Sample text for what we are trying to parse:
                ...
                var cimap = new FreeCIMap(
                'portal-free-ci',
                { 'center': [52.4,-4.08], 'zoom': 8 },
                {   # This bit would all be on one line.  Not quite JSON.
                  'Summer 16':          L.tileLayer.wms('https://data.envsys.co.uk/geoserver/topmets/wms?',     { layers: 'topmets:jul16',              attribution:'(c) European Union, 1995-2017', name:'Summer 16' }) ,
                  'Autumn 16':          L.tileLayer.wms('https://data.envsys.co.uk/geoserver/topmets/wms?',     { layers: 'topmets:sep16',              attribution:'(c) European Union, 1995-2017', name:'Autumn 16' }) ,
                  'Winter 16':          L.tileLayer.wms('https://data.envsys.co.uk/geoserver/topmets/wms?',     { layers: 'topmets:dec16',              attribution:'(c) European Union, 1995-2017', name:'Winter 16' }) ,
                  'Spring 17':          L.tileLayer.wms('https://data.envsys.co.uk/geoserver/topmets/wms?',     { layers: 'topmets:apr17',              attribution:'(c) European Union, 1995-2017', name:'Spring 17' }) ,
                  'Optical RGB 2016':   L.tileLayer.wms('https://data.envsys.co.uk/geoserver/topmets/wms?',     { layers: 'topmets:optical_rgb_2016',   attribution:'',                              name:'Optical RGB 2016' }) ,
                  'Optical NDVI 2016':  L.tileLayer.wms('https://data.envsys.co.uk/geoserver/topmets/wms?',     { layers: 'topmets:optical_ndvi_2016',  attribution:'',                              name:'Optical NDVI 2016' }) ,
                  'Optical EVI 2016':   L.tileLayer.wms('https://data.envsys.co.uk/geoserver/topmets/wms?',     { layers: 'topmets:optical_evi_2016',   attribution:'',                              name:'Optical EVI 2016' }) ,
                  'Optical NRI 2016':   L.tileLayer.wms('https://data.envsys.co.uk/geoserver/topmets/wms?',     { layers: 'topmets:optical_nri_2016',   attribution:'',                              name:'Optical NRI 2016' }) ,
                  'Optical OSAVI 2016': L.tileLayer.wms('https://data.envsys.co.uk/geoserver/topmets/wms?',     { layers: 'topmets:optical_osavi_2016', attribution:'',                              name:'Optical OSAVI 2016' }) ,
                  'OpenStreetMap':      L.tileLayer.wms('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',   { layers: '',                           attribution:'&copy; OpenStreetMap contributors', name:'OpenStreetMap' })
                }                                                                                                                  ^^^^^^^^^^^^^^^^^^
                ...                                                                                                             This is the bit we want.
        """
        lines = data.split('\n')
        line_num = 1
        for line in lines:
            if 'FreeCIMap' in line:
                line_num = line_num + 2
                break
            line_num += 1
        if line_num < len(lines):
            layer_def = lines[line_num]
            frags = layer_def.split(',')
            self.layer_names = []
            for fragment in frags:
                if 'layer' in fragment:
                    try:
                        layers_index = fragment.index(':')
                        topmets_index = fragment.index(':', layers_index + 1)
                        layer_name = fragment[topmets_index + 1:-1]
                        if layer_name:
                            self.layer_names.append(layer_name)
                            # Download from uppercase version but write to original lowercase version.
                    except ValueError:
                        # If we don't find two ':'s, ignore the line.
                        pass

    def handle_starttag(self, tag, attrs):
        """ Handle the start-tags for all elements in the HTML for the portal/free page.
        We are looking for an <input> tag called 'csrfmiddlewaretoken', and noting when we are in a java script.
        :param tag: The tag that has just been started.
        :param attrs: The attributes declared in the start tag.
        """
        if 'input' == tag:
            token = False
            for attr in attrs:
                if 'name' == attr[0] and 'csrfmiddlewaretoken' == attr[1]:
                    token = True
                if token and 'value' == attr[0]:
                    self.csrfmiddlewaretoken = attr[1]
                    break
        elif 'script' == tag:
            self.in_script = True

    def handle_endtag(self, tag):
        """ Handle the end-tags for all elements in the HTML for the portal/free page.
        :param tag: The tag that has just closed.
        We are just noting when we have come to the end of a java script.
        """
        if 'script' == tag:
            self.in_script = False

    def handle_data(self, data):
        """ Handle the content of an HTML element.
        :param data: The content of the current HTML element.
        We are only interested in looking at scripts for FreeCIMap info.
        """
        if self.in_script:
            if 'FreeCIMap' in data:
                self.extract_layer_names(data)


class ScrapeTool(QObject):
    # Based on QObject because we need to be able to emit signals.

    ROOT_URL = 'https://data.envsys.co.uk/'
    LOGIN_URL    = ROOT_URL + 'login/'
    LOGOUT_URL   = ROOT_URL + 'logout/'
    FREE_URL     = ROOT_URL + 'portal/free/'
    DOWNLOAD_URL = ROOT_URL + 'portal/free/download/{}/{}/'

    progress_signal = pyqtSignal(QgsMessageBarItem, int, int)
    cancel_signal = pyqtSignal(QgsMessageBarItem)

    def __init__(self, downloads_dir, parent=None):
        """ Constructor. """
        QObject.__init__(self, parent)
        self.downloads_dir = downloads_dir
        self.abort = False
        self.session = None
        self.layer_names = []
        self.message_widget = None

    # def __del__(self):
    #     debug.log('del {}'.format(self))

    def login(self, username, password):
        """ Log in to the server as if we are using a browser.
        Utils wouldn't know how to cancel this so if the user tries to
        cancel it will terminate the thread after a couple of seconds.
        (By which time it will probably complete anyway.)
        :param username: Username
        :param password: Password
        """
        self.abort = False
        self.progress_signal.emit(self.message_widget, 0, 3)
        # QApplication.processEvents()  # Give the message a chance to appear.
        self.session = session()
        # ------------------------------------------------------
        self.session.headers.update({
            'referer': self.ROOT_URL,
        })
        r = self.session.get(self.LOGIN_URL)                    # <<<<<<<<<<
        if self.abort:
            return
        # Session csrftoken returned in self.session.cookies
        # Django csrfmiddlewaretoken returned in a hidden input in the form.
        if 'csrfmiddlewaretoken' in r.content:
            parser = MapHTMLParser()
            parser.feed(r.content)
            if parser.csrfmiddlewaretoken:
                # ----------------------------------------------
                self.progress_signal.emit(self.message_widget, 1, 3)
                # QApplication.processEvents()  # Give the message a chance to appear.
                self.session.headers.update({
                    'referer': self.LOGIN_URL,
                })
                payload = {
                    'action': 'login',
                    'username': username,
                    'password': password,
                    'csrfmiddlewaretoken': parser.csrfmiddlewaretoken,
                }
                try:
                    response = self.session.post(self.LOGIN_URL, data=payload, allow_redirects=True)   # <<<<<<<<<<
                except:
                    raise
                if self.abort:
                    return
                if 'sessionid' in self.session.cookies:
                    # ------------------------------------------
                    self.progress_signal.emit(self.message_widget, 2, 3)
                    # QApplication.processEvents()  # Give the message a chance to appear.
                    self.session.headers.update({
                        'referer': self.LOGIN_URL,
                    })
                    r = self.session.get(self.FREE_URL)         # <<<<<<<<<<
                    # No point aborting at this stage.
                    parser.feed(r.content)
                    self.layer_names = parser.layer_names
                    parser = None
                else:
                    raise Exception
        # No point sending a progress update when we are about to remove the message anyway.

    def logout(self):
        """ Log out of the website.
        We can't guarantee to actually do this.
        """
        self.abort = False
        self.session.get(self.LOGOUT_URL)

    def download(self, layer_name, tile_ident):
        """ Download a composite image zipfile.
        :param layer_name: name of the layer, as extracted from the portal/free page.
        :param tile_ident: Tile identifier as extracted from the UK grid shapefile.
        """
        self.abort = False
        url = self.DOWNLOAD_URL.format(layer_name.upper(), tile_ident)
        r = self.session.get(url, stream=True)
        chunk_size = 1 << 20    # 1MB chunks.  Typical image size about 300MB.
        file_size = int(math.ceil(float(r.headers['Content-Length']) / chunk_size))
        if layer_name.startswith('optical_'):
            zipfile_name = layer_name
        else:
            zipfile_name = 'surf_tex_' + layer_name
        zipfile_name = os.path.expanduser(os.path.join(self.downloads_dir, zipfile_name+'_'+tile_ident+'.zip'))
        chunk_number = 1
        with open(zipfile_name, 'wb') as f:
            for chunk in r.iter_content(chunk_size=chunk_size):
                if self.abort:
                    break
                if chunk:   # filter out keep-alive chunks
                    self.progress_signal.emit(self.message_widget, chunk_number, file_size)
                    f.write(chunk)
                    chunk_number += 1
        if self.abort:
            os.remove(zipfile_name)
        # No point sending a progress update when we are about to remove the message anyway.
