# coding: utf-8

"""
    ESL Datahub API

    [API overview](https://data.envsys.co.uk/api/) # Environment Systems datahub API for receiving processed analysis-ready radar and optical imagery.

    OpenAPI spec version: 1.0.0
    Contact: api@envsys.co.uk
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class GEOJSONPolygon(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'type': 'str',
        'coordinates': 'str',
        'data': 'str'
    }

    attribute_map = {
        'type': 'type',
        'coordinates': 'coordinates',
        'data': 'data'
    }

    def __init__(self, type=None, coordinates=None, data=None):
        """
        GEOJSONPolygon - a model defined in Swagger
        """

        self._type = None
        self._coordinates = None
        self._data = None

        if type is not None:
          self.type = type
        if coordinates is not None:
          self.coordinates = coordinates
        if data is not None:
          self.data = data

    @property
    def type(self):
        """
        Gets the type of this GEOJSONPolygon.

        :return: The type of this GEOJSONPolygon.
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """
        Sets the type of this GEOJSONPolygon.

        :param type: The type of this GEOJSONPolygon.
        :type: str
        """

        self._type = type

    @property
    def coordinates(self):
        """
        Gets the coordinates of this GEOJSONPolygon.

        :return: The coordinates of this GEOJSONPolygon.
        :rtype: str
        """
        return self._coordinates

    @coordinates.setter
    def coordinates(self, coordinates):
        """
        Sets the coordinates of this GEOJSONPolygon.

        :param coordinates: The coordinates of this GEOJSONPolygon.
        :type: str
        """

        self._coordinates = coordinates

    @property
    def data(self):
        """
        Gets the data of this GEOJSONPolygon.
         { \"type\": \"Feature\", \"bbox\": [-10.0, -10.0, 10.0, 10.0], \"geometry\": { \"type\": \"Polygon\", \"coordinates\": [ [ [-10.0, -10.0], [10.0, -10.0], [10.0, 10.0], [-10.0, -10.0] ] ] } }

        :return: The data of this GEOJSONPolygon.
        :rtype: str
        """
        return self._data

    @data.setter
    def data(self, data):
        """
        Sets the data of this GEOJSONPolygon.
         { \"type\": \"Feature\", \"bbox\": [-10.0, -10.0, 10.0, 10.0], \"geometry\": { \"type\": \"Polygon\", \"coordinates\": [ [ [-10.0, -10.0], [10.0, -10.0], [10.0, 10.0], [-10.0, -10.0] ] ] } }

        :param data: The data of this GEOJSONPolygon.
        :type: str
        """

        self._data = data

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, GEOJSONPolygon):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
