# -*- coding: utf-8 -*-
"""
/****************************************************************************
 This file is part of EnvSys plugin for QGIS
 Integration with Environment Systems Data Services.
                             -------------------
        begin                : 2017-12-12
        copyright            : (C) 2017 by Environment Systems Ltd.
        email                : dataservices@envsys.co.uk
        git sha              : $Format:%H$
 ****************************************************************************/

/****************************************************************************
 *                                                                          *
 *   EnvSys plugin is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 3 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   EnvSys plugin is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
 *   GNU General Public License for more details.                           *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with EnvSys plugin. If not, see <https://www.gnu.org/licenses/>. *
 *                                                                          *
 ****************************************************************************/
"""


import os
from datetime import datetime, timedelta

from PyQt4.QtCore import Qt, QDate
from PyQt4.QtGui import QWidget, QCheckBox, QHBoxLayout, QVBoxLayout, QDialog, QLabel
from PyQt4 import uic

from qgis.core import *
from qgis.utils import iface

from backscatter_config_dialog import BackscatterConfigDialog
from optical_config_dialog import OpticalConfigDialog
from envsys_client import BackscatterRequestInfo, OpticalRequestInfo
from translate import tr

# from log import Log
# debug = Log('Search')


class DatasetSelector(QWidget):
    """ Simple widget that consists of a checkbox and some dataset information. """

    def __init__(self, dataset, group_num=0, parent=None):
        """
        Simple selector widget.
        :param dataset: The dataset dictionary returned from the API (backscatter or optical),
        :param group_num: Optical results are grouped by end-date.  Show groups in different colours.
        :param parent: Widget this one belongs to.
        """
        QWidget.__init__(self, parent)

        self.dataset = dataset
        self.layer = None
        time_end = datetime.strptime(dataset['time_end'], '%Y-%m-%dT%H:%M:%SZ')
        time_end = time_end.strftime('%B %d, %Y, %I:%M %p')
        if 'cloud_cover' in dataset:
            cloud_cover = dataset['cloud_cover'] if dataset['cloud_cover'] else 0
            title = tr('({})  Optical {}\nCloud coverage: {:.0f}%', 'Identifier, time, percentage cloud cover').format(
                dataset['id'], time_end, cloud_cover
            )
        else:
            title = '({}) Backscatter {}'.format(dataset['id'], time_end)
        self.check = QCheckBox(title)
        self.group_num = group_num
        if group_num & 1:  # For odd numbered groups
            dim = 0.9
            colour = self.check.palette().color(self.check.backgroundRole()).getRgb()
            self.check.setStyleSheet(
                'background-color: rgb({},{},{})'.format(colour[0] * dim, colour[1] * dim, colour[2] * dim))
        # Results on the website are shown as
        #   Backscatter Dec. 11, 2017, 6:21 a.m.
        #   Dataset ID: D884
        # or
        #   "Sentinel-2 Dec. 11, 2017, 11:24 a.m.
        #   Done: Sentinel-2: 2017-12-11 11:24:51+00:00
        #   Cloud Coverage: 0%""
        # The id shown on the website is not available.
        layout = QHBoxLayout(self)
        layout.addWidget(self.check)


class SearchWidget(QWidget):
    """ Widget for performing database searches. """
    DATE_FORMAT = 'd/M/yyyy'    # QDate format string, not strftime.
    LOW_VIS_TRANSPARENCY = 80
    HIGH_VIS_TRANSPARENCY = 60

    def __init__(self, utils,  parent=None):
        """ Constructor.
        :param utils: Common utilities object giving access to network calls etc.
        :param parent: Widget that this widget is a child of.
        """
        QWidget.__init__(self, parent)
        self.ui = uic.loadUi(os.path.expanduser(os.path.join(os.path.dirname(__file__), 'search.ui')), self)
        self.utils = utils
        self.time_start_dateEdit.setDate(QDate.currentDate().addDays(-14))
        self.time_end_dateEdit.setDate(QDate.currentDate())

        # 'Nearest' searches have been abandoned.  Components and code left in case they get resurrected.
        self.in_time_range_btn.setChecked(True)
        self.nearest_to_time_btn.hide()
        self.in_time_range_btn.hide()

        self.product_type_comboBox.currentIndexChanged.connect(self.clear_search_results)
        self.region_comboBox.currentIndexChanged.connect(self.clear_search_results)
        self.time_start_dateEdit.dateChanged.connect(self.clear_search_results)
        self.time_end_dateEdit.dateChanged.connect(self.clear_search_results)
        self.on_in_time_range_btn_clicked()

        self.product_type_index = -1
        self.dataset_layer_group = None
        self.layer_list = []
        self.selectors = []
        self.aoi_name = ''  # When we do AOI searches and product requests - i.e. from map extents or layers.
        self.aoi_points = []

        self.log_scale = False
        # OSGB-1936 is first up in the config dialog therefore this should be our default.
        self.projection_epsg = self.utils.WEB_MERCATOR_EPSG
        self.apply_cloud_mask = False

    def tab_shown(self):
        """ Initialization each time the tab is selected. """
        # Update the product type list, the region list, and the layer list.
        self.utils.check_authentication_ok()
        self.list_regions_result(True, None)
        if self.product_type_comboBox.count() == 0:
            self.utils.list_product_types(self.list_product_types_result)

    def tab_hidden(self):
        """ Clear the page each time the tab is hidden. """
        self.clear_search_results()

    def list_product_types_result(self, completed_ok, optical_product_types):
        """ Populate the product-type comboBox.
        :param completed_ok: True for job success.
        :param optical_product_types: The optical product type list if state==True, else None.
        """
        self.product_type_comboBox.clear()

        if completed_ok:
            product_types = [
                {'id': -1, 'name': tr('Backscatter'), 'description': None},
                {'id': -2, 'name': tr('Backscatter with ratio band'), 'description': None},
            ]
            optical_product_types = sorted(optical_product_types, key=lambda lyr: lyr['id'])
            product_types.extend(optical_product_types)
            for product_type in product_types:
                txt = product_type['name']
                product_ident = product_type['id']
                self.product_type_comboBox.addItem(txt, product_ident)
            self.product_type_comboBox.setCurrentIndex(0)
            self.product_type_index = self.product_type_comboBox.itemData(self.product_type_comboBox.currentIndex())

    def list_regions_result(self, completed_ok, regions):
        """ Populate the region combobox with a list of regions and layers.
        :param completed_ok: True for job success.
        :param regions: The region list if state==True, else None.
        """
        self.region_comboBox.clear()

        # Build layer list
        self.layer_list = self.utils.list_vector_layers()
        num_layers = len(self.layer_list)
        for layer_num in range(num_layers):
            # Can't use '0' as the layer_num so always subtract an extra '1'.
            self.region_comboBox.addItem(self.layer_list[layer_num].name(), -layer_num-1)

        # Special case of using the map as the AOI.
        self.region_comboBox.addItem(tr('Current map extent'), 0)

    # noinspection PyMethodMayBeStatic
    def help(self):
        """ Show help on this widget. """
        dlg = self.parent().parent().parent().parent()
        return dlg.show_help(tr('Search help'), 'search.html')

    def clear_search_results(self):
        """ Clear the search results.
        Called whenever the search parameters change.
        """
        layout = self.scrollAreaWidgetContents.layout()
        if layout is None:
            layout = QVBoxLayout()
            self.scrollAreaWidgetContents.setLayout(layout)
        else:
            for idx in range(len(self.selectors)):
                selector = self.selectors[idx]
                try:
                    QgsMapLayerRegistry.instance().removeMapLayer(selector.layer.id())
                except RuntimeError:
                    pass    # Layer has already been deleted by the user.
                selector.layer = None
                layout.removeWidget(selector)
                selector.deleteLater()
                self.selectors[idx] = None
                selector = None
            self.utils.clear_layout(layout)
            if self.dataset_layer_group is not None:
                legend = iface.legendInterface()
                legend.removeGroup(self.dataset_layer_group)
            self.dataset_layer_group = None
        self.selectors = []

        self.select_all_results_checkBox.setCheckState(Qt.Unchecked)
        self.select_all_results_checkBox.setEnabled(False)
        self.search_btn.setText(tr('Search'))
        self.config_btn.setEnabled(False)
        self.generate_btn.setEnabled(False)
        self.generate_btn.setText(tr('Generate products'))

    def on_nearest_to_time_btn_clicked(self):
        """ Handle selection of nearest-to-date searches.
        Button is hidden as these searches don't actually work at present.
        """
        self.clear_search_results()
        self.time_end_label.setEnabled(False)
        self.time_end_dateEdit.setEnabled(False)

    def on_in_time_range_btn_clicked(self):
        """ Handle selection of nearest-to-date searches.
        Button is hidden as this is the only kind of search available.
        """
        self.clear_search_results()
        self.time_end_label.setEnabled(True)
        self.time_end_dateEdit.setEnabled(True)

    def dataset_selected(self, state):
        """ Called when the state of the checkbox in one of the dataset selectors changes.
        Update the generate button to show how many products would be generated.
        :param state: The new state of the checkbox.
        """
        selector = self.sender().parent()
        try:
            if Qt.Checked == state:
                selector.layer.setLayerTransparency(self.HIGH_VIS_TRANSPARENCY)
            else:
                selector.layer.setLayerTransparency(self.LOW_VIS_TRANSPARENCY)
        except RuntimeError:    # Layer has been deleted by the user.
            pass
        iface.mapCanvas().refreshAllLayers()
        self.show_num_datasets_selected()

    def show_num_datasets_selected(self):
        """ Count the number of selected datasets and the number of products that will be generated.
        These will be the same for backscatter products but not necessarily so for optical products.
        """
        if not self.selectors:
            self.select_all_results_checkBox.setCheckState(Qt.Unchecked)
            self.select_all_results_checkBox.setEnabled(False)
            self.search_btn.setText(tr('Search'))
            self.config_btn.setEnabled(False)
            self.generate_btn.setEnabled(False)
            self.generate_btn.setText(tr('Generate products'))
        else:
            self.select_all_results_checkBox.setEnabled(True)
            num_requests = 0
            num_selected = 0
            if 0 <= self.product_type_index:    # Optical
                selected = set()
                for selector in self.selectors:
                    if Qt.Checked == selector.check.checkState():
                        selected.add(selector.group_num)
                        num_selected += 1
                num_requests = len(selected)
            else:                               # Backscatter
                for selector in self.selectors:
                    if Qt.Checked == selector.check.checkState():
                        num_selected += 1
                        num_requests += 1

            all_state = Qt.PartiallyChecked
            if num_selected == len(self.selectors):
                all_state = Qt.Checked
            elif 0 == num_selected:
                all_state = Qt.Unchecked
            self.select_all_results_checkBox.setCheckState(all_state)

            self.search_btn.setText(tr('Search ({})').format(len(self.selectors)))
            self.config_btn.setEnabled(True)
            if num_requests > 0:
                self.generate_btn.setEnabled(True)
                self.generate_btn.setText(tr('Generate {} products').format(num_requests))
            else:
                self.generate_btn.setEnabled(False)
                self.generate_btn.setText(tr('Generate products'))

    def on_search_btn_pressed(self):
        """ Handle pressing the search button, i.e. perform a search. """
        # Any previous results should have been removed before displaying new results, but just to make sure....
        self.clear_search_results()
        args = []
        kwargs = {'ordering': 'time_start'}

        # Use the date_0, date_1, and footprint arguments to filter the holdings presented to the user.
        self.product_type_index = self.product_type_comboBox.itemData(self.product_type_comboBox.currentIndex())
        region_idx = self.region_comboBox.itemData(self.region_comboBox.currentIndex())
        if 0 == region_idx:
            # Map extent
            map_extent = iface.mapCanvas().extent()
            map_epsg = iface.mapCanvas().mapRenderer().destinationCrs().authid()
            dst_srs = QgsCoordinateReferenceSystem(self.utils.SERVER_EPSG)
            src_srs = QgsCoordinateReferenceSystem(map_epsg)
            xform = QgsCoordinateTransform(src_srs, dst_srs)

            pt1 = xform.transform(QgsPoint(map_extent.xMinimum(), map_extent.yMinimum()))
            pt2 = xform.transform(QgsPoint(map_extent.xMinimum(), map_extent.yMaximum()))
            pt3 = xform.transform(QgsPoint(map_extent.xMaximum(), map_extent.yMaximum()))
            pt4 = xform.transform(QgsPoint(map_extent.xMaximum(), map_extent.yMinimum()))

            # We want these coordinates as the footprint in WKT format.
            wkt = 'Polygon(({0} {1},{2} {3},{4} {5},{6} {7},{0} {1}))'.format(
                pt1.x(), pt1.y(),   # 0, 1
                pt2.x(), pt2.y(),   # 2, 3
                pt3.x(), pt3.y(),   # 4, 5
                pt4.x(), pt4.y()    # 6, 7
            )
            kwargs['footprint'] = wkt
            self.aoi_name = 'FromMapExtent'
            self.aoi_points = [
                [pt1.x(), pt1.y()],   # 0, 1
                [pt2.x(), pt2.y()],   # 2, 3
                [pt3.x(), pt3.y()],   # 4, 5
                [pt4.x(), pt4.y()],   # 6, 7
                [pt1.x(), pt1.y()],   # 0, 1
            ]
        elif region_idx < 0:
            # We are using a layer as an AOI.
            region_idx += 1     # Restore the '1' we subtracted when building the list.
            layer = self.layer_list[abs(region_idx)]
            extent = layer.extent()
            xmin = extent.xMinimum()
            ymin = extent.yMinimum()
            xmax = extent.xMaximum()
            ymax = extent.yMaximum()
            hull = [[xmin, ymin], [xmin, ymax], [xmax, ymax], [xmax, ymin]]
            # hull = convex_hull_from_layer(layer)
            assert hull, tr('Unable to detect the convex hull for the selected layer.')

            layer_epsg_code = int(layer.crs().authid()[5:])
            src_srs = QgsCoordinateReferenceSystem(layer_epsg_code)
            dst_srs = QgsCoordinateReferenceSystem(self.utils.SERVER_EPSG)
            transform = QgsCoordinateTransform(src_srs, dst_srs)

            # map_srs = QgsCoordinateReferenceSystem(self.utils.WEB_MERCATOR_EPSG)
            # map_transform = QgsCoordinateTransform(src_srs, map_srs)
            # fred = self.utils.make_layer_from_coords('Fred', [hull], transform=map_transform)
            # QgsMapLayerRegistry.instance().addMapLayer(fred)  # To check our hull construction.

            wkt = 'Polygon(('
            first = True
            first_pt = None
            self.aoi_points = []
            for point in hull:
                pt = transform.transform(QgsPoint(point[0], point[1]))
                self.aoi_points.append([pt.x(), pt.y()])
                if first:
                    first_pt = pt
                    first = False
                wkt += '{} {}, '.format(pt.x(), pt.y())
            assert first_pt, tr('No points found in convex hull for selected layer.')
            wkt += '{} {}))'.format(first_pt.x(), first_pt.y())
            self.aoi_points.append(self.aoi_points[0])
            kwargs['footprint'] = wkt
            self.aoi_name = layer.name()
        else:
            # Simple region id.
            kwargs['region'] = region_idx

        # We always need an end-date.
        kwargs['date_1'] = self.time_end_dateEdit.date().toString(self.DATE_FORMAT)

        if self.in_time_range_btn.isChecked():  # Date range: We need a start date too.
            kwargs['date_0'] = self.time_start_dateEdit.date().toString(self.DATE_FORMAT)

        if self.product_type_index < 0:     # Backscatter
            self.utils.list_backscatter_datasets(args, kwargs, self.search_results)
        else:                               # Optical
            self.utils.list_optical_datasets(args, kwargs, self.search_results)

    def search_results(self, completed_ok, datasets):
        """ Process search results.
        :param completed_ok: Indicates whether the search was completed successfully.
        Even successful searches may return an empty list.
        :param datasets: List of datasets meeting the search parameters.
        """
        layout = self.scrollAreaWidgetContents.layout()
        if completed_ok:
            if datasets:
                self.config_btn.setEnabled(True)

                # Display new results.
                # For optical requests we check for the ability to
                # merge results, and present datasets grouped by end time.
                group_num = 0
                if self.dataset_layer_group is None:
                    legend = iface.legendInterface()
                    assert legend, tr('Couldn\'t find map legend.')
                    self.dataset_layer_group = legend.addGroup('SearchResults')
                    # There doesn't seems to be any way for this to fail, or at least to report an error.
                last_end_date = datasets[0]['time_end']
                for dataset in datasets:
                    if 0 <= self.product_type_index:
                        end_date = dataset['time_end']
                        if last_end_date != end_date:
                            last_end_date = end_date
                            group_num += 1
                    selector = DatasetSelector(dataset, group_num)
                    self.selectors.append(selector)
                    layout.addWidget(selector)
                    selector.check.stateChanged.connect(self.dataset_selected)
                    # Show each dataset as an in-memory layer.
                    assert 'footprint' in dataset, tr('No footprint found in dataset {}'.format(dataset))
                    assert 'coordinates' in dataset['footprint'],   \
                        tr('No coordinates found in footprint for dataset {}'.format(dataset))
                    layer_name = '({}){}'.format(dataset['id'], dataset['time_end'])
                    selector.layer = self.utils.make_layer_from_coords(layer_name, dataset['footprint']['coordinates'])
                    QgsMapLayerRegistry.instance().addMapLayer(selector.layer)
                    legend.moveLayer(selector.layer, self.dataset_layer_group)
                    selector.layer.setLayerTransparency(self.LOW_VIS_TRANSPARENCY)
            else:
                msg = QLabel(tr('No matching datasets were found.'), self.scrollAreaWidgetContents)
                layout.addWidget(msg)

        self.show_num_datasets_selected()

    def on_config_btn_pressed(self):
        """ Handle pressing the configuration button.
        Pops up a dialog showing appropriate options for the selected product type.
        As a side-effect, load all the datazones (backscatter and optical) and the selected datsets onto the map.
        """
        if self.product_type_index < 0:
            config_dlg = BackscatterConfigDialog(self.projection_epsg, self.log_scale)
            if QDialog.Accepted == config_dlg.exec_():
                projection_idx = config_dlg.projection_comboBox.currentIndex()
                self.projection_epsg = config_dlg.projection_comboBox.itemData(projection_idx)
                bandscale_idx = config_dlg.bandscale_comboBox.itemData(config_dlg.bandscale_comboBox.currentIndex())
                self.log_scale = 1 == bandscale_idx
        else:
            config_dlg = OpticalConfigDialog(self.projection_epsg, self.apply_cloud_mask)
            if QDialog.Accepted == config_dlg.exec_():
                projection_idx = config_dlg.projection_comboBox.currentIndex()
                self.projection_epsg = config_dlg.projection_comboBox.itemData(projection_idx)
                self.apply_cloud_mask = Qt.Checked == config_dlg.apply_cloud_mask_checkBox.checkState()
        config_dlg.deleteLater()
        config_dlg = None

    def on_generate_btn_pressed(self):
        """ Handle pressing the generate button. """
        dlg = self.parent().parent().parent().parent()
        if not self.utils.set_authentication():
            dlg.show_message('Unable to set authentication.')
            return

        # Backscatter or optical request?
        if self.product_type_index < 0:
            request_info = BackscatterRequestInfo()
            request_info.ratio_band = -2 == self.product_type_index
            request_info.log_scale = self.log_scale
        else:
            request_info = OpticalRequestInfo()
            request_info.product_type = self.product_type_index
            request_info.apply_cloud_mask = self.apply_cloud_mask

        request_info.projection = self.projection_epsg

        region_idx = self.region_comboBox.itemData(self.region_comboBox.currentIndex())
        if region_idx <= 0:
            geometry = {
                'type': 'Polygon',
                'coordinates': [self.aoi_points]
            }
            geometry = str(geometry).replace("'", '"').replace('(', '[').replace(')', ']')
            request_info.aoi = geometry
            request_info.name = self.aoi_name
        else:
            request_info.region = region_idx

        first = True
        for selector in self.selectors:
            if Qt.Checked == selector.check.checkState():
                if 0 > self.product_type_index:
                    request_info.source_data = selector.dataset['id']
                    self.utils.create_backscatter_request(request_info)
                else:
                    end_date = selector.dataset['time_end']
                    if first:
                        first = False
                        previous_end_date = end_date
                        request_info.datasets = []
                    if previous_end_date != end_date:
                        previous_end_date = end_date
                        self.utils.create_optical_request(request_info)
                        request_info.datasets = []
                    request_info.datasets.append(selector.dataset['id'])
        # If we were making optical requests then the last group of requests is still waiting to be done.
        if 0 <= self.product_type_index and request_info.datasets:
            self.utils.create_optical_request(request_info)

    # noinspection PyPep8Naming
    def on_select_all_results_checkBox_clicked(self):
        """ Called when the user clicks on the 'All results' checkbox.
        Set the state of all of the individual result selector checkboxes.
        """
        initial_state = self.select_all_results_checkBox.checkState()
        state = Qt.Unchecked if Qt.Unchecked == initial_state else Qt.Checked
        if not state == initial_state:
            self.select_all_results_checkBox.setCheckState(state)
        for selector in self.selectors:
            selector.check.setChecked(state)
        self.show_num_datasets_selected()
