# -*- coding: utf-8 -*-
"""
/****************************************************************************
 This file is part of EnvSys plugin for QGIS
 Integration with Environment Systems Data Services.
                             -------------------
        begin                : 2017-12-12
        copyright            : (C) 2017 by Environment Systems Ltd.
        email                : dataservices@envsys.co.uk
        git sha              : $Format:%H$
 ****************************************************************************/

/****************************************************************************
 *                                                                          *
 *   EnvSys plugin is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 3 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   EnvSys plugin is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
 *   GNU General Public License for more details.                           *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with EnvSys plugin. If not, see <https://www.gnu.org/licenses/>. *
 *                                                                          *
 ****************************************************************************/
"""


import os
from PyQt4 import QtGui, uic

FORM_CLASS, _ = uic.loadUiType(os.path.join(os.path.dirname(__file__), 'backscatter_config_dialog.ui'))


class BackscatterConfigDialog(QtGui.QDialog, FORM_CLASS):
    def __init__(self, projection_epsg, log_scale, parent=None):
        """Constructor.
        :param projection_epsg: Default EPSG code. 
        :param log_scale: True for decibel, False for linear.
        """
        super(BackscatterConfigDialog, self).__init__(parent)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://doc.qt.io/qt-5.10/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.setupUi(self)

        # Build list of projections.  Not much of a list but I can't see what projections the datasets use.
        projection = [
            {'id': 27700, 'description': 'OSGB 1936 (Ordnance Survey British National Grid) - UK use'},
            {'id':  3857, 'description': 'Web Mercator (Global use for online maps)'},
            # {'id':  4326, 'description': 'WGS84'},
            # {'id': 32630, 'description': 'WGS84 / UTM zone 30N'},
            # {'id':  4258, 'description': 'European Terrestrial Reference System 1989(ETRS89)'},
        ]
        current_index = 1
        for i in range(len(projection)):
            self.projection_comboBox.addItem('EPSG:{}, {}'.format(
                    projection[i]['id'],
                    projection[i]['description'],
                ),
                projection[i]['id']
            )
            if projection_epsg == projection[i]['id']:
                current_index = i
        self.projection_comboBox.setCurrentIndex(current_index)

        self.bandscale_comboBox.addItem('Linear', 0)
        self.bandscale_comboBox.addItem('Logarithmic', 1)
        current_index = 1 if log_scale else 0
        self.bandscale_comboBox.setCurrentIndex(current_index)