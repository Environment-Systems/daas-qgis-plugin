# -*- coding: utf-8 -*-
"""
/****************************************************************************
 This file is part of EnvSys plugin for QGIS
 Integration with Environment Systems Data Services.
                             -------------------
        begin                : 2017-12-12
        copyright            : (C) 2017 by Environment Systems Ltd.
        email                : dataservices@envsys.co.uk
        git sha              : $Format:%H$
 ****************************************************************************/

/****************************************************************************
 *                                                                          *
 *   EnvSys plugin is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 3 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   EnvSys plugin is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
 *   GNU General Public License for more details.                           *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with EnvSys plugin. If not, see <https://www.gnu.org/licenses/>. *
 *                                                                          *
 ****************************************************************************/
"""


from time import sleep

from PyQt4.QtCore import Qt, QObject, QThread, pyqtSignal, pyqtSlot, QMutex

from qgis.core import QgsMessageLog
from qgis.gui import QgsMessageBarItem

from translate import tr

# from log import Log
# debug = Log('Jobs')

TESTING = False

class WorkThread(QThread):
    progress_signal = pyqtSignal(QgsMessageBarItem, int, int)
    result_signal = pyqtSignal(QThread)

    def __init__(
            self,
            message_widget,
            worker_function,
            args,
            kwargs,
            returns_list,
    ):
        """ Constructor
        :param message_widget: The message bar item where we have a progress bar and a cancel button.
        :param worker_function: The function to be called to perform the task.
        :param args: List of positional arguments for the worker function.
        :param kwargs: Dictionary of keyword arguments for the worker function.
        :param returns_list: True for worker functions which return lists of things.
        """
        QThread.__init__(self)
        self.message_widget = message_widget
        self.worker_function = worker_function
        self.args = args
        self.kwargs = kwargs
        self.returns_list = returns_list

        # Declare all the items that may need to be re-initialised so that PyCharm
        # doesn't flag them as being defined outside the initializer.
        self.abort = False  # Indicates that we have been told to abort.
        self.finished = False
        self.item_list = []
        self.error = None

        self.reinit()  # Call reinit() just to be sure.

    def reinit(self):
        """ Initialization that is common to construction and to re-running the thread. """
        self.abort = False
        self.finished = False
        self.item_list = []
        self.error = None

    # def __del__(self):
    #     """ 'Destructor' just so that we can see when this gets deleted. """
    #     debug.log('del {}({})'.format(self, self.worker_function))

    def run(self):
        """ Perform whatever time-consuming task this thread was created for. """
        try:
            if self.returns_list:
                self.item_list = []
                list_offset = 0
                if 'list_optical_datasets' == self.worker_function.func_name:
                    list_limit = 1
                else:
                    list_limit = 100
                num_items = list_limit
                tmp_item_list = []
                total_num_items = 0
                while num_items == list_limit and not self.abort:
                    self.progress_signal.emit(self.message_widget, list_offset, total_num_items)
                    try:
                        # print('{}({}, {}, {}, {})'.format(self.worker_function, list_offset, list_limit, self.args, self.kwargs))
                        response = self.worker_function(offset=list_offset, limit=list_limit, *self.args, **self.kwargs)
                    except Exception as e:  # Typically ApiException
                        if TESTING:
                            print('#1: ' + str(e))
                        QgsMessageLog.instance().logMessage('1) ' + str(e))
                        self.error = e
                        tmp_item_list = []
                        break

                    total_num_items = response['count']
                    results = response['results']
                    num_items = len(results)
                    tmp_item_list.extend(results)
                    list_offset += num_items
                    if list_offset > total_num_items:
                        break
                self.item_list = tmp_item_list
            else:
                try:
                    # Some worker functions provide their own progress signals.
                    try:
                        self.worker_function.__self__.progress_signal.connect(self.echo_progress_signal)
                    except Exception:
                        # Not all worker functions provide their own progress signals.
                        pass

                    # debug.log('Work: {}({}, {})'.format(self.worker_function, self.args, self.kwargs))
                    response = self.worker_function(*self.args, **self.kwargs)
                    # debug.log('Response: ' + str(response))
                    self.item_list = response
                except Exception as e:  # Typically ApiException
                    if TESTING:
                        print('#2: ' + str(e))
                    QgsMessageLog.instance().logMessage('2) ' + str(e))
                    self.error = e
                    self.item_list = None
        except Exception as e:
            if TESTING:
                print('#3: ' + str(e))
            QgsMessageLog.instance().logMessage('3) ' + str(e))
            self.item_list = None
            self.error = e
        self.finished = True
        self.result_signal.emit(self)   # next_job()

    @pyqtSlot(QgsMessageBarItem, int, int)
    def echo_progress_signal(self, message_widget, val, total):
        """ Substitute the actual message widget for the empty value from scraper tasks.
        :param message_widget: Not used. Expected to be None.
        :param val: How many parts of the task have been completed.
        :param total: How many parts of the task there are in total.
        """
        if TESTING:
            print('echo progress {}, {}, {}'.format(self.message_widget, val, total))
        self.progress_signal.emit(self.message_widget, val, total)


class JobDetails(object):
    def __init__(
            self,
            job_fn,
            message,
            progress_bar_required,
            args,
            kwargs,
            result_is_list,
            result_fn,
            scraper=None,
            ident=None
    ):
        """ Constructor.
        :param job_fn: Defines the type of job that is to be done.
        :param message: Message to be shown in the message widget.
        :param progress_bar_required: True if the worker function generates progress messages.
        :param args: Positional arguments for the job.
        :param kwargs: Keyword arguments for the job
        :param result_is_list: True if the result is a list, False if it is a single item.
        :param result_fn: Slot to be called when the job finishes.
        :param scraper: ScrapeTool to initiate session with, if required.
        :param ident: An identifier to help work out what to do with the result.
        We don't do anything with this other than pass it back with the result.
        """
        self.job_fn = job_fn
        self.message = message
        self.progress_bar_required = progress_bar_required
        self.args = args
        self.kwargs = kwargs
        self.result_is_list = result_is_list
        self.result_fn = result_fn
        self.scraper = scraper
        self.ident = ident

    def __unicode__(self):
        return u'{}, {}, {}, {}, {}, {}, {}, {}, {}'.format(
            self.job_fn,
            self.message,
            self.progress_bar_required,
            self.args,
            self.kwargs,
            self.result_is_list,
            self.result_fn,
            self.scraper,
            self.ident,
        )


class JobQueue(QObject):
    job_complete_signal = pyqtSignal()

    def __init__(self, main_dlg):
        """ Constructor
        :param main_dlg: So that we can connect progress messages.
        """
        super(JobQueue, self).__init__()
        self.main_dlg = main_dlg

        self.job_queue = []                 # While one job is active, further jobs can be queued here.
        self.job_queue_mutex = QMutex()     # Ensure there are no issues with manipulating the queue.
        self.thread = None                  # Thread for performing work.

    def queue_job(
            self,
            job_fn,
            message,
            progress_bar_required,
            args,
            kwargs,
            result_is_list,
            result_fn,
            scraper=None,
            ident=None
    ):
        """ Add a job to the job queue.
        It may be executed right away but may have to wait for the current job to finish first.
        :param job_fn: Defines the type of job that is to be done.
        :param message: Message to be shown in the message widget.  If a tuple, first item is title, second message.
        :param progress_bar_required: True if the worker function generates progress messages.
        :param args: Positional arguments for the job.
        :param kwargs: Keyword arguments for the job
        :param result_is_list: True if the result is a list, False if it is a single item.
        :param result_fn: Slot to be called when the job finishes.
        :param scraper: ScrapeTool to initiate session with, if required.
        :param ident: An identifier to help work out what to do with the result.
        We don't do anything with this other than pass it back with the result.
        """
        job_details = JobDetails(
            job_fn,
            message,
            progress_bar_required,
            args,
            kwargs,
            result_is_list,
            result_fn,
            scraper,
            ident
        )

        self.job_queue_mutex.lock()
        self.job_queue.append(job_details)
        self.job_queue_mutex.unlock()

        if not self.thread:    # Is the job queue busy?
            self.next_job()     # No. Kick-start the first job.

    @pyqtSlot()
    def next_job(self):
        """ Start the first/next job in the queue.
        Called following
            * queueing of the first job in a previously empty queue,
            * a result signal from a thread,
            * a cancel signal.
        Should never be called if we are still busy and haven't been cancelled.
        """
        # At this point we don't know whether job[0] has completed or is yet to be started.
        assert self.job_queue, tr("No job found.")
        # Whether a job has completed or is just to be started, there should always be something in the queue.
        job_details = self.job_queue[0]

        if self.thread:
            assert self.thread.finished, tr('Job is not complete.\n{}').format(self.job_queue[0])
            # job has completed.

            if job_details.result_fn:
                # Pass both these values to the result function regardless of success, abort, or termination.
                completed_ok = not self.thread.abort and not self.thread.error
                if completed_ok:
                    result = self.thread.item_list
                else:
                    result = self.thread.abort or self.thread.error
                if job_details.ident is None:
                    job_details.result_fn(completed_ok, result)
                else:
                    job_details.result_fn(completed_ok, result, job_details.ident)

            # Clean up if we have been called as a result of a job finishing or being cancelled.
            self.result_cleanup()

            # Remove the completed/cancelled job from the queue.
            self.job_queue_mutex.lock()
            self.job_queue.pop(0)
            self.job_queue_mutex.unlock()
            self.job_complete_signal.emit()
        # Are there any more jobs to do?

        # Get any null-jobs out of the way first.
        while self.job_queue\
            and not self.job_queue[0].job_fn\
        :
            self.job_queue[0].result_fn(self.job_queue[0].ident)
            self.job_queue.pop(0)

        # Anything left?
        if self.job_queue:
            job_details = self.job_queue[0]

            # Create message and thread, connect signals, and run thread.
            if isinstance(job_details.message, tuple):
                title = job_details.message[0]
                message = job_details.message[1]
            else:
                title = tr("Information")
                message = job_details.message

            message_widget = self.main_dlg.show_cancel_message(
                message.format(*job_details.args, **job_details.kwargs),
                title=title,
                progress_bar_required=job_details.progress_bar_required
            )
            self.thread = WorkThread(
                message_widget,
                job_details.job_fn,
                job_details.args,
                job_details.kwargs,
                job_details.result_is_list
            )
            if job_details.progress_bar_required:
                self.thread.progress_signal.connect(self.main_dlg.show_progress)
            self.thread.result_signal.connect(self.next_job)
            self.thread.start()

    def cancel_thread(self):
        """ Handle user pressing the cancel button on the message bar.  """
        if self.thread:
            # === Disconnect the result signal ===============================
            # Thread is about to be deleted, which will destroy the connection anyway,
            # but just to minimize the chance of race conditions....
            try:
                self.thread.result_signal.disconnect(self.next_job)
            except Exception:  # TypeError
                pass
            # === Disconnect the progress signal ===============================
            if 'scraper_login' == self.thread.worker_function.func_name\
                or 'scraper_download' == self.thread.worker_function.func_name\
            :
                _, _, _, _, scraper = self.job_queue[0]     # job_fn, args, kwargs, result_fn, scraper
                try:
                    scraper.progress_signal.disconnect(self.main_dlg.show_progress)
                except Exception:  # TypeError
                    pass

                scraper.abort = True
            else:
                try:
                    self.thread.progress_signal.disconnect(self.main_dlg.show_progress)
                except Exception:  # TypeError
                    pass

            self.thread.abort = True
            # === Stop the thread if it hasn't stopped already. ================
            # Try asking nicely for about 1s
            # Downloading optical dataset info can take a couple of seconds!
            # OTOH, if we are aborting then the info will be thrown away anyway.
            for i in range(100):
                if self.thread.finished:
                    break
                sleep(0.01)
            else:
                self.thread.terminate()  # Be brutal!
                self.thread.wait()
                self.thread.finished = True

        self.next_job()

    def result_cleanup(self):
        """ Delete the thread and the message widget.
        Results must have been retrieved before calling this.
        """
        message_widget = self.thread.message_widget
        self.thread = None
        self.main_dlg.remove_message(message_widget)
