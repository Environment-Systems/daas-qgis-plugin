# -*- coding: utf-8 -*-
"""
/****************************************************************************
 This file is part of EnvSys plugin for QGIS
 Integration with Environment Systems Data Services.
                             -------------------
        begin                : 2017-12-12
        copyright            : (C) 2017 by Environment Systems Ltd.
        email                : dataservices@envsys.co.uk
        git sha              : $Format:%H$
 ****************************************************************************/

/****************************************************************************
 *                                                                          *
 *   EnvSys plugin is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 3 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   EnvSys plugin is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
 *   GNU General Public License for more details.                           *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with EnvSys plugin. If not, see <https://www.gnu.org/licenses/>. *
 *                                                                          *
 ****************************************************************************/
"""


import os

from PyQt4 import uic
from PyQt4.QtGui import QWidget, QMessageBox, QFileDialog

from qgis.core import *
from qgis.utils import iface
from qgis.analysis import QgsZonalStatistics

# from osgeo import gdal
# import gdalconst
# import osr
import ogr

from ConfigParser import NoSectionError, NoOptionError
from translate import tr

from reproject_file import reproject_file

# from log import Log
# debug = Log('ZonalStats')


class ZonalStatsWidget(QWidget):
    # Available stats are: Count, Sum, Mean, Median, StDev, Min, Max, Range, Minority, Majority, Variety, Variance,

    CONFIG_SECTION = 'ZonalStats'

    def __init__(self, utils, parent=None):
        """ Constructor.
        :param utils: Commnon utils object so that individual widgets
            don't all have to download their own version of the lists.
        :param parent: Widget that this widget is a child of.
        """
        QWidget.__init__(self, parent)
        self.ui = uic.loadUi(os.path.expanduser(os.path.join(os.path.dirname(__file__), 'zonal_stats.ui')), self)

        self.utils = utils
        self.vector_file_lineEdit.hide()
        self.select_vector_file_btn.hide()
        self.vector_from_layer_btn.setChecked(True)
        self.on_vector_from_layer_btn_toggled()
        self.regions = []

        try:
            self.raster_file_lineEdit.setText(self.utils.config.get(self.CONFIG_SECTION, 'Raster'))
        except (NoSectionError, NoOptionError):
            pass

        try:
            self.vector_file_lineEdit.setText(self.utils.config.get(self.CONFIG_SECTION, 'Vector'))
        except (NoSectionError, NoOptionError):
            pass

        self.variance_checkBox.hide()   # Docs say this exists but practice says it doesn't.
        self.stats_checkboxes = [
            self.count_checkBox,    self.sum_checkBox,      self.mean_checkBox,
            self.median_checkBox,   self.stddev_checkBox,   self.min_checkBox,
            self.max_checkBox,      self.range_checkBox,    self.minority_checkBox,
            self.majority_checkBox, self.variety_checkBox,  # self.variance_checkBox
        ]
        self.stats_flags = [
            int(str(QgsZonalStatistics.Count)),
            int(str(QgsZonalStatistics.Sum)),
            int(str(QgsZonalStatistics.Mean)),

            int(str(QgsZonalStatistics.Median)),
            int(str(QgsZonalStatistics.StDev)),
            int(str(QgsZonalStatistics.Min)),

            int(str(QgsZonalStatistics.Max)),
            int(str(QgsZonalStatistics.Range)),
            int(str(QgsZonalStatistics.Minority)),

            int(str(QgsZonalStatistics.Majority)),
            int(str(QgsZonalStatistics.Variety)),
            # QgsZonalStatistics.Variance
        ]
        for check in self.stats_checkboxes:
            check.stateChanged.connect(self.stat_selection_changed)
        try:
            # Accept decimal or '0x'-prefixed hex.
            required_stats = int(self.utils.config.get(self.CONFIG_SECTION, 'Stats'), 0)
            if 0 == required_stats:
                required_stats = 0x74  # 64|32|16|4 = Min|Max|Mean|StdDev
            for stat in zip(self.stats_checkboxes, self.stats_flags):
                if stat[1] & required_stats:
                    stat[0].setChecked(True)
        except (NoSectionError, NoOptionError):
            for btn in [self.min_checkBox, self.max_checkBox, self.mean_checkBox, self.stddev_checkBox]:
                btn.setChecked(True)
        try:
            self.prefix_lineEdit.setText(self.utils.config.get(self.CONFIG_SECTION, 'Prefix'))
        except (NoSectionError, NoOptionError):
            pass

    def tab_shown(self):
        """ Initialization each time the tab is selected. """
        # Which vector selector is checked?
        if self.vector_from_layer_btn.isChecked():
            self.on_vector_from_layer_btn_toggled()
        elif self.vector_from_shapefile_btn.isChecked():
            self.on_vector_from_shapefile_btn_toggled()
        else:
            assert self.vector_from_region_btn.isChecked(),\
                tr("Unable to determine vector source.  Assuming 'regions'.")
            self.on_vector_from_region_btn_toggled()

    def stat_selection_changed(self):
        """ Save the state of the checkboxes.
        The easiest way of handling this is to scan through all the checkboxes.
        """
        if not self.utils.config.has_section(self.CONFIG_SECTION):
            self.utils.config.add_section(self.CONFIG_SECTION)
        stats = 0
        for stat in zip(self.stats_checkboxes, self.stats_flags):
            if stat[0].isChecked():
                stats |= int(stat[1])
        self.utils.config.set(self.CONFIG_SECTION, 'Stats', '0x{:04x}'.format(stats))
        self.utils.save_config()

    def on_zonal_stats_btn_pressed(self):
        """ Handle pressing the zonal stats button. """
        # In the python console,
        #   all_layers = QgsMapLayerRegistry.instance().mapLayers()
        #   for lyr in QgsMapLayerRegistry.instance().mapLayers():
        #       print(lyr, all_layers[lyr].crs().authid())  => "EPSG:4326"
        attribute_prefix = self.prefix_lineEdit.text()
        raster_band_idx = 1

        # Raster: Always a file.
        raster_filename = self.raster_file_lineEdit.text()
        raster_layer = QgsRasterLayer(raster_filename, 'tmp_raster')
        assert raster_layer, tr('No raster layer identified.')
        raster_epsg = int(raster_layer.crs().authid()[5:])   # Drop the 'epsg:' prefix from the authid.
        raster_extent = raster_layer.extent()
        raster_geometry = QgsGeometry.fromRect(raster_extent)
        # ds = gdal.Open(raster_filename, gdalconst.GA_ReadOnly)
        # proj = ds.GetProjection()
        # raster_spatial_ref = osr.SpatialReference(wkt=proj)
        # raster_epsg = int(raster_spatial_ref.GetAuthorityCode(None))

        # Vector: a) layer, b) shapefile, or c) region.
        # To calculate stats we need a layer in the same projection as the raster file,
        # Reprojecting in-memory is proving difficult so I'll try doing it for files using downloaded code to reproject.
        # Simplest approach I can think of:
        #   a. Layers should always have a source shapefile.  Use that.
        #   b. Shapefile. Done.
        #   c. Write regions as shapefiles.
        #   Reproject shapefile if necessary.
        #   Calculate stats.
        #   If shapefile was re-projected, load original file as a layer and copy attributes back to there.
        if self.vector_from_region_btn.isChecked():
            # Create layer from region.
            region_idx = self.vector_zone_list.itemData(self.vector_zone_list.currentIndex())
            region = self.regions[region_idx]
            (vector_filename, name) = self.utils.write_geometry_as_shapefile(
                region.region['name'],
                region.geometry,
                epsg=self.utils.SERVER_EPSG
            )
            source_type = 1
        elif self.vector_from_shapefile_btn.isChecked():
            # Load shapefile.
            txt = self.vector_file_lineEdit.text()
            assert txt, tr('No vector file identified.')
            vector_filename = os.path.expanduser(txt)
            assert os.path.isfile(vector_filename), tr('Vector filename does not specify a file.')
            source_type = 2
        else:   # self.vector_from_layer_btn.isChecked():
            vector_layer_id = self.vector_zone_list.itemData(self.vector_zone_list.currentIndex())
            original_vector_layer = QgsMapLayerRegistry.instance().mapLayers()[vector_layer_id]
            vector_filename = original_vector_layer.source()
            source_type = 3

        # Vector specification is now a file.  Reproject?
        vector_layer = QgsVectorLayer(vector_filename, 'tmp', 'ogr')
        vector_epsg = int(vector_layer.crs().authid()[5:])     # Drop the 'epsg:' prefix from the authid.

        if vector_epsg != raster_epsg:
            # We need to reproject the shapefile.
            (vector_path, ext) = os.path.splitext(vector_filename)
            reprojected_vector_filename = vector_path + '_reprojected' + ext
            reproject_file(vector_filename, reprojected_vector_filename, raster_epsg)

            # Load our reprojected file.
            tmp_vector_layer = QgsVectorLayer(reprojected_vector_filename, 'polygon', 'ogr')

            # Does the raster cover all of the vector features?
            features = tmp_vector_layer.getFeatures()
            raster_contains_vector = True
            for feature in features:
                if not raster_geometry.contains(feature.geometry()):
                    raster_contains_vector = False
                    break

            if raster_contains_vector:
                for stat in zip(self.stats_checkboxes, self.stats_flags):
                    if stat[0].isChecked():
                        s = QgsZonalStatistics.Statistic(stat[1])
                        try:
                            zonalstats = QgsZonalStatistics(
                                tmp_vector_layer,
                                raster_filename,
                                attribute_prefix,
                                raster_band_idx,
                                s
                            )
                            zonalstats.calculateStatistics(None)
                        except BaseException:
                            pass
                tmp_vector_layer = None     # The file should have been updated.  We don't need this any more.

                # Copy attributes back to the original file.
                self.copy_attributes(reprojected_vector_filename, vector_filename, attribute_prefix)
            else:
                msg = QMessageBox(QMessageBox.Information, tr('Warning'),
                                  tr('The raster layer must cover the vector layer'))
                msg.exec_()
        else:
            for stat in zip(self.stats_checkboxes, self.stats_flags):
                if stat[0].isChecked():
                    s = QgsZonalStatistics.Statistic(stat[1])
                    zonalstats = QgsZonalStatistics(vector_layer, raster_filename, attribute_prefix, raster_band_idx, s)
                    zonalstats.calculateStatistics(None)
        if 3 == source_type:
            original_vector_layer.reload()
        else:
            name, ext = os.path.splitext(os.path.basename(vector_filename))
            iface.addVectorLayer(vector_filename, name, "ogr")

    # noinspection PyMethodMayBeStatic
    def copy_attributes(self, src_filename, dst_filename, prefix):
        """ Copy all the attribute matching our prefix from one file to the other. """
        dst_datasource = ogr.Open(dst_filename, 1)
        src_datasource = ogr.Open(src_filename, 0)
        dst_layer = dst_datasource.GetLayer()
        src_layer = src_datasource.GetLayer()
        dst_layer.ResetReading()
        src_layer.ResetReading()
        for feature_num in range(dst_layer.GetFeatureCount()):
            dst_feature = dst_layer.GetFeature(feature_num)
            src_feature = src_layer.GetFeature(feature_num)
            dst_items = dst_feature.items()
            src_items = src_feature.items()
            for key in src_items.keys():
                if key.startswith(prefix) and key not in dst_items:
                    dst_layer.CreateField(ogr.FieldDefn(key, ogr.OFTReal))
                dst_feature.SetField(key, src_items[key])
            dst_layer.SetFeature(dst_feature)
        dst_feature = None
        src_feature = None
        dst_layer = None
        src_layer = None
        dst_datasource = None
        src_datasource = None

    def load_layer_lists(self):
        """ Load the list of vector layers. """
        self.vector_zone_list.clear()

        map_layers = sorted(QgsMapLayerRegistry.instance().mapLayers().values(), key=lambda lyr: lyr.name())
        for layer in map_layers:
            if layer.type() == QgsMapLayer.VectorLayer:
                self.vector_zone_list.addItem(layer.name())

    # noinspection PyPep8Naming
    def on_raster_file_lineEdit_textChanged(self):
        """ Save the user-selected raster filename to the config. """
        filename = os.path.expanduser(self.raster_file_lineEdit.text())
        self.utils.config.set(self.CONFIG_SECTION, 'Raster', filename)
        self.utils.save_config()
        self.zonal_stats_btn.setEnabled(os.path.isfile(filename))

    def on_select_raster_file_btn_pressed(self):
        """ Handle the browse button for raster files. """
        filedlg = QFileDialog()
        filename = filedlg.getOpenFileName(
            filedlg,
            tr('Open file', 'Dialog for selecting a file to open'),
            os.path.expanduser('~/'),
            'GeoTIFFs (*.tif)'
        )
        self.raster_file_lineEdit.setText(filename)

    def on_vector_from_layer_btn_toggled(self):
        """ Set what type of source we want for the vector information. """
        if self.vector_from_layer_btn.isChecked():
            self.vector_zone_list.clear()
            valid_layers = self.utils.list_valid_vector_layers()
            self.zonal_stats_btn.setEnabled([] != valid_layers)
            for layer in valid_layers:
                self.vector_zone_list.addItem(layer.name(), layer.id())         # id is a string.
            self.vector_file_lineEdit.hide()
            self.select_vector_file_btn.hide()
            self.vector_zone_list.show()

    def on_vector_from_shapefile_btn_toggled(self):
        """ Set what type of source we want for the vector information. """
        if self.vector_from_shapefile_btn.isChecked():
            self.vector_file_lineEdit.show()
            self.select_vector_file_btn.show()
            self.vector_zone_list.hide()

    def on_select_vector_file_btn_pressed(self):
        """ Handle the browse button for vector files. """
        filedlg = QFileDialog()
        filename = filedlg.getOpenFileName(
            filedlg,
            tr('Open file', 'Dialog for selecting a file to open'),
            os.path.expanduser('~/'),
            'Shapefiles (*.shp)'
        )
        self.vector_file_lineEdit.setText(filename)

    def on_vector_from_region_btn_toggled(self):
        """ Set what type of source we want for the vector information. """
        if self.vector_from_region_btn.isChecked():
            self.vector_zone_list.clear()
            self.vector_file_lineEdit.hide()
            self.select_vector_file_btn.hide()
            self.vector_zone_list.show()
            self.utils.list_regions(self.list_regions_result)

    def list_regions_result(self, completed_ok, regions):
        """ Update the region list
        :param completed_ok: Indicates whether the region list was fetched successfully or not.
        :param regions: The region list, if completed_ok.
        """
        if completed_ok:
            self.regions = regions
            self.zonal_stats_btn.setEnabled([] != self.regions)
            for region_idx in range(len(self.regions)):
                region = self.regions[region_idx]
                self.vector_zone_list.addItem(region['name'], region_idx)     # id is a number.

    # noinspection PyPep8Naming
    def on_vector_file_lineEdit_textChanged(self):
        """ Handle changes to the vector filename """
        filename = os.path.expanduser(self.vector_file_lineEdit.text())
        self.utils.config.set(self.CONFIG_SECTION, 'Vector', filename)
        self.utils.save_config()
        self.zonal_stats_btn.setEnabled(os.path.isfile(filename))

    # noinspection PyPep8Naming
    def on_prefix_lineEdit_textEdited(self):
        """ Handle changes to the prefix text by saving them to the config. """
        self.utils.config.set(self.CONFIG_SECTION, 'Prefix', self.prefix_lineEdit.text())
        self.utils.save_config()

    def help(self):
        """ Show help on this widget. """
        dlg = self.parent().parent().parent().parent()
        return dlg.show_help(
            tr('Zonal statistics help', 'Statistics gathered for particular zones on the map.'),
            'zonal_stats.html'
        )
