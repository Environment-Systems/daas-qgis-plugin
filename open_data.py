# -*- coding: utf-8 -*-
"""
/****************************************************************************
 This file is part of EnvSys plugin for QGIS
 Integration with Environment Systems Data Services.
                             -------------------
        begin                : 2017-12-12
        copyright            : (C) 2017 by Environment Systems Ltd.
        email                : dataservices@envsys.co.uk
        git sha              : $Format:%H$
 ****************************************************************************/

/****************************************************************************
 *                                                                          *
 *   EnvSys plugin is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 3 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   EnvSys plugin is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
 *   GNU General Public License for more details.                           *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with EnvSys plugin. If not, see <https://www.gnu.org/licenses/>. *
 *                                                                          *
 ****************************************************************************/
"""


import os
from PyQt4 import uic
from qgis.core import *
from qgis.utils import iface

from PyQt4.QtCore import Qt
from PyQt4.QtGui import QWidget, QCheckBox, QHBoxLayout, QVBoxLayout
from requests import session
from translate import tr

# from log import Log
# debug = Log('OpenData')


class LayerSelector(QWidget):
    """ Simple widget that consists of a checkbox and layer name. """

    def __init__(self, layer, name, parent=None):
        """ Simple selector widget.
        :param layer: WMS layer fetched from API
        :param name: Layer's display name
        :param parent: Widget this one belongs to.
        """
        QWidget.__init__(self, parent)

        self.layer = layer
        self.check = QCheckBox(name)
        layout = QHBoxLayout(self)
        layout.addWidget(self.check)


class OpenDataWidget(QWidget):
    ROOT_URL = 'https://data.envsys.co.uk/'
    OPEN_DATA_URL = ROOT_URL + 'api/free_layers/'
    CRS = 'EPSG:3857'
    FORMAT = 'image/png'

    def __init__(self, utils, parent=None):
        """ Constructor
        :param utils: Common utils object so that individual widgets
            don't all have to download their own version of the lists.
        :param parent: Widget that this widget is a child of.
        """
        QWidget.__init__(self, parent)
        self.ui = uic.loadUi
        self.ui = uic.loadUi(os.path.expanduser(os.path.join(os.path.dirname(__file__), 'open_data.ui')), self)
        self.utils = utils
        self.main_dlg = None

        self.dataset_layer_group = None
        self.layers_list = []
        self.loaded_layers = []
        self.legend = iface.legendInterface()

    def tab_shown(self):
        """ Initialization each time the tab is selected. """
        self.main_dlg = self.parent().parent().parent().parent()
        # self.main_dlg.tip_window()

        layout = self.layers_scrollAreaWidgetContents.layout()
        if layout is None:
            layout = QVBoxLayout()
            self.layers_scrollAreaWidgetContents.setLayout(layout)

        layers_data = self.list_open_data_layers()
        layer_and_display_names = self.parse_layers_data(layers_data)

        self.prepare_layers_list(layer_and_display_names)
        self.list_layers()

    def list_open_data_layers(self):
        """ Fetch a list of open data layers.
        :return: Api response with layers data.
        """
        layers = []
        try:
            layers = session().get(self.OPEN_DATA_URL)
        except:
            self.main_dlg.show_message(tr('Unable to connect to Data Services.'))
            pass
        return layers.json()

    def parse_layers_data(self, data):
        """ Parse layers data fetched from api.
        :param data: Api response with layers data.
        :return: List of tuples with layer name and display name in each.
        """
        names = []
        for item in data:
            if item.get("layer") == '':
                pass
            layer_name = item.get("layer").encode('ascii', 'ignore')
            display_name = item.get("name").encode('ascii', 'ignore')
            names.append(tuple((layer_name, display_name)))
        return names

    def extract_workspace_and_name(self, layer_name):
        """ Split layer name to workspace and short name
        (this will be used later to build url to fetch layer).
        :param layer_name: Name of the layer from the response.
        :return: A tuple containing workspace and short name.
        """
        return tuple(layer_name.split(':'))

    def get_layer_to_load(self, workspace, layer_name, display_name):
        """ Fetch WMS layer to be loaded to QGis
        :param workspace: Workspace name to be used in url.
        :param layer_name: Short name to be used in url.
        :param display_name: Name to be displayed in Layers Panel
        :return: QgsRasterLayer object
        """
        urlWithParams = 'crs=' + self.CRS \
                        + '&layers=' + layer_name \
                        + '&styles=&format=' + self.FORMAT \
                        + '&url=' + self.ROOT_URL \
                        + 'geoserver/' + workspace + '/ows?'
        return QgsRasterLayer(urlWithParams, display_name, 'wms')

    def prepare_layers_list(self, layer_and_display_names):
        """ Create list of QgsRasterLayer objects
        :param layer_and_display_names: A tuple containing layer name and display name from api response.
        """
        for layer in layer_and_display_names:
            layer_name, display_name = layer
            workspace, name = self.extract_workspace_and_name(layer_name)
            self.layers_list.append(self.get_layer_to_load(workspace, name, display_name))

    def list_layers(self):
        """ Load downloaded layers. Initially all layers are listed in Open Data tab."""
        layout = self.layers_scrollAreaWidgetContents.layout()

        if len(self.layers_list) and self.dataset_layer_group is None:
            self.main_dlg.show_message(tr('Loading Open Data Layers.'))
            self.dataset_layer_group = self.legend.addGroup(tr('Open Data'))
            for layer in self.layers_list:
                selector = LayerSelector(layer, layer.name())
                layout.addWidget(selector)
                selector.check.stateChanged.connect(self.load_layer_selected)
            self.main_dlg.message_bar.clearWidgets()

    def load_layer_selected(self, state):
        """ Load layer to QGis when checkbox selected.
        :param state: The new state of the checkbox.
        """
        selector = self.sender().parent()
        try:
            if Qt.Checked == state:
                if selector.layer in self.loaded_layers:
                    self.legend.setLayerVisible(selector.layer, True)
                else:
                    self.loaded_layers.append(selector.layer)
                    QgsMapLayerRegistry.instance().addMapLayer(selector.layer)
                    self.legend.moveLayer(selector.layer, self.dataset_layer_group)
            else:
                self.legend.setLayerVisible(selector.layer, False)
        except RuntimeError:
            pass

    # noinspection PyMethodMayBeStatic
    def help(self):
        """ Show help on this widget. """
        dlg = self.parent().parent().parent().parent()
        return dlg.show_help(tr('Open Data help'), 'open_data.html')
