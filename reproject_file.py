""" https://pcjericks.github.io/py-gdalogr-cookbook/projection.html
Tweaked to extract source EPSG code from the vector file and to use
a given target EPSG code.
"""

from osgeo import ogr, osr
import os

def reproject_file(src_filename, dst_filename, target_epsg):
    driver = ogr.GetDriverByName('ESRI Shapefile')

    # output SpatialReference
    outSpatialRef = osr.SpatialReference()
    outSpatialRef.ImportFromEPSG(target_epsg)

    # get the input layer
    inDataSet = driver.Open(src_filename)
    inLayer = inDataSet.GetLayer()
    inSpatialRef = inLayer.GetSpatialRef()
    # inEpsg = int(inSpatialRef.GetAuthorityCode(None))

    # create the CoordinateTransformation
    coordTrans = osr.CoordinateTransformation(inSpatialRef, outSpatialRef)

    # create the output layer
    if os.path.exists(dst_filename):
        driver.DeleteDataSource(dst_filename)
    outDataSet = driver.CreateDataSource(dst_filename)
    # dst_layer_name, ext = os.path.splitext(os.path.basename(dst_filename))
    # The const-ness of the layer name seems to matter!  Use a fixed string and pass srs in separately.
    outLayer = outDataSet.CreateLayer('tmp_layer_name', srs=outSpatialRef, geom_type=ogr.wkbMultiPolygon)

    # add fields
    inLayerDefn = inLayer.GetLayerDefn()
    for i in range(0, inLayerDefn.GetFieldCount()):
        fieldDefn = inLayerDefn.GetFieldDefn(i)
        outLayer.CreateField(fieldDefn)
    
    # get the output layer's feature definition
    outLayerDefn = outLayer.GetLayerDefn()

    # loop through the input features
    inFeature = inLayer.GetNextFeature()
    while inFeature:
        # get the input geometry
        geom = inFeature.GetGeometryRef()
        # reproject the geometry
        geom.Transform(coordTrans)
        # create a new feature
        outFeature = ogr.Feature(outLayerDefn)
        # set the geometry and attribute
        outFeature.SetGeometry(geom)
        for i in range(0, outLayerDefn.GetFieldCount()):
            outFeature.SetField(outLayerDefn.GetFieldDefn(i).GetNameRef(), inFeature.GetField(i))
        # add the feature to the shapefile
        outLayer.CreateFeature(outFeature)
        # dereference the features and get the next input feature
        outFeature = None
        inFeature = inLayer.GetNextFeature()
    
    # Save and close the shapefiles
    inDataSet = None
    outDataSet = None