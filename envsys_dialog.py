from locale import getlocale
from ConfigParser import SafeConfigParser

from qgis.gui import QgsMessageBar
from qgis.utils import *
#try:
#    # For testing.
#    from dummy_main_dialog import QgsMessageBarItem
#except:
from qgis.gui import QgsMessageBarItem


from PyQt4 import QtGui, uic, QtCore
from PyQt4.QtGui import QMessageBox, QDialogButtonBox, QSizePolicy, QProgressBar
from PyQt4.QtCore import pyqtSlot

from open_data import OpenDataWidget
from translate import tr

from base_dialog import BaseDialog, TipDialog
from settings import SettingsWidget
from search import SearchWidget
from products import ProductsWidget
# from zonal_stats import ZonalStatsWidget
from free import FreeWidget
from utils import Utils

# from log import Log
# debug = Log('Main')

# This translation is used in testing
dummy = tr('Good morning')

FORM_CLASS, _ = uic.loadUiType(os.path.join(os.path.dirname(__file__), 'envsys_dialog_base.ui'))


class EnvsysDialog(BaseDialog):

    def __init__(self, utils, settings_dlg, parent=None):
        """Constructor."""
        super(EnvsysDialog, self).__init__(utils, parent)

        self.plugin_tabs.addTab(OpenDataWidget(utils), tr('Open Data'))
        self.plugin_tabs.addTab(FreeWidget(utils, parent), tr('Download Open Data'))
        self.plugin_tabs.addTab(SearchWidget(utils), tr('Search and Download'))
        self.plugin_tabs.addTab(ProductsWidget(utils), tr('Download Products'))

        self.current_tab = 0
        self.plugin_tabs.setCurrentIndex(self.current_tab)
        self.plugin_tabs.currentChanged.connect(self.plugin_tabs_currentChanged)

        self.utils = utils
        self.utils.set_main_dlg(self)
        self.settings_dlg = settings_dlg

    def on_settings_btn_clicked(self):
        self.settings_dlg.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        self.settings_dlg.show()

    def tip_window(self):
        if self.utils.config.get(self.utils.TIP_WINDOW, self.utils.TIP_DISPLAY_LABEL) == 'True':
            return TipDialog(self.utils).show_tip_window()
