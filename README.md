# README #

Get your application up and running.

### What is this repository for? ###

* This plugin integrates Environment Systems' Data Services into QGIS
using the REST API.
* Version 0.9.3

### How do I get set up? ###

* Whilst we are aiming to get this plugin into the central QGIS plugin repository, for now it is installable from our own repo.
* In QGIS, go to the plugins menu, and select "Manage and Install Plugins..."
* In the Plugins window, choose settings from the bar on the left and under "Plugin Repositories" click the add button
* In the url field, enter https://data.envsys.co.uk/static/qgis/plugins.xml - note that this url only works with https
* Give the repository a sensible name, e.g., Environment Systems, then click OK.
* If you click "All" in the left hand bar, you should now be able to see and install the EnvSys plugin.


### Who do I talk to? ###

* If you encounter any issues with the plugin, please use the issues tab on this bitbucket page to report them
* For any other queries, contact the repo owner: [Rob](mailto:rob.rokosz@envsys.co.uk) or [Seb](mailto:sebastian.clarke@envsys.co.uk).
* Other community or team contact: [Environment Systems Developers](mailto:dataservices@envsys.co.uk).
