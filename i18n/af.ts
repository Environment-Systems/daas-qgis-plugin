<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="af" sourcelanguage="en">
<context>
    <name>@default</name>
    <message>
        <location filename="../envsys_dialog.py" line="53"/>
        <source>Good morning</source>
        <translation type="unfinished">Goeie more</translation>
    </message>
    <message>
        <location filename="../utils.py" line="207"/>
        <source>Authentication details are valid.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../utils.py" line="209"/>
        <source>Authentication details are &lt;em&gt;not&lt;/em&gt; valid.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.py" line="143"/>
        <source>Shapes directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.py" line="224"/>
        <source>Authentication help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.py" line="226"/>
        <source>Configuration help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../envsys_dialog.py" line="84"/>
        <source>Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../envsys_dialog.py" line="153"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../envsys_dialog.py" line="169"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../utils.py" line="368"/>
        <source>Downloading optical holdings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../utils.py" line="388"/>
        <source>Downloading backscatter holdings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../utils.py" line="406"/>
        <source>Downloading optical product types.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../utils.py" line="424"/>
        <source>Downloading regions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../utils.py" line="483"/>
        <source>Generating product.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../utils.py" line="507"/>
        <source>Downloading products list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../utils.py" line="614"/>
        <source>Requesting download url.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../utils.py" line="540"/>
        <source>Downloading product.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../envsys_dialog.py" line="85"/>
        <source>Regions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../utils.py" line="443"/>
        <source>Creating region.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../utils.py" line="563"/>
        <source>Log in and fetch list of map layers.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../utils.py" line="588"/>
        <source>Info</source>
        <comment>Short version of Information</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../envsys_dialog.py" line="270"/>
        <source>Error showing help.
{}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../envsys_dialog.py" line="298"/>
        <source>Help for {} is not currently available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../utils.py" line="588"/>
        <source>Download {0}_{1}.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../envsys_dialog.py" line="86"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../envsys_dialog.py" line="87"/>
        <source>Products</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../envsys_dialog.py" line="89"/>
        <source>Free</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../utils.py" line="649"/>
        <source>Please set username and password before continuing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.py" line="162"/>
        <source>Downloads directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.py" line="214"/>
        <source>About EnvSys plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.py" line="50"/>
        <source>Plugin version: {}
{}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.py" line="210"/>
        <source>Opening information in your default browser.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Envsys</name>
    <message>
        <location filename="../envsys.py" line="177"/>
        <source>&amp;Environment Systems</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../envsys.py" line="167"/>
        <source>Environment Systems</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EnvsysDialogBase</name>
    <message>
        <location filename="../envsys_dialog_base.ui" line="14"/>
        <source>EnvSys</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>config</name>
    <message>
        <location filename="../config.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.ui" line="36"/>
        <source>Authentication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.ui" line="44"/>
        <source>Check authentication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.ui" line="51"/>
        <source>See password as you type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.ui" line="58"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.ui" line="68"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.ui" line="101"/>
        <source>Save authentication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.ui" line="121"/>
        <source>Downloads directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.ui" line="138"/>
        <source>Build information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.ui" line="155"/>
        <source>About EnvSys plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.ui" line="219"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.ui" line="187"/>
        <source>Shapes directory</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
