FORMS = \
../config.ui                    \
../envsys_dialog_base.ui        \

SOURCES = \
../__init__.py                  \
../config.py                    \
../envsys.py                    \
../envsys_dialog.py             \
../envsys_dialog_base.ui        \
../plugin_upload.py             \
../resources.py                 \
../utils.py                     \

TRANSLATIONS =	\
af.ts           \
envsys.ts       \
envsys_es_ES.ts
