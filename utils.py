# -*- coding: utf-8 -*-
"""
/****************************************************************************
 This file is part of EnvSys plugin for QGIS
 Integration with Environment Systems Data Services.
                             -------------------
        begin                : 2017-12-12
        copyright            : (C) 2017 by Environment Systems Ltd.
        email                : dataservices@envsys.co.uk
        git sha              : $Format:%H$
 ****************************************************************************/

/****************************************************************************
 *                                                                          *
 *   EnvSys plugin is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 3 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   EnvSys plugin is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
 *   GNU General Public License for more details.                           *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with EnvSys plugin. If not, see <https://www.gnu.org/licenses/>. *
 *                                                                          *
 ****************************************************************************/
"""


# Add the extras directory to the python search path so
# Add the extras directory to the python search path so
# that the envsys_client and zipfile modules can be found.
# Need to do this before we can import our widgets.
# Putting this code at the start of every source file that accesses
# envsys_client or zipfile.py should keep the unit-testing happy.
import os
import sys
import urllib
from copy import deepcopy
import base64

from ConfigParser import ConfigParser, NoOptionError

import qgis
from qgis.core import *
from qgis.gui import QgsMessageBarItem

from PyQt4.QtCore import QThread, pyqtSignal, pyqtSlot, QVariant, QObject

from job_queue import JobQueue
from translate import tr
# ---------------------------------------------------------------------
# The envsys_client module is installed in a subdirectory rather than as a full Python library.
extras_path = os.path.join(os.path.dirname(__file__), 'extras')
if extras_path not in sys.path:
    # Allow us to override installed versions if we need to.
    sys.path.insert(0, extras_path)
import envsys_client
from envsys_client.api_client import ApiException

REGIONS_API = 0
BACKSCATTER_API = 1
OPTICAL_API = 2
MISCELLANEOUS_API = 3

api = [
    envsys_client.ARegionsApi(),
    envsys_client.BBackscatterApi(),
    envsys_client.COpticalApi(),
    envsys_client.DMiscellaneousApi()
]


# ---------------------------------------------------------------------
# from log import Log
# debug = Log('Utils')

ISO_DATETIME_FORMAT = '%Y-%m-%dT%H:%M:%SZ'


def encrypt(plaintext):
    """ Encrypt plaintext in some way.
    Allows for different methods to be employed consistently.
    :param plaintext: Text to be encrypted.
    :return: Encrypted version of plaintext.
    """
    return base64.b64encode(plaintext)


def decrypt(ciphertext):
    """ Decrypt ciphertext
    Allows for different methods to be employed consistently.
    :param ciphertext: Encrypted text to be decrypted.
    :return: Decrypted version of ciphertext.
    """
    return base64.b64decode(ciphertext)


class Utils(QObject):
    """ Utility functions expected to be common across several aspects of the plugin.
    Derived from QObject so that we can emit signals for tabs to process.
    """

    # --- Config file section and item titles. ---------------------------------
    WORK_DIRS_SECTION = 'WorkingDirectories'
    SHAPES_LABEL = 'ShapesDir'
    DOWNLOADS_LABEL = 'DownloadsDir'
    TIP_WINDOW = "TipWindow"
    TIP_DISPLAY_LABEL = "DisplayTipWindow"

    WGS84_EPSG = 4326
    WEB_MERCATOR_EPSG = 3857
    OSGB1936_EPSG = 27700
    SERVER_EPSG = WGS84_EPSG

    # Support for checking which product types are supported by which dataset types.
    # This information is not available for download so it is all hard-coded here.
    # See odt_supports_opt() below.
    # Actually, all product types are supported by all dataset types at present.
    R = 0       # This will have to do until we can use Python 3 enums.
    G = 1
    B = 2
    NIR = 3
    SWIR = 4
    RE705 = 5
    RE740 = 6
    RE783 = 7
    NNIR = 8
    SWIR219 = 9

    ODT_BANDS_AVAILABLE = [
        {R, G, B, NIR, SWIR, RE705},                                # Sentinel-2
        {R, G, B, NIR, SWIR, RE705, RE740, RE783, NNIR, SWIR219}    # Sentinel-2-Sub60m
    ]
    OPT_BANDS_REQUIRED = [
        {R, G, B},          # RGB
        {R, G, B, NIR},     # Sentinel2 - 10m - Bands
        {R, G},             # NRI
        {G, SWIR},          # NDWI
        {R, NIR},           # OSAVI
        {R, NIR},           # NDVI
        {R, B, RE705},      # PSRI
        {R, B, NIR},        # EVI
    ]

    # Clarify interpretation of some arguments.
    PROGRESS_BAR_REQUIRED = True
    PROGRESS_BAR_NOT_REQUIRED = False
    RESULT_IS_LIST = True
    RESULT_IS_SINGLE = False

    download_queue_signal = pyqtSignal()
    progress_signal = pyqtSignal(QgsMessageBarItem, int, int)
    result_signal = pyqtSignal(QThread)
    message_bar_widgetRemoved = pyqtSlot(QgsMessageBarItem)

    # --------------------------------------------------------------------------

    def __init__(self):
        """ Initialize this module.  Don't perform any network access until it is explicitly requested. """
        QObject.__init__(self)
        self.settings_dlg = None
        self.main_dlg = None
        self.job_queue = None
        self.download_queue = None
        envsys_client.configuration.host = 'https://data.envsys.co.uk/api/'    # Only possible value
        self.config_filename = os.path.expanduser('~/.envsys_plugin')
        self.config = ConfigParser()
        self.config.read(self.config_filename)
        self.username = ''
        self.password = ''
        self.homepage = ''

        # self.job_queue = JobQueue(self.main_dlg)
        # self.download_queue = JobQueue(self.main_dlg)

        # Get working dir information.
        if not self.config.has_section(self.WORK_DIRS_SECTION):
            self.config.add_section(self.WORK_DIRS_SECTION)

        try:
            self.shapes_dir = self.config.get(self.WORK_DIRS_SECTION, self.SHAPES_LABEL)
        except NoOptionError:
            self.shapes_dir = os.path.expanduser('~/')

        try:
            self.downloads_dir = self.config.get(self.WORK_DIRS_SECTION, self.DOWNLOADS_LABEL)
        except NoOptionError:
            self.downloads_dir = os.path.expanduser('~/')

        if not self.config.has_section(self.TIP_WINDOW):
            self.config.add_section(self.TIP_WINDOW)
            self.config.set(self.TIP_WINDOW, self.TIP_DISPLAY_LABEL, 'True')

        try:
            self.display_tip_window = self.config.get(self.TIP_WINDOW, self.TIP_DISPLAY_LABEL)
        except NoOptionError:
            self.display_tip_window = True

    def set_main_dlg(self, main_dlg):
        self.main_dlg = main_dlg
        self.job_queue = JobQueue(self.main_dlg)
        self.download_queue = JobQueue(self.main_dlg)

    def set_settings_dlg(self, settings_dlg):
        self.settings_dlg = settings_dlg
        self.job_queue = JobQueue(self.settings_dlg)
        self.download_queue = JobQueue(self.settings_dlg)

    def set_shapes_dir(self, shapes_dir):
        """ Set the directory where we will create shapefiles e.g. when we download regions. """
        self.shapes_dir = os.path.expanduser(str(shapes_dir))
        self.config.set(self.WORK_DIRS_SECTION, self.SHAPES_LABEL, self.shapes_dir)
        self.save_config()

    def set_downloads_dir(self, downloads_dir):
        """ Set the directory where we will download available products.
         We will also unzip products there.
         """
        self.downloads_dir = os.path.expanduser(str(downloads_dir))
        self.config.set(self.WORK_DIRS_SECTION, self.DOWNLOADS_LABEL, self.downloads_dir)
        self.save_config()

    def save_config(self):
        """ Save the config file. """
        with open(self.config_filename, 'wb') as configfile:
            self.config.write(configfile)

    def set_authentication(self):
        """ Update the username and password used for authentication in api calls.
        :return: True if both a username and a password are available.
        Doesn't check to see whether they are correct though.
        """
        envsys_client.configuration.username = self.username
        envsys_client.configuration.password = self.password
        return self.username and self.password

    def check_authentication_ok(self):
        """ Check whether the username and password are valid.
        Do this by requesting a url for product #0, which doesn't exist.
        This keeps the response fast.
        """
        if not self.get_download_url(0, self.check_authentication_ok_result):
            self.check_authentication_ok_result(False, None, None)

    def check_authentication_ok_result(self, completed_ok, result, ident):
        """ Check whether the username and password were accepted.
        A 'Not found' message tells us that the credentials are good.
        :param completed_ok: True for job success.
        :param result: Error status.  In the unlikely event of the job completing ok, this would be the download URL
        :param ident: Identifies the check_authentication_ok() call.  Unused.
        """
        # "404 - Not Found" would indicate that the authentication
        # was accepted but no such product was found.
        # Other error possibility seems to be an AssertionError.
        if (not completed_ok and (isinstance(result, ApiException) and 404 == result.status))\
            or (completed_ok and isinstance(result, dict)):
            # self.message_bar_widgetRemoved(self.config_dlg.message_bar)
            self.settings_dlg.show_message(tr('Authentication details are valid.'), duration=2)
        else:
            # self.message_bar_widgetRemoved(self.config_dlg.message_bar)
            self.settings_dlg.show_message(tr('Authentication details are <em>not</em> valid.'), duration=2)
            self.no_auth()

    def odt_supports_opt(self, odt, opt):
        """ Check whether optical data type supports optical product type.
        :param odt: Optical dataset type.
        :param opt: Optical product type.
        """
        return self.OPT_BANDS_REQUIRED[opt].issubset(self.ODT_BANDS_AVAILABLE[odt])

    # === QGIS functionality. ==================================================

    # noinspection PyMethodMayBeStatic
    def list_valid_vector_layers(self):
        """ Create a list of those map layers which are vector layers with one feature.
        These are the ones we can create regions for or use as AOIs.
        :return: List of valid layers.
        """
        all_layers = QgsMapLayerRegistry.instance().mapLayers()
        valid_layers = []
        for layer_id in all_layers.keys():
            layer = all_layers[layer_id]
            if layer.type() == QgsMapLayer.VectorLayer                              \
                and qgis.core.QgsWKBTypes.PolygonGeometry == layer.geometryType()   \
            :
                all_features = layer.getFeatures()

                # Do we have a single feature, with geometry?
                num_features = 0
                for feature in all_features:
                    num_features += 1
                    if num_features > 1:
                        num_features = 0
                        break
                    geom = feature.geometry()
                    if geom is None:
                        num_features = 0
                        break

                if 1 == num_features:
                    # Layer is suitable for offering to create a corresponding region/aoi.
                    valid_layers.append(layer)
        return valid_layers

    # noinspection PyMethodMayBeStatic
    def list_vector_layers(self):
        """ Create a list of those map layers which are vector layers.
        Some layers may include multiple features.  When creating regions for these or using them as AOIs,
        we will have to create multiple regions or perform multiple searches/create multiple products.
        :return: List of vector layers.
        """
        all_layers = QgsMapLayerRegistry.instance().mapLayers()
        vector_layers = []
        for layer_id in all_layers.keys():
            layer = all_layers[layer_id]
            if layer.type() == QgsMapLayer.VectorLayer                              \
                and qgis.core.QgsWKBTypes.PolygonGeometry == layer.geometryType()   \
            :
                # Layer is suitable for offering to create a corresponding region/aoi.
                vector_layers.append(layer)
        return vector_layers

    # noinspection PyMethodMayBeStatic
    def make_geometry_from_coords(self, coords, transform=None):
        """ Make a QGIS geometry object using given coordinate info.
        A geometry object is just a set of numbers with no projection information.
        :param coords: List of coordinates where each coordinate is a list of two numbers
            e.g.    [[1, 2], [3, 4], [5, 6]]
        :param transform: Projection transform to be applied to each point.
        :return: Geometry object.
        """
        list_polygon = []
        for p in coords[0]:
            if transform:
                point = transform.transform(p[0], p[1])
            else:
                point = QgsPoint(p[0], p[1])
            list_polygon.append(point)
        list_polygon.append(list_polygon[0])
        geom_p = QgsGeometry.fromPolygon([list_polygon])
        return geom_p

    # noinspection PyMethodMayBeStatic
    def make_layer_from_geometry(self, name, zone, epsg=WGS84_EPSG):
        """ Make a QGIS layer from a geometry object.
        Don't create a shapefile for it.
        Don't add it to the map.
        :param name: The new layer name.
        :param zone: A geometry object specifying the shape of the layer.
        :param epsg: EPSG code to be assigned to the layer.  Doesn't affect the numbers though.
        :return: The new in-memory layer.
        """
        # Create new virtual layer
        vlyr = QgsVectorLayer("Polygon?crs=epsg:" + str(epsg), str(name), "memory")
        dprov = vlyr.dataProvider()

        # Add field to virtual layer
        dprov.addAttributes([
            QgsField("name", QVariant.String),
            # QgsField("size", QVariant.Double)
        ])

        vlyr.updateFields()

        feature = QgsFeature()
        feature.setGeometry(zone)
        dprov.addFeatures([feature])
        vlyr.setLayerTransparency(60)
        vlyr.updateExtents()

        return vlyr

    def make_layer_from_coords(self, name, coords, transform=None, epsg=WGS84_EPSG):
        """ Make an in-memory layer from the given coordinates.
        :param name: Name of the layer
        :param coords: Coordinates of the polygon vertices.
        :param transform: Projection transform to perform, if any.
        :param epsg: EPSG code for the layer.
        :return: The new in-memory layer.
        """
        geometry = self.make_geometry_from_coords(coords, transform)
        if transform:
            epsg = int(transform.destCRS().authid()[5:])
            layer = self.make_layer_from_geometry(name, geometry, epsg)
        else:
            layer = self.make_layer_from_geometry(name, geometry, epsg)
        return layer

    # === Threaded network calls ===============================================

    def stop_thread_for(self, message_widget):
        """ Cancel the current operation associated with the given message widget.
        :param message_widget: The message widget associated with the operation to be cancelled.
        """
        if self.job_queue\
            and self.job_queue.thread\
            and message_widget == self.job_queue.thread.message_widget\
        :
            self.job_queue.cancel_thread()
        elif self.download_queue \
            and self.download_queue.thread\
            and message_widget == self.download_queue.thread.message_widget\
        :
            self.download_queue.cancel_thread()

    # noinspection PyPep8Naming
    @pyqtSlot(QgsMessageBarItem)
    def message_bar_widgetRemoved(self, message_widget):
        """ Cancel the thread when its message is closed, just as when it gets cancelled.
        :param message_widget: Message item that was removed.
        """
        self.stop_thread_for(message_widget)

    def list_optical_datasets(self, args, kwargs, result_fn):
        """ Fetch a list of optical holdings, filtered and ordered according to args and kwargs.
        :param args: Positional artgument list.
        :param kwargs: Keyword argument list.
        :param result_fn: Function to be called on completion of the job.
        """
        if self.set_authentication():
            self.job_queue.queue_job(
                api[OPTICAL_API].list_optical_datasets,
                tr('Downloading optical holdings.'),
                self.PROGRESS_BAR_REQUIRED,
                args,
                kwargs,
                self.RESULT_IS_LIST,
                result_fn
            )
            return True
        self.no_auth()
        return False

    def list_backscatter_datasets(self, args, kwargs, result_fn):
        """ Fetch a list of backscatter holdings, filtered and ordered according to args and kwargs.
        :param args: Positional artgument list.
        :param kwargs: Keyword argument list.
        :param result_fn: Function to be called on completion of the job.
        """
        if self.set_authentication():
            self.job_queue.queue_job(
                api[BACKSCATTER_API].list_backscatters,
                tr('Downloading backscatter holdings.'),
                self.PROGRESS_BAR_REQUIRED,
                args,
                kwargs,
                self.RESULT_IS_LIST,
                result_fn
            )
            return True
        self.no_auth()
        return False

    def list_product_types(self, result_fn):
        """ Fetch a list of product types.
        :param result_fn: Function to be called on completion of the job.
        """
        if self.set_authentication():
            self.job_queue.queue_job(
                api[OPTICAL_API].optical_product_types,
                tr('Downloading optical product types.'),
                self.PROGRESS_BAR_REQUIRED,
                [],
                {},
                self.RESULT_IS_LIST,
                result_fn
            )
            return True
        self.no_auth()
        return False

    def list_regions(self, result_fn):
        """ Fetch a list of regions.
        :param result_fn: Function to be called on completion of the job.
        """
        if self.set_authentication():
            self.job_queue.queue_job(
                api[REGIONS_API].list_regions,
                tr('Downloading regions.'),
                self.PROGRESS_BAR_REQUIRED,
                [],
                {},
                self.RESULT_IS_LIST,
                result_fn
            )
            return True
        self.no_auth()
        return False

    def create_region(self, region_info, result_fn=None):
        """ Create a new region by uploading suitable info.
        :param region_info: Suitable combination of information to create a new region.
        :param result_fn: Function to be called on completion of the job.
        """
        if self.set_authentication():
            self.job_queue.queue_job(
                api[REGIONS_API].create_region,
                tr('Creating region.'),
                self.PROGRESS_BAR_NOT_REQUIRED,
                deepcopy([region_info]),
                {},
                self.RESULT_IS_SINGLE,
                result_fn
            )
            return True
        self.no_auth()
        return False

    def create_backscatter_request(self, request_info, result_fn=None):
        """ Request that a backscatter product gets created.
        :param request_info: All of the information required to make the product request.
        :param result_fn: Function to be called on completion of the job.
        """
        if self.set_authentication():
            args = deepcopy([request_info])
            kwargs = {}
            self.job_queue.queue_job(
                api[BACKSCATTER_API].create_backscatter_request,
                tr('Generating product.'),
                self.PROGRESS_BAR_NOT_REQUIRED,
                args,
                kwargs,
                self.RESULT_IS_SINGLE,
                result_fn
            )
            return True
        self.no_auth()
        return False

    def create_optical_request(self, request_info, result_fn=None):
        """ Request that a backscatter product gets created.
        :param request_info: All of the information required to make the product request.
        :param result_fn: Function to be called on completion of the job.
        """
        if self.set_authentication():
            self.job_queue.queue_job(
                api[OPTICAL_API].create_optical_request,
                tr('Generating product.'),
                self.PROGRESS_BAR_NOT_REQUIRED,
                deepcopy([request_info]),
                {},
                self.RESULT_IS_SINGLE,
                result_fn
            )
            return True
        self.no_auth()
        return False

    def all_product_requests(self, completed, result_fn):
        """ Request that a product gets created.
        :param completed: Indicates whether to restrict products listed to ones that are complete, or show everything.
        :param result_fn: Function to be called on completion of the job.
        """
        if self.set_authentication():
            args = []
            kwargs = {}
            if completed:
                kwargs['completed'] = str(completed).lower()

            self.job_queue.queue_job(
                api[MISCELLANEOUS_API].all_product_requests,
                tr('Downloading products list.'),
                self.PROGRESS_BAR_REQUIRED,
                args,
                deepcopy(kwargs),
                self.RESULT_IS_LIST,
                result_fn
            )
            return True
        self.no_auth()
        return False

    def _download_hook(self, count, block_size, total_size):
        """ Handle progress feedback from urlretrieve().
        :param count: Number of blocks transferred so far.
        :param block_size: Block size in bytes.
        :param total_size: Size of file in bytes.
        """
        # assert self.download_thread is not None, tr('No active download.')
        self.progress_signal.emit(self.download_queue.thread.message_widget, count*block_size, total_size)

    def download_product(self, url, full_filename, result_fn, ident=None):
        """ Request that a product gets downloaded using the download thread.
        :param url: URL to download from.
        :param full_filename: Pathname to download to.
        :param result_fn: Function to be called on completion of the job.
        :param ident: Identifies the call that produced the result.
        """
        if self.set_authentication():
            args = [deepcopy(url), deepcopy(full_filename), self._download_hook]
            kwargs = {}

            self.download_queue.queue_job(
                urllib.urlretrieve,
                tr('Downloading product.'),
                self.PROGRESS_BAR_REQUIRED,
                args,
                kwargs,
                self.RESULT_IS_SINGLE,
                result_fn,
                ident=ident
            )
            return True
        self.no_auth()
        return False

    def scraper_login(self, scraper, result_fn):
        """ Request that a product gets downloaded.
        :param scraper: ScrapeTool to initiate session with.
        :param result_fn: Function to be called on completion of the job.
        """
        if self.set_authentication():
            args = [self.username, self.password]
            kwargs = {}

            self.download_queue.queue_job(
                scraper.login,
                tr('Log in and fetch list of map layers.'),
                self.PROGRESS_BAR_REQUIRED,
                deepcopy(args),
                kwargs,
                self.RESULT_IS_SINGLE,
                result_fn,
                scraper
            )
            return True
        self.no_auth()
        return False

    def scraper_download(self, scraper, layer_name, tile_ident, result_fn=None):
        """ Request that a free composite image be downloaded.
        :param scraper: Scraper to use
        :param layer_name: Name of the layer we want
        :param tile_ident: Identifier of the tile we want the image for.  A string of 5 digits with leading zeros.
        :param result_fn: Function to be called on completion of the job.
        """
        if self.set_authentication():
            args = [layer_name, tile_ident]
            kwargs = {}

            self.download_queue.queue_job(
                scraper.download,
                (tr('Info', 'Short version of Information'), tr('Download {0}_{1}.')),
                self.PROGRESS_BAR_REQUIRED,
                deepcopy(args),
                kwargs,
                self.RESULT_IS_SINGLE,
                result_fn,
                scraper
            )
            return True
        self.no_auth()
        return False

    def get_download_url(self, ident, result_fn, result_ident=None):
        """ Request a download url for a product (or products)
        :param ident: Identifier of a product for which we want a download URL.
        :param result_fn: Function to be called on completion of the job.
        :param result_ident: Identifier to help process the resulting URLs.
        """
        if not result_ident:
            result_ident = ident

        if self.set_authentication():
            args = [ident]
            kwargs = {}
            self.job_queue.queue_job(
                api[MISCELLANEOUS_API].get_download_url,
                tr('Requesting download url.'),
                self.PROGRESS_BAR_NOT_REQUIRED,
                deepcopy(args),
                kwargs,
                self.RESULT_IS_SINGLE,
                result_fn,
                ident=result_ident
            )
            return True
        self.no_auth()
        return False

    def sync(self, ident, result_fn):
        """ Request a download url for a product (or products)
        :param ident: Identifier of a product for which we want a download URL.
        :param result_fn: Function to be called on completion of the job.
        """
        args = []
        kwargs = {}
        self.job_queue.queue_job(
            None,
            'Sync.',    # Should never actually appear.
            self.PROGRESS_BAR_NOT_REQUIRED,
            args,
            kwargs,
            self.RESULT_IS_SINGLE,
            result_fn,
            ident=ident
        )
        return True

    def no_auth(self):
        """ Show a 'No authorization' message. """
        self.main_dlg.show_message(tr('Please set username and password before continuing.'))

    # === End of threaded network calls ========================================

    def clear_layout(self, layout):
        while layout.count():
            child = layout.takeAt(0)
            if child.widget() is not None:
                child.widget().deleteLater()
            elif child.layout() is not None:
                self.clear_layout(child.layout())

    def get_layers(self, result_fn):
        if self.set_authentication():
            self.job_queue.queue_job(
                api[REGIONS_API].list_regions,
                tr('Downloading regions.'),
                self.PROGRESS_BAR_REQUIRED,
                [],
                {},
                self.RESULT_IS_LIST,
                result_fn
            )
            return True
        self.no_auth()
        return False