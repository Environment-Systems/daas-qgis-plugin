# -*- coding: utf-8 -*-
"""
/****************************************************************************
 This file is part of EnvSys plugin for QGIS
 Integration with Environment Systems Data Services.
                             -------------------
        begin                : 2017-12-12
        copyright            : (C) 2017 by Environment Systems Ltd.
        email                : dataservices@envsys.co.uk
        git sha              : $Format:%H$
 ****************************************************************************/

/****************************************************************************
 *                                                                          *
 *   EnvSys plugin is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 3 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   EnvSys plugin is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
 *   GNU General Public License for more details.                           *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with EnvSys plugin. If not, see <https://www.gnu.org/licenses/>. *
 *                                                                          *
 ****************************************************************************/
"""


import os
import zipfile
import urlparse
import errno
import datetime

from PyQt4.QtCore import Qt, pyqtSlot, QAbstractTableModel, QModelIndex
from PyQt4.QtGui import QWidget, QSortFilterProxyModel, QColor
from PyQt4 import uic

import qgis
# from qgis.core import QgsRasterLayer

from translate import tr

# from log import Log
# debug = Log('Products')

REQUEST_DATE_FORMAT = '%Y-%m-%dT%H:%M:%S.%fZ'
DISPLAY_DATE_HM_FORMAT = '%B %d, %Y, %I:%M %p'
DISPLAY_DATE_HMS_FORMAT = '%B %d, %Y, %I:%M:%S.%f %p'

AVAILABLE_STATE = 'Available'
IN_PROGRESS_STATE = 'In Progress'
ERROR_STATE = 'Error'

BACKSCATTER_TYPE = 'Backscatter Imagery'
OPTICAL_TYPE = 'Optical Imagery'
OPTICAL_DERIVED_TYPE = 'Optical-derived Imagery'

class MyTableModel(QAbstractTableModel):

    '''
    Website columns:
        col #0: "Filter by region"
            Optical Derived - NRI
            Feb. 17, 2018, 11:33 a.m.
            User Region: home
            Cloud Mask: True
            Projection: OSGB 1936 / British National Grid
        col #1  "Type"
            Backscatter Imagery, Optical Imagery, Optical-derived Imagery
        col #2: "Status"
            Available, In Progress, Historical, Error
        col #3: "Requested"
            2 weeks ago
        col #4: "Actions"
            Download, delete, archive
    '''
    DESCRIPTION_COL = 0
    TYPE_COL = 1
    STATUS_COL = 2
    REQUESTED_COL = 3
    NUM_COLUMNS = 4     # Don't include an "Actions" column.

    def __init__(self, parent, all_products, header):
        """ Constructor
        :param parent: Parent node in the data.  Not really applicable for tables.
        :param all_products: Data to be displayed.
        :param header: Column titles.
        """
        QAbstractTableModel.__init__(self, parent)
        self.all_products = all_products
        self.header = header

    def rowCount(self, parent=None, *args, **kwargs):
        """ Specify the number of rows in the table.
        :param parent: Parent node in the data.  Not really applicable for tables.
        :param args: Additional positional arguments
        :param kwargs: Additional keyword arguments
        """
        return len(self.all_products)

    def columnCount(self, parent=None, *args, **kwargs):
        """ Specify the number of columns in the table.
        :param parent: Parent node in the data.  Not really applicable for tables.
        :param args: Additional positional arguments
        :param kwargs: Additional keyword arguments
        :return:    int 
        """
        return MyTableModel.NUM_COLUMNS

    def data(self, model_index, role=None):
        """ Fetch a data item.
        :param model_index: Identifies a product request.
        :param role: : Role for which the access is happening.
            We are generally only interested in the Display role and the CheckState role for DESCRIPTION_COL.
        :return: 
        """
        if not model_index.isValid():
            return None

        col = model_index.column()
        if Qt.CheckStateRole == role:
            if MyTableModel.DESCRIPTION_COL == col:
                state = Qt.Checked if self.all_products[model_index.row()].checked else Qt.Unchecked
                return state
            return None

        if Qt.DisplayRole == role:
            if MyTableModel.DESCRIPTION_COL == col:
                return self.all_products[model_index.row()].description
            elif MyTableModel.TYPE_COL == col:
                return self.all_products[model_index.row()].type
            elif MyTableModel.STATUS_COL == col:
                return self.all_products[model_index.row()].status
            elif MyTableModel.REQUESTED_COL == col:
                return self.all_products[model_index.row()].request_time_str

        if Qt.ToolTipRole == role:
            if MyTableModel.STATUS_COL == col\
                and ERROR_STATE == self.all_products[model_index.row()].status\
            :
                return self.all_products[model_index.row()].request['error_message']
            elif MyTableModel.REQUESTED_COL == col:
                return self.all_products[model_index.row()].request_exact_time_str

        if Qt.BackgroundColorRole == role:
            if 'Available' == self.all_products[model_index.row()].status:
                return QColor(128, 255, 128)
        return None

    def setData(self, model_index, val, role=None):
        """ Alter the data through the table.  Only used to select available products.
        :param model_index: Identifies a product request.
        :param val: New value of the data item.
        :param role: Role in which the update is happening.  Only perform any action for display role.
        :return: 
        """
        if Qt.CheckStateRole == role and MyTableModel.DESCRIPTION_COL == model_index.column():
            self.all_products[model_index.row()].checked = (Qt.Checked == val)
            self.dataChanged.emit(model_index, model_index)
            return True
        return False

    def flags(self, model_index):
        """ Indicates whether it is possible to select this product request.
        :param model_index: Identifies a product request.
        :return: True if the product can be selected.
        """
        f = super(MyTableModel, self).flags(model_index)
        if MyTableModel.DESCRIPTION_COL == model_index.column():
            status_index = self.createIndex(model_index.row(), MyTableModel.STATUS_COL)
            data = self.data(status_index, Qt.DisplayRole)
            if data and "Available" == data:
                f |= Qt.ItemIsUserCheckable
        return f

    def headerData(self, col, orientation, role=None):
        """ Return column header titles.
        :param col: Column number for which a title is required.
        :param orientation: ?
        :param role: Only required for display role.
        :return: Column title.
        """
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self.header[col]
        return None

    def sort(self, col, order=None):
        """ Sort table
        :param col: column to sort on.
        :param order: ascending or descending.
        """
        self.layoutAboutToBeChanged.emit()
        if MyTableModel.DESCRIPTION_COL == col:
            self.all_products = sorted(self.all_products, key=lambda item: item.description)
        elif MyTableModel.TYPE_COL == col:
            self.all_products = sorted(self.all_products, key=lambda item: item.type)
        elif MyTableModel.STATUS_COL == col:
            self.all_products = sorted(self.all_products, key=lambda item: item.status)
        elif MyTableModel.REQUESTED_COL == col:
            self.all_products = sorted(self.all_products, key=lambda item: item.request_time_dt)

        if order == Qt.DescendingOrder:
            self.all_products.reverse()
        self.layoutChanged.emit()

    def sync(self):
        """ Synchronize the display with the data. """
        self.beginResetModel()
        self.endResetModel()

        top_left = self.index(0, 0, QModelIndex())
        bottom_right = self.index(self.rowCount(), self.columnCount(), QModelIndex())
        self.dataChanged.emit(top_left, bottom_right)


class MyFilter(QSortFilterProxyModel):
    def __init__(self):
        """ Constructor. """
        super(MyFilter, self).__init__()

        self.show_backscatter = True
        self.show_optical = True
        self.show_optical_derived = True

        self.show_available = True
        self.show_in_progress = False
        self.show_other = False

    def filterAcceptsRow(self, source_row, source_parent):
        """ Indicate whether to show this row.
        :param source_row: Which row we are talking about.
        :param source_parent: Not used.  Parent only matters for tree-structured data.
        """
        # Can't filter by region because we don't really have that information.
        # We could probably extract it from the URL in available products but
        # we would still have no clues for any other product requests.

        # Filter by type
        index = self.sourceModel().index(source_row, MyTableModel.TYPE_COL, source_parent);
        type = self.sourceModel().data(index, Qt.DisplayRole)
        show = (BACKSCATTER_TYPE == type and self.show_backscatter)\
            or (OPTICAL_TYPE == type and self.show_optical)\
            or (OPTICAL_DERIVED_TYPE == type and self.show_optical_derived)
        if not show:
            return False

        # Filter by status
        index = self.sourceModel().index(source_row, MyTableModel.STATUS_COL, source_parent);
        status = self.sourceModel().data(index, Qt.DisplayRole)
        show = self.show_available and 'Available' == status \
            or \
                self.show_in_progress and 'In Progress' == status \
                or (
                    self.show_other
                    and not 'In Progress' == status
                    and not 'Available' == status
                )
        return show


class DataItem(object):
    DOWNLOADS_DIR = ''

    def __init__(self,  request):
        """ Constructor.
        :param request: Product-request data from server.
        """
        self.request = request
        self.checked = False
        self.description = '{} - {}'.format(request['id'], request['type'])
        self.type = request['type']
        self.status = request['status']
        self.request_time_dt = datetime.datetime.strptime(request['request_time'], REQUEST_DATE_FORMAT)
        self.request_time_str = self.request_time_dt.strftime(DISPLAY_DATE_HM_FORMAT)
        self.request_exact_time_str = self.request_time_dt.strftime(DISPLAY_DATE_HMS_FORMAT)
        self.url = ''
        self.filename = ''

    def set_url(self, url):
        """ Set URL and filename for this product request.
        :param url: Download ULR.
        :return: 
        """
        self.url = url
        name = os.path.basename(urlparse.urlparse(url).path)
        root, ext = os.path.splitext(name)  # Separate name from '.zip' extension.
        # See whether the product has been downloaded before so that we don't do unnecessary downloads.
        download_filename = os.path.join(DataItem.DOWNLOADS_DIR, name)
        if os.path.isfile(download_filename):
            self.filename = download_filename
        self.description = '{} - {}'.format(self.request['id'], root)


class ProductsWidget(QWidget):
    """ Widget for downloading available products. """

    def __init__(self, utils,  parent=None):
        """ Constructor.
        :param utils: Common utilities object giving access to network calls etc.
        :param parent: Widget that this widget is a child of.
        """
        QWidget.__init__(self, parent)
        self.utils = utils
        self.ui = uic.loadUi(os.path.expanduser(os.path.join(os.path.dirname(__file__), 'products.ui')), self)
        self.ui.show()

        self.num_selected = 0

        self.header = ['Description', 'Type', 'Status', 'Requested']
        self.table_model = MyTableModel(self, [], self.header)
        self.table_model.dataChanged.connect(self.dataChanged)

        self.filter = MyFilter()
        self.filter.setDynamicSortFilter(True)
        self.filter.setSourceModel(self.table_model)
        self.filter.show_available = self.show_available_checkBox.isChecked()
        self.filter.show_in_progress = self.show_in_progress_checkBox.isChecked()
        self.filter.show_other = self.show_other_checkBox.isChecked()

        self.show_backscatter_checkBox.setChecked(self.filter.show_backscatter)
        self.show_optical_checkBox.setChecked(self.filter.show_optical)
        self.show_optical_derived_checkBox.setChecked(self.filter.show_optical_derived)

        self.show_available_checkBox.setChecked(self.filter.show_available)
        self.show_in_progress_checkBox.setChecked(self.filter.show_in_progress)
        self.show_other_checkBox.setChecked(self.filter.show_other)

        self.product_tableView.setModel(self.filter)
        self.product_tableView.resizeColumnsToContents()
        self.product_tableView.setSortingEnabled(True)

    def tab_shown(self):
        """ Initialization each time the tab is selected. """
        self.refresh_product_list()


    @pyqtSlot(QModelIndex, QModelIndex)
    def dataChanged(self, top_left, bottom_right):
        """ When the selections change, update the product counts.
        :param top_left: Unused.  Top left changed grid in the table
        :param bottom_right: Unused.  Bottom right changed grid in the table
        """
        self.show_num_selected()

    def show_num_selected(self):
        """ Show the number of selected products and the number of available products. """
        num_available = 0
        num_selected = 0
        if self.show_available_checkBox.isChecked():
            for product in self.table_model.all_products:
                if 'Available' == product.status:
                    num_available += 1
                    if product.checked:
                        num_selected += 1
        self.selected_label.setText(tr('{} of {} selected').format(num_selected, num_available))

        if 0 == num_selected:
            self.select_all_checkBox.setCheckState(Qt.Unchecked)
        elif num_selected == num_available:     # '0 of 0' should leave "Select all" unchecked.
            self.select_all_checkBox.setCheckState(Qt.Checked)
        else:
            self.select_all_checkBox.setCheckState(Qt.PartiallyChecked)

        self.show_btn.setEnabled(num_selected > 0)

    def refresh_product_list(self):
        self.utils.check_authentication_ok()
        self.table_model.all_products = []
        self.table_model.sync()
        self.product_tableView.clearSelection()

        """ Download list of product requests. """
        if not (self.filter.show_backscatter or self.filter.show_optical or self.filter.show_optical_derived):
            self.show_backscatter_checkBox.setChecked(True)
            self.filter.show_backscatter = True

        if not (self.filter.show_available or self.filter.show_in_progress or self.filter.show_other):
            self.show_available_checkBox.setChecked(True)
            self.filter.show_available = True

        completed = self.filter.show_available and not self.filter.show_in_progress and not self.filter.show_other
        self.utils.all_product_requests(completed, self.product_requests_results)

    def product_requests_results(self, completed_ok, all_product_requests):
        """ Fetch download URLs for all the 'Available' products.
        :param completed_ok: Whether the list of products was fetched successfully or not.
        :param all_product_requests: If successful, the list of products.
        """
        if completed_ok:
            self.table_model.all_products = []
            for request in all_product_requests:
                r = DataItem(request)
                self.table_model.all_products.append(r)

                if self.filter.show_available and 'Available' == request['status']:
                    self.utils.get_download_url(request['id'], self.download_url_result)
            self.utils.sync(0, self.sync_results)

    def download_url_result(self, completed_ok, url, ident):
        """ Add the download url to the product info.
        : param completed_ok: True if we got the download URL successfully.
        : param url: The download URL for the product-of-interest
        : param ident:  The identifier of the product-of-interest.
        """
        if completed_ok:
            DataItem.DOWNLOADS_DIR = self.utils.downloads_dir
            # Find the product this is the url for.
            for product in self.table_model.all_products:
                if ident == product.request['id']:
                    product.set_url(url)
                    break

    def sync_results(self, ident):
        """ Update the list of product selectors.
        All the URLs should have been received at this point.
        Now we can show product with sensible names.
        :param ident: Unused.  Exists in case we need to distinguish between multiple sync events.
        """
        self.table_model.sync()

        # We want to display a message if the matching results list is empty, i.e. if the table is empty.
        # We don't care whether there is data in the model - only whether anything is shown in the table.
        # Qt doesn't provide a QTableView.rowCount().  Our workaround is to try and select row 0 in the table.
        # If the number of items selected is zero, the table is empty.
        self.product_tableView.selectRow(0)
        table_is_empty = not self.product_tableView.selectedIndexes()
        self.product_tableView.clearSelection()
        if table_is_empty:
            self.product_tableView.hide()
            self.no_products_label.show()
        else:
            self.product_tableView.show()
            self.product_tableView.resizeColumnsToContents()
            self.no_products_label.hide()

    # noinspection PyPep8Naming
    @pyqtSlot(bool)
    def on_show_backscatter_checkBox_clicked(self, checked):
        """ Handle user clicking the 'Backscatter' checkbox.
        :param checked: New state of the checkbox.
        """
        self.filter.show_backscatter = checked
        self.refresh_product_list()    # Refresh.

    # noinspection PyPep8Naming
    @pyqtSlot(bool)
    def on_show_optical_checkBox_clicked(self, checked):
        """ Handle user clicking the 'Optical' checkbox.
        :param checked: New state of the checkbox.
        """
        self.filter.show_optical = checked
        self.refresh_product_list()    # Refresh.

    # noinspection PyPep8Naming
    @pyqtSlot(bool)
    def on_show_optical_derived_checkBox_clicked(self, checked):
        """ Handle user clicking the 'Optical-derived' checkbox.
        :param checked: New state of the checkbox.
        """
        self.filter.show_optical_derived= checked
        self.refresh_product_list()    # Refresh.

    # noinspection PyPep8Naming
    @pyqtSlot(bool)
    def on_show_available_checkBox_clicked(self, checked):
        """ Handle user clicking the 'Available' checkbox.
        :param checked: New state of the checkbox.
        """
        self.filter.show_available = checked
        self.refresh_product_list()    # Refresh.

    # noinspection PyPep8Naming
    @pyqtSlot(bool)
    def on_show_in_progress_checkBox_clicked(self, checked):
        """ Handle user clicking the 'In Progress' checkbox.
        :param checked: New state of the checkbox.
        """
        self.filter.show_in_progress = checked
        self.refresh_product_list()    # Refresh.

    # noinspection PyPep8Naming
    @pyqtSlot(bool)
    def on_show_other_checkBox_clicked(self, checked):
        """ Handle user clicking the 'Other' checkbox.
        :param checked: New state of the checkbox.
        """
        self.filter.show_other = checked
        self.refresh_product_list()    # Refresh.

    # noinspection PyPep8Naming
    def on_select_all_checkBox_clicked(self):
        """ Called when the user clicks on the 'all products' checkbox.
        Set the state of all of the individual product selector checkboxes.
        """
        initial_state = self.select_all_checkBox.checkState()
        state = Qt.Unchecked if Qt.Unchecked == initial_state else Qt.Checked
        if not state == initial_state:
            self.select_all_checkBox.setCheckState(state)
        state = (Qt.Checked == state)
        for product in self.table_model.all_products:
            if 'Available' == product.status:
                product.checked = state
        self.table_model.sync()

    def download_product(self, completed_ok, url, ident, result_fn=None):
        """ Having fetched a URL to download from, we can now download the product
        :param completed_ok: Indicates whether a URL was obtained.
        :param url: The URL, if completed_ok.
        :param ident: An identifier to keep track of where to store information.
        :param result_fn: Function to call when the product has been downloaded.
        This will be 'None' if we are just downloading,
        or 'show_product()' if we are to show it when downloaded.
        """
        # Downloading products is expected to take a while so the flashing messages should be ok.
        url_components = urlparse.urlsplit(url)
        path, filename = os.path.split(url_components.path)
        full_filename = os.path.join(self.utils.downloads_dir, filename)
        for product in self.table_model.all_products:
            if ident == product.request['id']:
                product.filename = full_filename
                product.url = url
                self.utils.download_product(url, full_filename, result_fn, ident)
                break

    def on_show_btn_pressed(self):
        """ Handle pressing the show button. """
        # Ensure all checked products have been downloaded but don't download anything we don't have to.
        for product in self.table_model.all_products:
            if product.checked:
                if product.filename and os.path.isfile(product.filename):
                    # Show the product.
                    self.show_product(True, None, product.request['id'])
                else:
                    # Download the product and then show it.
                    self.download_product(True, product.url, product.request['id'], self.show_product)

    # noinspection PyMethodMayBeStatic
    def show_product(self, completed_ok, result, ident):
        """ Unzip a product and load it as a raster layer.
        :param completed_ok: Indicates whether the product was downloaded successfully.
        :param result: Nominal result of doing the download.  'None' if successful.  Unused.
        :param ident: Identifier to keep track of where to store information.
        """
        if completed_ok:
            for product in self.table_model.all_products:
                if ident == product.request['id']:
                    download_dir = os.path.dirname(product.filename)
                    filename = os.path.basename(product.filename)
                    name, extension = os.path.splitext(filename)

                    try:
                        os.mkdir(os.path.join(download_dir, name))
                    except OSError as e:
                        if errno.EEXIST != e.errno:
                            raise

                    assert os.path.isfile(product.filename), tr("Couldn't find downloaded product")
                    z = zipfile.ZipFile(product.filename)
                    for zname in z.namelist():
                        if '/' != zname[-1]:
                            f = z.open(zname)
                            content = f.read()
                            f.close()
                            with open(os.path.join(download_dir, name, os.path.basename(zname)), 'wb') as f:
                                f.write(content)

                    qgis.utils.iface.addRasterLayer(os.path.join(download_dir, name, name + '.tif'), name)
                    break

    # noinspection PyMethodMayBeStatic
    def help(self):
        """ Show help on this widget. """
        dlg = self.parent().parent().parent().parent()
        return dlg.show_help(tr('Products help'), 'products.html')
