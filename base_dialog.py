# -*- coding: utf-8 -*-
"""
/****************************************************************************
 This file is part of EnvSys plugin for QGIS
 Integration with Environment Systems Data Services.
                             -------------------
        begin                : 2017-12-12
        copyright            : (C) 2017 by Environment Systems Ltd.
        email                : dataservices@envsys.co.uk
        git sha              : $Format:%H$
 ****************************************************************************/

/****************************************************************************
 *                                                                          *
 *   EnvSys plugin is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 3 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   EnvSys plugin is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
 *   GNU General Public License for more details.                           *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with EnvSys plugin. If not, see <https://www.gnu.org/licenses/>. *
 *                                                                          *
 ****************************************************************************/
"""


from locale import getlocale
from ConfigParser import SafeConfigParser

from qgis.gui import QgsMessageBar
from qgis.utils import *
#try:
#    # For testing.
#    from dummy_main_dialog import QgsMessageBarItem
#except:
from qgis.gui import QgsMessageBarItem


from PyQt4 import QtGui, uic
from PyQt4.QtGui import QMessageBox, QDialogButtonBox, QSizePolicy, QProgressBar, QCheckBox
from PyQt4.QtCore import pyqtSlot, Qt
from translate import tr

from settings import SettingsWidget
from search import SearchWidget
from products import ProductsWidget
# from zonal_stats import ZonalStatsWidget
from free import FreeWidget
from utils import Utils

# from log import Log
# debug = Log('Base')

# This translation is used in testing
dummy = tr('Good morning')

FORM_CLASS, _ = uic.loadUiType(os.path.join(os.path.dirname(__file__), 'envsys_dialog_base.ui'))


class BaseDialog(QtGui.QDialog, FORM_CLASS):
    """ The top level dialog, providing a tab control with places for each 'page' of the plugin."""

    show_progress = pyqtSlot(QgsMessageBarItem, int, int)
    on_message_bar_widgetAdded = pyqtSlot(QgsMessageBarItem)
    on_message_bar_widgetRemoved = pyqtSlot()   # QgsMessageBarItem)    # Documented but doesn't work.

    def __init__(self, utils, parent=None):
        """Constructor."""
        super(BaseDialog, self).__init__(parent)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://doc.qt.io/qt-5.10/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect

        self.setupUi(self)
        # self.setWindowFlags(Qt.WindowStaysOnTopHint)  # Can't find WindowStaysOnTopHint.

        config = SafeConfigParser()
        config.read(os.path.join(os.path.dirname(__file__), 'metadata.txt'))
        self.plugin_version = config.get('general', 'version') + ' beta' * bool(config.get('general', 'experimental'))
        self.setWindowTitle('Data Services {}'.format(self.plugin_version))
        utils.homepage = config.get('general', 'homepage')
        config = None
        self.gui_active = False

        # Auto-connect is somehow causing this function to report errors in testing.
        # Manually connect so that we know that the instance has been created before the function is called.

        help_button = self.help_button_box.button(QDialogButtonBox.Help)
        help_button.setAutoDefault(False)
        help_button.clicked.connect(self.help)

        close_button = self.button_box.button(QDialogButtonBox.Close)
        close_button.setAutoDefault(False)

        self.settings_btn.setAutoDefault(False)

        # ------------------------------------------------------
        self.message_stack = []
        self.canceled_messages = []
        self.message_bar = QgsMessageBar()
        self.message_bar.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        self.layout().insertWidget(0, self.message_bar)     # Insert message bar at the top of the dialog.
        self.message_bar.widgetAdded.connect(self.on_message_bar_widgetAdded)
        self.message_bar.widgetRemoved.connect(self.on_message_bar_widgetRemoved)
        self.num_progress_calls = 0     # Helps with testing.
        self.last_progress_val = None
        self.last_progress_total = None
        # ------------------------------------------------------

        # self.setWindowIcon(QIcon(':/plugins/Envsys/favicon-32x32.png'))     # Doesn't show up. :-(
        # self.setStyleSheet("background-image:url(android-chrome-512x512.png);")

    # noinspection PyPep8Naming
    @pyqtSlot(int)
    def plugin_tabs_currentChanged(self, index):
        """ User has clicked on a tab but the docs are unclear about whether
        the tab has been changed or not at this point.  I think it has.
        In any case, close down the old tab and initialise the newly selected tab.
        :param index: The index of the selected tab.
        """
        old_widget = self.plugin_tabs.widget(self.current_tab)
        try:
            old_widget.tab_hidden()
        except AttributeError:
            pass    # No tab_hidden handler for this tab.

        self.current_tab = index
        new_widget = self.plugin_tabs.widget(self.current_tab)
        try:
            new_widget.tab_shown()
        except AttributeError:
            pass    # No tab_shown handler for this tab.

    def show_message(self, msg, level=QgsMessageBar.INFO, title=tr('Information'), duration=0):
        """ Show a message in the message pane.
        :param msg: Text message to be shown.
        :param level: Indicates message importance using colour - blue, yellow, red.
        :param title: Message prefix.
        :param duration: Duration the message is to be displayed for. 0=indefinite.
        See https://docs.qgis.org/2.14/en/docs/pyqgis_developer_cookbook/communicating.html
        """
        # We could put a message on the main QGIS message bar like this:
        #   iface.messageBar().pushMessage(title, msg, level, duration)  # level = (INFO, WARNING, CRITICAL, SUCCESS)

        # Put a message on the plugin's own message bar.
        self.message_bar.pushMessage(title, msg, level, duration)
        self.message_bar.repaint()
        # We could log messages like this:
        #   log_name = tr('EnvSys plugin')  # Or just 'Plugins'
        #   QgsMessageLog.logMessage(msg, log_name, level)  # level = (INFO, WARNING, CRITICAL)

    def show_cancel_message(
            self,
            msg,
            level=QgsMessageBar.INFO,
            title=tr('Information'),
            progress_bar_required=False
    ):
        """ Show a message with a cancel button and, optionally, a progress bar.
        :param msg: Text message to be shown.
        :param level: Indicates message importance using colour - blue, yellow, red.
        :param title: Message prefix.
        :param progress_bar_required:  True if a progress bar is required.
        :return: The message widget that gets created.
        """
        widget = self.message_bar.createMessage(title, msg)
        button = QPushButton(widget)
        button.setText(tr('Cancel'))
        button.pressed.connect(self.cancel_fn)
        widget.layout().addWidget(button)
        if progress_bar_required:
            progress_bar = QProgressBar(widget)
            progress_bar.setObjectName('envsys_progress_bar')
            # We don't know how many items need to be downloaded
            # until we have completed the first network request.
            progress_bar.setRange(0, 0)     # Show 'busy' until we know what the range will be.
            progress_bar.setFormat('%v of %m')  # e.g. show '5 of 27' instead of '18%'
            widget.layout().addWidget(progress_bar)

        # I have a choice here of using "noinspection PyArgumentList" to suppress all argument checking
        # or just letting PyCharm flag duration as an "unexpected argument".  It is a valid argument.
        self.message_bar.pushWidget(widget, level, duration=0)
        # The message stays until the caller removes it by calling remove_message().
        self.message_bar.repaint()
        return widget

    @pyqtSlot()
    def cancel_fn(self):
        """ Handle user pressing the cancel button on a cancellable message. """
        cancel_btn = self.sender()
        cancel_btn.setEnabled(False)
        message_widget = cancel_btn.parent()
        self.utils.stop_thread_for(message_widget)

    # noinspection PyPep8Naming
    @pyqtSlot(QgsMessageBarItem)
    def on_message_bar_widgetAdded(self, message_widget):
        """ Handles widgetAdded signal from message bar.
        Push the new message widget on to the stack so that we can keep track of what is being displayed.
        :param message_widget: Message that has been added to the message bar.
        """
        if message_widget not in self.message_stack:
            self.message_stack.append(message_widget)
        else:
            # Phantom on_message_bar_widgetAdded() for something that has already been added.
            pass

    # noinspection PyPep8Naming
    @pyqtSlot()     # QgsMessageBarItem)
    def on_message_bar_widgetRemoved(self):     # , message_widget):
        """ Handles widgetRemoved signal from message bar.
        Pop the last message widget from the stack so that we can keep track of what is being displayed.
        : param message_widget: Message that has been removed from the message bar.
        Unfortunately, specifying this parameter causes QGIS to crash.
        Whether we use the parameter or not, we never get into this function.
        The item removed will generally be what we have on top of our message stack.
        """
        if self.message_stack:
            message_widget = self.message_stack.pop()
            if message_widget in self.canceled_messages:
                self.canceled_messages.remove(message_widget)

            next_item = self.message_bar.currentItem()
            if next_item in self.canceled_messages:
                self.message_bar.popWidget()    # Will cause a further widgetRemoved() signal,
            #                                     which may lead to further removals.
            self.utils.message_bar_widgetRemoved(message_widget)

    @pyqtSlot(QgsMessageBarItem, int, int)
    def show_progress(self, message_widget, val, total):
        """ Update the progress bar in a cancellable message.
        :param message_widget: The cancellable message widget that is to be updated.
        :param val: The value to set the progress bar to.
        :param total: The maximum value of the progress bar.
        We generally don't know this value until we get the first result from a query.
        """
        self.num_progress_calls += 1    # Helps with testing.
        self.last_progress_val = val
        self.last_progress_total = total

        # Find progress bar.
        if message_widget:
            progress_bar = message_widget.findChild(QProgressBar)
            if progress_bar:
                progress_bar.setMaximum(total)
                progress_bar.setValue(val)
                message_widget.update()

    def remove_message(self, message_widget):
        """ Remove a message widget from the message bar.
        :param message_widget: Widget to be removed from the message bar.
        """
        if self.message_stack and message_widget is self.message_stack[-1]:
            self.message_bar.popWidget()
        elif message_widget:    # Just in case we're told to remove None.
            self.canceled_messages.append(message_widget)

        next_item = self.message_bar.currentItem()
        if next_item in self.canceled_messages:
            self.message_bar.popWidget()    # Will cause a further widgetRemoved() signal,
        #                                     which may lead to further removals.

    def help(self):
        """ When help is requested, pass the request on to whatever widget is currently showing. """
        w = self.plugin_tabs.currentWidget()
        try:
            w.help()
        except Exception as e:
            self.show_message(tr('Error showing help.\n{}').format(str(e)))

    # noinspection PyMethodMayBeStatic
    def show_help(self, title, help_filename, buttons=None):
        """ Common code for opening a help file.
        EnvsysDialog.help() calls <TabWidget>.help(), which calls EnvsysDialog.show_help().
        Opens a QMessageBox to show HTML help.
        :param title: Title for message box.
        :param help_filename: Just the name of the help file, which is assumed
        to be in <plugin_root>/envsys_help/en_GB/ (or other locale directory).
        :param buttons: Set of buttons to show if the caller wants feedback.
        """
        msg = QMessageBox()
        locale = getlocale()
        if not locale or locale[0] is None:
            locale = ('en_GB', 'UTF-8')
        base_dir = os.path.dirname(__file__)
        src_file = os.path.join(base_dir, 'envsys_help', locale[0], help_filename)
        if os.path.isfile(src_file):
            if buttons:
                msg.setStandardButtons(buttons)
                msg.setIcon(QMessageBox.Question)
            msg.setWindowTitle(title)
            with open(src_file, 'r') as f:
                txt = f.read()
                msg.setText(txt)
                return msg.exec_()
        else:
            msg.setText(tr('Help for {} is not currently available').format(title))
            return msg.exec_()

    def show_tip_window(self):
        tip_msg = QMessageBox()
        checkbox = QCheckBox('Show tip on startup')
        checkbox.isChecked()
        tip_msg.layout().addWidget(checkbox)

        locale = getlocale()
        if not locale or locale[0] is None:
            locale = ('en_GB', 'UTF-8')
        base_dir = os.path.dirname(__file__)
        src_file = os.path.join(base_dir, 'envsys_help', locale[0], 'tip.html')
        tip_msg.setWindowTitle(tr('EnvSys Tip'))

        with open(src_file, 'r') as f:
            txt = f.read()
            tip_msg.setText(txt)
            tip_msg.exec_()


class TipDialog(QtGui.QDialog):

    def __init__(self, utils, parent=None):
        super(TipDialog, self).__init__(parent)
        # super(TipDialog, self).__init__(utils, parent)
        self.utils = utils

        self.tip_msg = QMessageBox()
        self.checkbox = QCheckBox('Show tip on startup')
        self.checkbox.setChecked(True)
        self.tip_msg.layout().addWidget(self.checkbox)
        self.checkbox.stateChanged.connect(self.checkbox_selected)

        locale = getlocale()
        if not locale or locale[0] is None:
            locale = ('en_GB', 'UTF-8')
        base_dir = os.path.dirname(__file__)
        src_file = os.path.join(base_dir, 'envsys_help', locale[0], 'tip.html')
        self.tip_msg.setWindowTitle(tr('EnvSys Tip'))

        with open(src_file, 'r') as f:
            txt = f.read()
            self.tip_msg.setText(txt)

    def show_tip_window(self):
        return self.tip_msg.exec_()

    def checkbox_selected(self, state):
        if Qt.Unchecked == state:
            self.utils.config.set(self.utils.TIP_WINDOW, self.utils.TIP_DISPLAY_LABEL, 'False')
        elif Qt.Checked == state:
            self.utils.config.set(self.utils.TIP_WINDOW, self.utils.TIP_DISPLAY_LABEL, 'True')
        self.utils.save_config()
