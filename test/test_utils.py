# coding=utf-8
"""Utils test."""

__author__ = 'dataservices@envsys.co.uk'
__date__ = '2018-March-15'
__copyright__ = 'Copyright 2018, Environment Systems Ltd.'

import os
import sys
import unittest
import urlparse
from time import sleep
from ConfigParser import ConfigParser

# from PyQt4.QtCore import Qt
from PyQt4.QtGui import QApplication, QWidget

from qgis.gui import QgsMessageBar
from qgis.core import *
import qgis.utils

import os
import sys

def include_dir(directory):
    if directory not in sys.path:
        sys.path.insert(0, directory)


root_dir = os.path.abspath(os.path.dirname(__file__))
root_dir = os.path.split(root_dir)[0]
include_dir(root_dir)
include_dir(os.path.join(root_dir, 'extras'))
include_dir(os.path.join(root_dir, 'test'))

from utils import encrypt, decrypt, Utils
from envsys_dialog import EnvsysDialog
from scrape import ScrapeTool

#def include_dir(directory):
#    if directory not in sys.path:
#        sys.path.insert(0, directory)
#
#root_dir, filename = os.path.split(os.path.dirname(__file__))
#root_dir, last_dir = os.path.split(root_dir)
#include_dir(root_dir)
#include_dir(os.path.join(root_dir, 'extras'))
#include_dir(os.path.join(root_dir, 'test'))

import envsys_client
from envsys_client.api_client import ApiException
from envsys_client import NewRegion, BackscatterRequestInfo, OpticalRequestInfo

# from log import Log
# debug = Log('test_utils')

# APP = QApplication([])  # Create once.  Never any need to try and close it down.
from utilities import get_qgis_app
QGIS_APP = get_qgis_app()

ABER_NAME = 'Aber_test'
ABER_COORDS = [[
    [-4.101333618164063, 52.40359185857786],
    [-4.101333618164063, 52.41783281673367],
    [-4.046401977539063, 52.41783281673367],
    [-4.046401977539063, 52.40359185857786],
    [-4.101333618164063, 52.40359185857786]
]]
ABER_ID = None

class Test1Encryption(unittest.TestCase):
    """ Test encryption functions work. """

    def setUp(self):
        """ Runs before each test. """
        pass

    def tearDown(self):
        """ Runs after each test. """
        pass

    def test_encrypt(self):
        """ Test encryption. """
        self.assertEqual('SGVsbG8gd29ybGQu', encrypt('Hello world.'))

    def test_decrypt(self):
        """ Test decryption. """
        self.assertEqual('Hello world.', decrypt('SGVsbG8gd29ybGQu'))


class Test2Config(unittest.TestCase):
    """ Test config tab is properly set up. """

    def setUp(self):
        """ Runs before each test. """
        self.main_dlg = EnvsysDialog()
        self.utils = Utils(self.main_dlg)

    def tearDown(self):
        """ Runs after each test. """
        self.utils = None
        self.main_dlg = None

    def test_set_shapes_dir(self):
        """ Test set_shapes_dir(self, shapes_dir) """
        assert self.utils.shapes_dir
        assert os.path.isdir(self.utils.shapes_dir)

        self.config_filename = os.path.expanduser('~/.envsys_plugin')
        self.config = ConfigParser()
        self.config.read(self.config_filename)
        assert self.config.has_section(self.utils.WORK_DIRS_SECTION)
        original_config_shapes_dir = self.config.get(self.utils.WORK_DIRS_SECTION, self.utils.SHAPES_LABEL)
        assert original_config_shapes_dir == self.utils.shapes_dir

        self.utils.set_shapes_dir(original_config_shapes_dir + '_testing')

        self.config.read(self.config_filename)
        new_config_shapes_dir = self.config.get(self.utils.WORK_DIRS_SECTION, self.utils.SHAPES_LABEL)
        assert self.utils.shapes_dir == original_config_shapes_dir + '_testing'
        assert new_config_shapes_dir == self.utils.shapes_dir

        self.utils.set_shapes_dir(original_config_shapes_dir)

        self.config.read(self.config_filename)
        new_config_shapes_dir = self.config.get(self.utils.WORK_DIRS_SECTION, self.utils.SHAPES_LABEL)
        assert self.utils.shapes_dir == original_config_shapes_dir
        assert new_config_shapes_dir == self.utils.shapes_dir

    def test_set_downloads_dir(self):
        """ Test set_downloads_dir(self, downloads_dir) """
        assert self.utils.downloads_dir
        assert os.path.isdir(self.utils.downloads_dir)

        self.config_filename = os.path.expanduser('~/.envsys_plugin')
        self.config = ConfigParser()
        self.config.read(self.config_filename)
        assert self.config.has_section(self.utils.WORK_DIRS_SECTION)
        original_config_downloads_dir = self.config.get(self.utils.WORK_DIRS_SECTION, self.utils.DOWNLOADS_LABEL)
        assert original_config_downloads_dir == self.utils.downloads_dir

        self.utils.set_downloads_dir(original_config_downloads_dir + '_testing')

        self.config.read(self.config_filename)
        new_config_downloads_dir = self.config.get(self.utils.WORK_DIRS_SECTION, self.utils.DOWNLOADS_LABEL)
        assert self.utils.downloads_dir == original_config_downloads_dir + '_testing'
        assert new_config_downloads_dir == self.utils.downloads_dir

        self.utils.set_downloads_dir(original_config_downloads_dir)

        self.config.read(self.config_filename)
        new_config_downloads_dir = self.config.get(self.utils.WORK_DIRS_SECTION, self.utils.DOWNLOADS_LABEL)
        assert self.utils.downloads_dir == original_config_downloads_dir
        assert new_config_downloads_dir == self.utils.downloads_dir

    def test_save_config(self):
        """ Test save_config(self) """
        # Tested in test_set_shapes/downloads_dir()
        pass

    def test_set_authentication(self):
        """ Test set_authentication(self) """
        self.utils.username = 'qwerty'
        self.utils.password = 'asdfgh'
        assert self.utils.set_authentication()
        assert envsys_client.configuration.username == self.utils.username
        assert envsys_client.configuration.password == self.utils.password

        self.utils.username = 'asdfgh'
        self.utils.password = 'qwerty'
        assert self.utils.set_authentication()
        assert envsys_client.configuration.username == self.utils.username
        assert envsys_client.configuration.password == self.utils.password


@unittest.skip('QGIS testing is only practical from inside QGIS.')
class Test3Qgis(unittest.TestCase):
    def setUp(self):
        """ Runs before each test. """
        self.main_dlg = DummyMainDlg()
        self.utils = Utils(self.main_dlg)

        QgsApplication.setPrefixPath('/usr/share/qgis/', True)
        self.app = qgis.core.QgsApplication([], True)
        self.app.initQgis()

    def tearDown(self):
        """ Runs after each test. """
        # self.app.exitQgis()
        self.app = None

    @unittest.skip("Can't add layers to registry.")
    def test01_list_valid_vector_layers(self):
        """ Test list_valid_vector_layers(self) """
        print('test01_list_valid_vector_layers')

        # Doesn't work.  iface is None.
        # qgis.utils.iface.addVectorLayer(os.path.join(self.utils.shapes_dir, 'Aber.shp'), 'testlayer', 'ogr')
        # aLayer = qgis.utils.iface.activeLayer()

        # Doesn't work.  Unable to add layers to registry.
        layer1 = QgsVectorLayer(os.path.join(self.utils.shapes_dir, 'Aber.shp'), 'anewlayer', 'memory')
        assert None is not QgsMapLayerRegistry.instance().addMapLayer(layer1, False)
        layers = QgsMapLayerRegistry.instance().mapLayers()
        assert layers, 'Unable to add layers.'

        # Also need to add non-valid i.e. raster layers.

        valid_layers = self.utils.list_valid_vector_layers()
        assert valid_layers

    @unittest.skip("Can't add layers to registry.")
    def test02_list_vector_layers(self):
        """ Test list_vector_layers(self) """
        print('test02_list_vector_layers')

        pass

    def test03_make_geometry_from_coords(self):
        """ Test make_geometry_from_coords(self, coords, transform=None) """
        print('test03_make_geometry_from_coords')

        geom = self.utils.make_geometry_from_coords(ABER_COORDS)
        assert geom, geom
        assert isinstance(geom, QgsGeometry)

    def test04_make_layer_from_geometry(self):
        """ Test make_layer_from_geometry(self, name, zone, epsg=WGS84_EPSG) """
        print('test04_make_layer_from_geometry')

        geom = self.utils.make_geometry_from_coords(ABER_COORDS)
        layer = self.utils.make_layer_from_geometry(ABER_NAME, geom)
        assert layer, layer
        assert isinstance(layer, QgsVectorLayer), layer

    def test05_make_layer_from_coords(self):
        """ Test make_layer_from_coords(self, name, coords, transform=None, epsg=WGS84_EPSG): """
        print('test05_make_layer_from_coords')

        layer = self.utils.make_layer_from_coords(ABER_NAME, ABER_COORDS)
        assert layer, layer
        assert isinstance(layer, QgsVectorLayer), layer


class Test4Network(unittest.TestCase):
    """ Test networking functionality. """

    def setUp(self):
        """ Runs before each test. """
        self.completed_ok = None
        self.result = None
        self.ident = None

        self.main_dlg = EnvsysDialog()
        self.utils = Utils(self.main_dlg)

    def tearDown(self):
        """ Runs after each test. """
        self.utils = None
        self.main_dlg = None

    def get_text(self, message_widget):
        text_edit = None
        for w in message_widget.children():
            if 'textEdit' == w.objectName():
                text_edit = w
                break
        if text_edit:
            return text_edit.toPlainText()
        return None

    def print_message_stack(self):
        n = 0
        for m in self.main_dlg.message_stack:
            print('    {}: {}'.format(n, self.get_text(m)))
            n += 1

    def test01_check_authentication_ok_no_auth(self):
        """ Test check_authentication_ok(self) """
        # check_authentication_ok() has to access the data services site but then it just pops up one of two messages.
        # First check result of not having set any credentials.
        self.utils.check_authentication_ok()
        sleep(1)
        assert self.main_dlg.message_stack
        assert 2 == len(self.main_dlg.message_stack), len(self.main_dlg.message_stack)
        assert 'Information: Authentication details are not valid.' == self.get_text(self.main_dlg.message_stack[-1]), \
            self.get_text(self.main_dlg.message_stack[-1])
        self.main_dlg.remove_message(self.main_dlg.message_stack[-1])
        assert 'Information: Please set username and password before continuing.' == self.get_text(self.main_dlg.message_stack[-1]), \
            self.get_text(self.main_dlg.message_stack[-1])
        self.main_dlg.remove_message(self.main_dlg.message_stack[-1])
        assert not self.main_dlg.message_stack

    def test02_check_authentication_ok_bad_auth(self):
        # Next check the result of having incorrect credentials.
        self.utils.username = 'test_username'
        self.utils.password = 'test_password'
        self.utils.check_authentication_ok()
        sleep(1)
        QGIS_APP[0].processEvents()
        assert self.main_dlg.message_stack
        assert 2 == len(self.main_dlg.message_stack), len(self.main_dlg.message_stack)
        assert 'Information: Authentication details are not valid.' == self.get_text(self.main_dlg.message_stack[-1]), \
            self.get_text(self.main_dlg.message_stack[-1])
        assert 'Information: Requesting download url.' == self.get_text(self.main_dlg.message_stack[-2]), \
            self.get_text(self.main_dlg.message_stack[-2])
        self.main_dlg.remove_message(self.main_dlg.message_stack[-1])   # Removes both messages.
        assert not self.main_dlg.message_stack

    def test03_check_authentication_ok_good_auth(self):
        # Finally, check the result of having correct credentials.
        self.utils.username = os.environ['test_username']
        self.utils.password = os.environ['test_password']
        self.utils.check_authentication_ok()
        sleep(1)
        QGIS_APP[0].processEvents()
        assert 2 == len(self.main_dlg.message_stack)
        assert 'Information: Authentication details are valid.' == self.get_text(self.main_dlg.message_stack[-1]), \
            self.get_text(self.main_dlg.message_stack[-1])
        assert 'Information: Requesting download url.' == self.get_text(self.main_dlg.message_stack[-2]), \
            self.get_text(self.main_dlg.message_stack[-2])
        self.main_dlg.remove_message(self.main_dlg.message_stack[-1])   # Removes both messages.
        assert not self.main_dlg.message_stack

    def test04_odt_supports_opt(self):
        """ Test odt_supports_opt(self, odt, opt) """
        for product_type in range(8):
            for dataset_type in range(2):
                # All dataset types currently support all product types.
                assert self.utils.odt_supports_opt(dataset_type, product_type)

    def test05_check_authentication_ok_result(self):
        """ Test check_authentication_ok_result(self, completed_ok, result, ident) """
        # Implicit in test_check_authentication_ok_*()
        pass

    @unittest.skip('Needs further thought.')
    def test06_stop_thread_for(self):
        """ Test stop_thread_for(self, message_widget) """
        pass

    @unittest.skip('Requires stop_thread_for().')
    def test07_message_bar_widgetRemoved(self):
        """ Test message_bar_widgetRemoved(self, message_widget) """
        pass

    def result_fn(self, completed_ok, result, ident=None):
        self.result = result
        self.completed_ok = completed_ok
        self.ident = ident

    def test08_list_optical_datasets(self):
        """ Test list_optical_datasets(self, args, kwargs, result_fn): """
        self.utils.username = os.environ['test_username']
        self.utils.password = os.environ['test_password']

        # I'd like to fetch this list without any restrictions but
        # it takes far too long to list several thousand datasets.
        # Any small area in the uk should produce just two hits.
        if not ABER_ID:
            self.test12_create_region()

        args = []
        kwargs = {'region': ABER_ID}
        self.utils.list_optical_datasets(args, kwargs, self.result_fn)
        sleep(1)
        QGIS_APP[0].processEvents()
        assert self.result, self.result
        assert self.completed_ok, self.completed_ok
        assert 3 == len(self.result), self.result
        assert None is self.ident, self.ident

    def test09_list_backscatter_datasets(self):
        """ Test list_backscatter_datasets(self, args, kwargs, result_fn) """
        self.utils.username = os.environ['test_username']
        self.utils.password = os.environ['test_password']

        # I'd like to fetch this list without any restrictions but
        # it takes far too long to list several thousand datasets.
        # Any small area in the uk should produce just two hits.
        if not ABER_ID:
            self.test12_create_region()

        args = []
        kwargs = {'region': ABER_ID}
        self.utils.list_backscatter_datasets(args, kwargs, self.result_fn)
        sleep(1)
        QGIS_APP[0].processEvents()
        assert self.result, self.result
        assert self.completed_ok, self.completed_ok
        assert 50 < len(self.result), self.result
        assert None is self.ident, self.ident

    def test10_list_product_types(self):
        """ Test list_product_types(self, result_fn) """
        self.utils.username = os.environ['test_username']
        self.utils.password = os.environ['test_password']

        self.utils.list_product_types(self.result_fn)
        sleep(1)
        QGIS_APP[0].processEvents()
        assert self.result, self.result
        assert self.completed_ok, self.completed_ok
        # Actually just lists optical product types.
        assert 8 == len(self.result)
        expected_products = [
            (2, 'RGB'),
            (1, 'Sentinel2-10m-Bands'),
            (3, 'NRI'),
            (4, 'NDWI'),
            (5, 'OSAVI'),
            (6, 'NDVI'),
            (7, 'PSRI'),
            (8, 'EVI'),
        ]
        actual_products = [(p['id'], p['name']) for p in self.result]
        assert expected_products == actual_products, actual_products
        assert None is self.ident, self.ident

    def test11_list_regions(self):
        """ Test list_regions(self, result_fn) """
        self.utils.username = os.environ['test_username']
        self.utils.password = os.environ['test_password']

        self.utils.list_regions(self.result_fn)
        sleep(1)
        QGIS_APP[0].processEvents()
        assert self.result, self.result
        assert 1 < len(self.result)
        assert self.completed_ok, self.completed_ok
        assert None is self.ident, self.ident

    def test12_create_region(self):
        """ Test create_region(self, region_info, result_fn) """
        self.utils.username = os.environ['test_username']
        self.utils.password = os.environ['test_password']

        region_definition = NewRegion()
        region_definition.name = ABER_NAME
        geometry = {
            'type': 'Polygon',
            'coordinates': ABER_COORDS
        }
        region_definition.geometry = str(geometry) \
            .replace("'", '"') \
            .replace('(', '[') \
            .replace(')', ']')
        self.utils.create_region(region_definition, self.result_fn)

        sleep(1)
        QGIS_APP[0].processEvents()
        assert self.result, self.result
        assert self.completed_ok, self.completed_ok
        assert self.result.id
        assert self.result.name
        assert self.result.geometry
        assert self.result.created
        assert self.result.updated
        assert None is self.ident, self.ident
        global ABER_ID
        ABER_ID = self.result.id

    def test13_create_backscatter_request(self):
        """ Test create_backscatter_request(self, request_info) """
        self.utils.username = os.environ['test_username']
        self.utils.password = os.environ['test_password']

        if not ABER_ID:
            self.test12_create_region

        self.test12_create_region()
        self.test09_list_backscatter_datasets()
        dataset = self.result[0]['id']
        self.completed_ok = False
        self.result = None

        request_info = BackscatterRequestInfo()
        request_info.ratio_band = False
        request_info.log_scale = False
        request_info.projection = self.utils.WEB_MERCATOR_EPSG
        request_info.region = ABER_ID
        request_info.source_data = dataset
        self.utils.create_backscatter_request(request_info, self.result_fn)

        sleep(1)
        QGIS_APP[0].processEvents()
        assert self.result, self.result
        assert self.completed_ok, self.completed_ok
        assert not self.result.completed
        assert not self.result.error
        assert None is self.result.error_msg, self.result
        assert self.result.id, self.result
        assert not self.result.log_scale, self.result
        assert self.utils.WEB_MERCATOR_EPSG == self.result.projection, self.result
        assert not self.result.ratio_band, self.result
        assert ABER_NAME == self.result.region_name, self.result
        assert self.result.request_time, self.result
        assert dataset == self.result.source_data, self.result
        assert self.result.valid_area, self.result
        assert None is self.ident, self.ident

    def test14_create_optical_request(self):
        """ Test create_optical_request(self, request_info) """
        self.utils.username = os.environ['test_username']   
        self.utils.password = os.environ['test_password']

        if not ABER_ID:
            self.test12_create_region

        self.test08_list_optical_datasets()
        dataset = self.result[0]['id']
        self.completed_ok = False
        self.result = None

        request_info = OpticalRequestInfo()
        request_info.product_type = 2   # RGB
        request_info.apply_cloud_mask = False
        request_info.projection = self.utils.WEB_MERCATOR_EPSG
        request_info.region = ABER_ID   # Aber
        request_info.datasets = [dataset]
        self.utils.create_optical_request(request_info, self.result_fn)

        sleep(1)
        QGIS_APP[0].processEvents()
        print(self.result)
        assert self.result, self.result
        assert self.completed_ok, self.completed_ok
        assert not self.result.completed
        assert not self.result.error
        assert None is self.result.error_msg, self.result
        assert self.result.id, self.result
        assert not self.result.apply_cloud_mask, self.result
        assert self.utils.WEB_MERCATOR_EPSG == self.result.projection, self.result
        assert 2 == self.result.product_type, self.result
        # assert ABER_NAME == self.result.region_name, self.result
        assert self.result.request_time, self.result
        assert [dataset] == self.result.datasets, self.result
        assert self.result.valid_area, self.result
        assert None is self.ident, self.ident

    def test15_all_product_requests(self):
        """ Test all_product_requests(self, completed, result_fn) """
        self.utils.username = os.environ['test_username']
        self.utils.password = os.environ['test_password']

        self.utils.all_product_requests(False, self.result_fn)
        sleep(1)
        QGIS_APP[0].processEvents()
        assert self.completed_ok, self.completed_ok
        assert self.result, self.result
        assert 1 < len(self.result)
        assert None is self.ident, self.ident

    def test16_get_download_url(self):
        """ Test get_download_url(self, ident, result_fn, result_ident=None) """
        self.utils.username = os.environ['test_username']
        self.utils.password = os.environ['test_password']

        self.test15_all_product_requests()
        ident = None
        for request in self.result:
            if 'Available' == request['status']:
                ident = request['id']
                break
        assert ident
        self.completed_ok = None
        self.result = None
        self.utils.get_download_url(ident, self.result_fn, ident)

        sleep(1)
        QGIS_APP[0].processEvents()
        assert self.completed_ok, self.completed_ok
        assert self.result, self.result
        assert 1 < len(self.result)
        assert ident is self.ident, self.ident

    @unittest.skip('Too time-consuming.')
    def test17_download_product(self):
        """ Test download_product(self, url, full_filename, result_fn, ident=None) """
        self.utils.username = os.environ['test_username']
        self.utils.password = os.environ['test_password']

        self.test16_get_download_url()
        url = self.result
        self.completed_ok = None
        self.result = None
        url_components = urlparse.urlsplit(url)
        path, filename = os.path.split(url_components.path)
        full_filename = os.path.join(self.utils.downloads_dir, filename)
        self.utils.download_product(url, full_filename, self.result_fn)

        sleep(1)
        QGIS_APP[0].processEvents()
        assert self.result, self.result
        assert self.completed_ok, self.completed_ok
        assert None is self.ident, self.ident
        assert self.main_dlg.num_progress_calls, self.main_dlg.num_progress_calls

    @unittest.skip('Too time-consuming.')
    def test18_scraper_login(self):
        """ Test scraper_login(self, scraper, result_fn) """
        self.utils.username = os.environ['test_username']
        self.utils.password = os.environ['test_password']

        self.scraper = ScrapeTool(self.utils.downloads_dir)
        self.utils.scraper_login(self.scraper, self.result_fn)

        # Login takes about 3s.
        for i in range(10):
            sleep(1)
            QGIS_APP[0].processEvents()
            if self.completed_ok:
                break
        assert None is self.result, self.result
        assert self.completed_ok, self.completed_ok
        assert None is self.ident, self.ident
        assert 3 == self.main_dlg.num_progress_calls, self.main_dlg.num_progress_calls

    @unittest.skip('Too time-consuming.')
    def test19_scraper_download(self):
        """ Test scraper_download(self, scraper, layer_name, tile_ident, result_fn=None) """
        self.test18_scraper_login()

        self.completed_ok = None
        self.result = None
        self.utils.scraper_download(self.scraper, self.scraper.layer_names[0], '00131', self.result_fn)

        # Expected to take about 30s.
        for i in range(60):
            sleep(1)
            QGIS_APP[0].processEvents()
            if self.completed_ok:
                break
        assert None is self.result, self.result
        assert self.completed_ok, self.completed_ok
        assert None is self.ident, self.ident
        assert 3+301 == self.main_dlg.num_progress_calls, self.main_dlg.num_progress_calls

    def sync_result_fn(self, ident):
        self.ident = ident

    def test20_sync(self):
        """ Test sync(self, ident, result_fn) """
        self.utils.sync(123, self.sync_result_fn)
        sleep(1)
        QGIS_APP[0].processEvents()
        assert 123 == self.ident, self.ident

    def test21_no_auth(self):
        """ Test no_auth(self) """
        pass


if __name__ == '__main__':
    unittest.main()
