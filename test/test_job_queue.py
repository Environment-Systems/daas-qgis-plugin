# coding=utf-8
"""Utils test."""

__author__ = 'dataservices@envsys.co.uk'
__date__ = '2018-March-15'
__copyright__ = 'Copyright 2018, Environment Systems Ltd.'

import os
import sys
import unittest
from time import sleep

from PyQt4.QtCore import Qt
from PyQt4.QtGui import QApplication


def include_dir(directory):
    if directory not in sys.path:
        sys.path.insert(0, directory)


root_dir = os.path.abspath(os.path.dirname(__file__))
root_dir = os.path.split(root_dir)[0]
include_dir(root_dir)
include_dir(os.path.join(root_dir, 'extras'))
include_dir(os.path.join(root_dir, 'test'))


from job_queue import WorkThread, JobQueue
# Import Utils just for RESULT_IS_SINGLE, RESULT_IS_LIST, PROGRESS_BAR_REQUIRED, PROGRESS_BAR_NOT_REQUIRED
from utils import Utils
from envsys_dialog import EnvsysDialog

# from dummy_main_dialog import DummyMainDlg

# APP = QApplication([])  # Create once.  Never any need to try and close it down.
from utilities import get_qgis_app
QGIS_APP = get_qgis_app()

# from log import Log
# debug = Log('TestJobQ')


class TestWorkThread(unittest.TestCase):
    """ Test thread works. """

    def setUp(self):
        """ Runs before each test. """
        self.step = 0
        self.finished = False
        self.main_dlg = EnvsysDialog()

    def tearDown(self):
        """ Runs after each test. """
        self.main_dlg = None

    # noinspection PyMethodMayBeStatic
    def test1_reinit(self):
        """ Test reinit(self) """

        # Not really interested in any of the parameters here.
        message_widget = None
        worker_function = None
        args = []
        kwargs = {}
        returns_list = False

        thread = WorkThread(
            message_widget,
            worker_function,
            args,
            kwargs,
            returns_list,
        )
        thread.abort = True
        thread.finished = True
        thread.item_list = [1, 2, 3]
        thread.error = 'Error'

        thread.reinit()

        assert not thread.abort, thread.abort
        assert not thread.finished, thread.finished
        assert [] == thread.item_list, thread.item_list
        assert None is thread.error, thread.error

    def single_worker_function(self, *args, **kwargs):   # a=1, b=2, c=3, stuff='qwerty'):
        """ Check that the worker gets called with the correct arguments. """
        self.step += 1
        assert (11, 22, 33) == args
        assert {'stuff': 'asdfgh'} == kwargs, kwargs
        return 'Single'

    def worker_function_finished(self):
        """ Handle results signal from thread. """
        self.finished = True    # Just note that this function has been called.

    def test2_single_run(self):
        """ test single run(self) """
        message_widget = self.main_dlg.show_cancel_message('Hello')
        thread = WorkThread(
            message_widget,
            self.single_worker_function,
            [11, 22, 33],
            {'stuff': 'asdfgh'},
            Utils.RESULT_IS_SINGLE,
        )
        # Not interested in progress signals.
        thread.result_signal.connect(self.worker_function_finished)
        thread.start()
        thread.wait()
        QGIS_APP[0].processEvents()
        assert not thread.abort, thread.abort
        assert thread.finished, thread.finished
        assert self.finished, self.finished
        assert thread.error is None, thread.error
        assert 'Single' == thread.item_list

        assert 1 == self.step
        assert 0 == self.main_dlg.num_progress_calls

    def list_worker_function(self, *args, **kwargs):
        """ Check that the worker gets called with the correct arguments. """
        self.step += 1
        if 1 == self.step:
            assert 0 == kwargs['offset'], kwargs
            assert 100 == kwargs['limit'], kwargs
            assert (11, 22, 33) == args, args
            assert 'asdfgh' == kwargs['stuff'], kwargs
            return {
                'count': 123,
                'results': range(0, 100)
            }
        elif 2 == self.step:
            assert 100 == kwargs['offset'], kwargs
            assert 100 == kwargs['limit'], kwargs
            assert (11, 22, 33) == args, args
            assert 'asdfgh' == kwargs['stuff'], kwargs
            return {
                'count': 123,
                'results': range(100, 123)
            }
        else:
            assert False, 'Worker function called too many times.'

    def test3_list_run(self):
        """ test list run(self) """
        message_widget = self.main_dlg.show_cancel_message('Hello', progress_bar_required=True)
        assert message_widget, message_widget
        thread = WorkThread(
            message_widget,
            self.list_worker_function,
            [11, 22, 33],
            {'stuff': 'asdfgh'},
            Utils.RESULT_IS_LIST,
        )
        thread.progress_signal.connect(self.main_dlg.show_progress)
        thread.result_signal.connect(self.worker_function_finished)
        thread.start()
        thread.wait()
        QGIS_APP[0].processEvents()
        assert not thread.abort, thread.abort
        assert thread.finished, thread.finished
        assert self.finished, self.finished
        assert thread.error is None, thread.error
        assert range(0, 123) == thread.item_list, str(thread.item_list)

        assert 2 == self.step
        assert 2 == self.main_dlg.num_progress_calls, self.main_dlg.num_progress_calls
        assert 100 == self.main_dlg.last_progress_val, self.main_dlg.last_progress_val
        assert 123 == self.main_dlg.last_progress_total, self.main_dlg.last_progress_total


class TestJobQueue(unittest.TestCase):
    """ Test job queue works. """

    def setUp(self):
        """ Runs before each test. """
        self.sync_step = 0
        self.step = 0
        self.completed_ok = None
        self.result = None
        self.ident = None
        self.finished = False

        self.main_dlg = EnvsysDialog()
        self.job_queue = JobQueue(self.main_dlg)

    def tearDown(self):
        """ Runs after each test. """
        self.job_queue = None
        self.main_dlg = None

    def work_single(self):
        self.step += 1
        return 456

    def sync_result_fn(self, ident):
        self.finished = True
        self.ident = ident

    def test1_queue_job_sync(self):
        """ Test sync queue_job(
                self,
                job_fn,
                message,
                progress_bar_required,
                args,
                kwargs,
                result_is_list,
                result_fn,
                scraper=None,
                ident=None
            )
        """
        # Test 'sync' job.
        self.job_queue.queue_job(
            None,
            'Test #1',
            Utils.PROGRESS_BAR_NOT_REQUIRED,
            [],
            {},
            Utils.RESULT_IS_SINGLE,
            self.sync_result_fn,
            ident=123
        )
        # Shouldn't actually create a thread at all, so no need to wait, etc.
        assert self.finished, self.finished
        assert 123 == self.ident, self.ident

    def result_fn(self, completed_ok, result, ident=None):
        self.finished = True
        self.completed_ok = completed_ok
        self.result = result
        self.ident = ident

    def test2_queue_job_single(self):
        """ Test single-result queue_job(
                self,
                job_fn,
                message,
                progress_bar_required,
                args,
                kwargs,
                result_is_list,
                result_fn,
                scraper=None,
                ident=None
            )
        """
        # Test single-result job.
        self.job_queue.queue_job(
            self.work_single,
            'Test #2',
            Utils.PROGRESS_BAR_NOT_REQUIRED,
            [],
            {},
            Utils.RESULT_IS_SINGLE,
            self.result_fn,
            ident=123
        )
        sleep(1)
        QGIS_APP[0].processEvents()
        assert 1 == self.step, self.step
        assert 456 == self.result, self.result
        assert self.completed_ok, self.completed_ok
        assert 123 == self.ident, self.ident

    def work_list(self, *args, **kwargs):
        # print('work_list({}, {})'.format(args, kwargs))

        self.step += 1
        if 1 == self.step:
            assert 0 == kwargs['offset'], kwargs
            assert 100 == kwargs['limit'], kwargs
            assert (11,) == args, args
            assert 'asdfgh' == kwargs['stuff'], kwargs
            return {
                'count': 123,
                'results': range(1, 101)
            }
        elif 2 == self.step:
            assert 100 == kwargs['offset'], kwargs
            assert 100 == kwargs['limit'], kwargs
            assert (11,) == args, args
            assert 'asdfgh' == kwargs['stuff'], kwargs
            return {
                'count': 123,
                'results': range(101, 124)
            }
        else:
            assert False, 'Worker function called too many times.'

    def test3_queue_job_list(self):
        """ Test list-result queue_job(
                self,
                job_fn,
                message,
                progress_bar_required,
                args,
                kwargs,
                result_is_list,
                result_fn,
                scraper=None,
                ident=None
            )
        """
        # Test list-result job.
        self.job_queue.queue_job(
            self.work_list,
            'Test #3',
            Utils.PROGRESS_BAR_REQUIRED,
            [11],
            {'stuff': 'asdfgh'},
            Utils.RESULT_IS_LIST,
            self.result_fn,
            ident=456
        )
        sleep(1)
        QGIS_APP[0].processEvents()
        assert range(1, 124) == self.result, self.result
        assert self.completed_ok, self.completed_ok
        assert 456 == self.ident, self.ident
        assert 2 == self.step
        assert 2 == self.main_dlg.num_progress_calls, self.main_dlg.num_progress_calls
        assert 100 == self.main_dlg.last_progress_val, self.main_dlg.last_progress_val
        assert 123 == self.main_dlg.last_progress_total, self.main_dlg.last_progress_total

    def work_1(self):
        assert 0 == self.step
        self.step += 1
        return 1

    def result_1(self, completed_ok, result, ident):
        assert 1 == self.step, self.step
        assert 1 == result, result
        assert completed_ok, completed_ok
        assert 11 == ident, ident
        self.completed_ok = completed_ok
        self.result = result
        self.ident = ident

    def work_2(self):
        assert 1 == self.step
        self.step += 1

        # Check previous result
        assert self.completed_ok, self.completed_ok
        assert 1 == self.result, self.result
        assert 11 == self.ident, self.ident
        # Prepare for this result.
        self.completed_ok = False
        self.result = None
        self.ident = None

        return 2

    def result_2(self, completed_ok, result, ident):
        assert 2 == self.step, self.step
        assert 2 == result, result
        assert completed_ok, completed_ok
        assert 22 == ident, ident
        self.completed_ok = completed_ok
        self.result = result
        self.ident = ident

    def work_3(self):
        assert 2 == self.step
        self.step += 1

        # Check previous result
        assert self.completed_ok, self.completed_ok
        assert 2 == self.result, self.result
        assert 22 == self.ident, self.ident
        # Prepare for this result.
        self.completed_ok = False
        self.result = None
        self.ident = None

        return 3

    def result_3(self, completed_ok, result, ident):
        assert 3 == self.step, self.step
        assert 3 == result, result
        assert completed_ok, completed_ok
        assert 33 == ident, ident
        self.completed_ok = completed_ok
        self.result = result
        self.ident = ident

    def test4_next_job(self):
        """ Test queue_job(self) """
        self.job_queue.queue_job(
            self.work_1,
            'Test #4.1',
            Utils.PROGRESS_BAR_NOT_REQUIRED,
            [],
            {},
            Utils.RESULT_IS_SINGLE,
            self.result_1,
            ident=11
        )
        self.job_queue.queue_job(
            self.work_2,
            'Test #4.2',
            Utils.PROGRESS_BAR_NOT_REQUIRED,
            [],
            {},
            Utils.RESULT_IS_SINGLE,
            self.result_2,
            ident=22
        )
        self.job_queue.queue_job(
            self.work_3,
            'Test #4.3',
            Utils.PROGRESS_BAR_NOT_REQUIRED,
            [],
            {},
            Utils.RESULT_IS_SINGLE,
            self.result_3,
            ident=33
        )
        for i in range(5):
            sleep(1)
            QGIS_APP[0].processEvents()
        assert 3 == self.result, self.result
        assert self.completed_ok, self.completed_ok
        assert 33 == self.ident, self.ident
        assert 3 == self.step
        assert 0 == self.main_dlg.num_progress_calls, self.main_dlg.num_progress_calls

    def test5_cancel_thread(self):
        """ Test cancel_thread(self) """
        # Tricky.
        pass

    def test6_result_cleanup(self):
        """ Test result_cleanup(self) """
        # Inherent in test_next_job().
        pass

    def test7_echo_progress_signal(self):
        """ Test echo_progress_signal(self, message_widget, val, total) """
        # Inherent in test_queue_job_list().
        pass


if __name__ == '__main__':
    unittest.main()


