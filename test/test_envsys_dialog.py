# coding=utf-8
"""Dialog test.

.. note:: This program is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation; either version 2 of the License, or
     (at your option) any later version.

"""

__author__ = 'dataservices@envsys.co.uk'
__date__ = '2017-12-12'
__copyright__ = 'Copyright 2018, Environment Systems Ltd.'

import unittest

from PyQt4.QtGui import QDialogButtonBox, QDialog, QSplitter, QTabWidget, QStackedWidget, QStackedLayout

import os
import sys


def include_dir(directory):
    if directory not in sys.path:
        sys.path.insert(0, directory)


root_dir = os.path.abspath(os.path.dirname(__file__))
root_dir = os.path.split(root_dir)[0]
include_dir(root_dir)
include_dir(os.path.join(root_dir, 'extras'))
include_dir(os.path.join(root_dir, 'test'))


import settings
import search
import products
import free

from envsys_dialog import EnvsysDialog

from utilities import get_qgis_app
QGIS_APP = get_qgis_app()


class EnvsysResourcesTest(unittest.TestCase):
    """Test dialog works."""

    def setUp(self):
        """Runs before each test."""
        self.dialog = EnvsysDialog(None)

    def tearDown(self):
        """Runs after each test."""
        self.dialog = None

    def test_dialog_close(self):
        """Test we can click Close."""
        button = self.dialog.button_box.button(QDialogButtonBox.Close)
        button.click()
        result = self.dialog.result()
        self.assertEqual(result, QDialog.Rejected)

    def test_tabs(self):
        splitter = self.dialog.findChild(QSplitter, 'message_splitter')
        tabs = splitter.findChild(QTabWidget, 'plugin_tabs')
        stack = tabs.findChild(QStackedWidget, 'qt_tabwidget_stackedwidget')
        actual_children = stack.children()

        expected_children = [
            QStackedLayout,
            settings.SettingsWidget,
            search.SearchWidget,
            products.ProductsWidget,
            free.FreeWidget,
        ]

        assert len(expected_children) == len(actual_children)
        for e in expected_children:
            for a in actual_children:
                if type(a) == e:
                    break
            else:
                assert False, '{} not found.'.format(e)

if __name__ == "__main__":
    suite = unittest.makeSuite(EnvsysDialogTest)
    runner = unittest.TextTestRunner(verbosity=2)
    runner.run(suite)

