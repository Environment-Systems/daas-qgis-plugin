__author__ = 'dataservices@envsys.co.uk'
__date__ = '2018-03-21'
__copyright__ = 'Copyright 2018, Environment Systems Ltd.'

from time import sleep

import unittest

from PyQt4.QtGui import QDialogButtonBox, QDialog, QSplitter, QTabWidget, QStackedWidget, QPushButton

import os
import sys

def include_dir(directory):
    if directory not in sys.path:
        sys.path.insert(0, directory)


root_dir = os.path.abspath(os.path.dirname(__file__))
root_dir = os.path.split(root_dir)[0]
include_dir(root_dir)
include_dir(os.path.join(root_dir, 'extras'))
include_dir(os.path.join(root_dir, 'test'))

from envsys_dialog import EnvsysDialog

import settings

from utilities import get_qgis_app
QGIS_APP = get_qgis_app()
#  <qgis._core.QgsApplication object at 0x7ff76afa6348>,            Qt app
#  <qgis._gui.QgsMapCanvas object at 0x7ff76afa6478>,               canvas
#  <test.qgis_interface.QgisInterface object at 0x7ff76afa6510>,    iface ?
#  <PyQt4.QtGui.QWidget object at 0x7ff76afa63e0>)                  main QGIS window ?


n=0
def show_widget_tree(w, indent=0, parent_id=0):
    global n
    n += 1
    nn = n
    print('{:5} {:5}'.format(parent_id, n) + '  '*indent + '{}: "{}"'.format(type(w), w.objectName()))
    for child in w.children():
        show_widget_tree(child, indent + 1, nn)


def get_text(message_widget):
    text_edit = None
    for w in message_widget.children():
        if 'textEdit' == w.objectName():
            text_edit = w
            break
    if text_edit:
        return text_edit.toPlainText()
    return None


def print_message_stack(main_dlg):
    n = 0
    for m in main_dlg.message_stack:
        print('    {}: {}'.format(n, get_text(m)))
        n += 1


class TestConfig(unittest.TestCase):
    """Test Config tab works."""

    def setUp(self):
        """Runs before each test."""
        self.main_dlg = EnvsysDialog(None)

        splitter = self.main_dlg.findChild(QSplitter, 'message_splitter')
        tabs = splitter.findChild(QTabWidget, 'plugin_tabs')
        stack = tabs.findChild(QStackedWidget, 'qt_tabwidget_stackedwidget')
        self.config = stack.findChild(settings.SettingsWidget, 'config')

    def tearDown(self):
        """Runs after each test."""
        self.config = None
        self.main_dlg = None

    def test_check_auth(self):
        btn = self.config.findChild(QPushButton, 'check_authentication_btn')
        assert btn
        btn.click()
        sleep(1)
        QGIS_APP[0].processEvents()

        assert self.main_dlg.message_stack
        assert 2 == len(self.main_dlg.message_stack), len(self.main_dlg.message_stack)
        assert 'Information: Authentication details are valid.' == get_text(self.main_dlg.message_stack[-1]), \
            get_text(self.main_dlg.message_stack[-1])
        assert 'Information: Requesting download url.' == get_text(self.main_dlg.message_stack[-2]), \
            get_text(self.main_dlg.message_stack[-2])
        self.main_dlg.remove_message(self.main_dlg.message_stack[-1])
        assert not self.main_dlg.message_stack
