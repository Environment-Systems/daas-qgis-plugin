from PyQt4.QtCore import QObject, pyqtSlot

from qgis.gui import QgsMessageBar


class QgsMessageBarItem(QObject):
    """ Dummy message class. """

    def __init__(self, msg, level, title, duration, cancel_btn_required, progress_bar_required):
        super(QgsMessageBarItem, self).__init__()
        self.msg = msg
        self.level = level
        self.title = title
        self.duration = duration
        self.cancel_btn_required = cancel_btn_required
        self.progress_bar_required = progress_bar_required

DEBUG = False

class DummyMainDlg(QObject):
    """ Dummy dialog class. """

    def __init__(self):
        super(DummyMainDlg, self).__init__()
        self.message_stack = []
        self.num_progress_calls = 0
        self.last_progress_val = -1
        self.last_progress_total = -1

    def show_message(self, msg, level=QgsMessageBar.INFO, title='Information', duration=0):
        if DEBUG:
            print('show_message(' + str(msg) + ')')
        msg_item = QgsMessageBarItem(msg, level, title, duration, False, False)
        self.message_stack.append(msg_item)
        return msg_item

    def show_cancel_message(
            self,
            msg,
            level=QgsMessageBar.INFO,
            title='Information',
            progress_bar_required=False
    ):
        if DEBUG:
            print('show_cancel_message(' + str(msg) + ')')
        msg_item = QgsMessageBarItem(msg, level, title, -1, True, progress_bar_required)
        self.message_stack.append(msg_item)
        return msg_item

    @pyqtSlot(QgsMessageBarItem, int, int)
    def show_progress(self, message_widget, val, total):
        if DEBUG:
            print('show_progress({}, {}, {})'.format(message_widget.msg, val, total))
        assert message_widget is self.message_stack[-1]
        assert message_widget.cancel_btn_required
        assert message_widget.progress_bar_required
        assert (val >= 0) and (val <= total)

        self.num_progress_calls += 1
        self.last_progress_val = val
        self.last_progress_total = total

    def remove_message(self, message_widget):
        """ Remove a message from the message stack.
        The 'real thing' only removes the top-of-stack message and remembers other messages for later removal.
        Instead, we just delete messages from the middle of the stack.
        :param message_widget: The message to be removed.
        """
        if DEBUG:
            print('remove {}'.format(message_widget.msg))
        self.message_stack.remove(message_widget)

    def help(self):
        pass

    def show_help(self, title, help_filename, buttons=None):
        pass
