# coding=utf-8
"""Resources test.

.. note:: This program is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation; either version 2 of the License, or
     (at your option) any later version.

"""

__author__ = 'dataservices@envsys.co.uk'
__date__ = '2017-12-12'
__copyright__ = 'Copyright 2017, Environment Systems Ltd.'

import os
import unittest
import ConfigParser

from PyQt4.QtGui import QIcon


class EnvsysDialogTest(unittest.TestCase):
    """Test resources work."""

    def setUp(self):
        """Runs before each test."""
        pass

    def tearDown(self):
        """Runs after each test."""
        pass

    def test_icon_png(self):
        """Test icon is present."""
        file_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir, 'metadata.txt'))
        parser = ConfigParser.ConfigParser()
        parser.optionxform = str
        parser.read(file_path)
        icon_name = parser.get('general', 'icon')

        path = ':/plugins/Envsys/' + icon_name
        icon = QIcon(path)
        self.assertFalse(icon.isNull())

if __name__ == "__main__":
    suite = unittest.makeSuite(EnvsysResourcesTest)
    runner = unittest.TextTestRunner(verbosity=2)
    runner.run(suite)



