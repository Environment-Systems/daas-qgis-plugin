# -*- coding: utf-8 -*-
"""
/****************************************************************************
 This file is part of EnvSys plugin for QGIS
 Integration with Environment Systems Data Services.
                             -------------------
        begin                : 2017-12-12
        copyright            : (C) 2017 by Environment Systems Ltd.
        email                : dataservices@envsys.co.uk
        git sha              : $Format:%H$
 ****************************************************************************/

/****************************************************************************
 *                                                                          *
 *   EnvSys plugin is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 3 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   EnvSys plugin is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
 *   GNU General Public License for more details.                           *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with EnvSys plugin. If not, see <https://www.gnu.org/licenses/>. *
 *                                                                          *
 ****************************************************************************/
"""


import os
import sys
from datetime import datetime
import io
import string

from PyQt4.QtCore import QThread

from qgis.core import QgsMessageLog

stuff = {
    'debug_file': io.open(os.path.expanduser('~/debug.txt'), 'w', encoding='utf8'),
    'module_width': 0,
    'func_width': 0,
    'last_thread': 0,
}


class Log(object):
    def __init__(self, module_name):
        """ Constructor
        :param module_name: The name of the module that wants to use the log.
        """
        self.module_name = module_name
        if stuff['module_width'] < len(module_name):
            stuff['module_width'] = len(module_name)

    # noinspection PyMethodMayBeStatic
    def current_func_name(self, n=0):
        """ Find the name of the calling function.
        Can actually find all ancestors (one at a time), not just one generation.
        :param n: How many generations to go back.
        :return: (filename, function name, line number) of caller.
        """
        frame = sys._getframe(n + 1)
        return (frame.f_code.co_filename, frame.f_code.co_name, frame.f_lineno)

    def log(self, msg='""', stack_dump_depth=0):
        """ Write a message to the log, prefixed with some standard bits of information.
        :param msg: Message to be written
        :param stack_dump_depth: Causes a traceback to be printed below the message.
        """
        # ----------------------------------------------------------------------
        # Nothing to do with the rest of the log, but arrange for QGIS log messages to be recorded.
        if 0 == stuff['func_width']:
            # This is the first log message.
            filename = os.path.expanduser('~/error.log')
            stuff['debug_file'].write(u'Setting up message log' + filename + '\n')
            stuff['debug_file'].flush()

            def write_log_message(message, tag, level):
                if stuff is not None:
                    if stuff['debug_file'] is not None:
                        print('stuff='+str(stuff))
                        print('debug file=' + str(stuff['debug_file']))
                        stuff['debug_file'].write(u'{tag}({level}): {message}\n'.format(
                            tag=tag,
                            level=level,
                            message=message
                        ))
                        stuff['debug_file'].flush()

                # with open(filename, 'a') as logfile:
                #     logfile.write('{tag}({level}): {message}'.format(tag=tag, level=level, message=message))

            QgsMessageLog.instance().messageReceived.connect(write_log_message)
        # ----------------------------------------------------------------------

        time = datetime.now().strftime('%H:%M:%S.%f')
        thread = hex(QThread.currentThreadId())
        if stuff['last_thread'] != thread:
            stuff['last_thread'] = thread
            changed = u'*'
        else:
            changed = u' '
        filename, fn_name, line_num = self.current_func_name(1)
        if stuff['func_width'] < len(fn_name):
            stuff['func_width'] = len(fn_name)
        #           Time  Thread  Module+line num    Function  Message
        fmt_str = u'{{}} | {{}}{{}} | {{:<{}}}:{{:>4}} | {{:<{}}} | {{}}\n'
        fmt_str = fmt_str.format(stuff['module_width'], stuff['func_width'])
        stuff['debug_file'].write(fmt_str.format(
            unicode(time),
            unicode(thread),
            changed,
            unicode(self.module_name),
            unicode(line_num),
            unicode(fn_name),
            msg
        ))
        stuff['debug_file'].flush()
        for i in range(stack_dump_depth, 0, -1):
            try:
                filename, fn_name, line_num = self.current_func_name(i)
                stuff['debug_file'].write(u'{}:{} | {}\n'.format(filename, line_num, fn_name))
                stuff['debug_file'].flush()
            except ValueError:  # Stack may not be deep enough.
                stuff['debug_file'].write(u'---\n')
                stuff['debug_file'].flush()

    # noinspection PyMethodMayBeStatic
    def hexdump(self, msg='""'):
        """ Write a hex-dump of a string where we might have non-ascii characters.
        Format: "0000: 00 00 00 00  00 00 00 00   00 00 00 00  00 00 00 00  ................"
        where the first group of digits is the offset into the string,
        followed by hex ordinal values of each character,
        followed by normal character if printable, otherwise '.'
        :param msg: Message to be dumped.
        """
        self.log('hexdump')
        offset = 0
        values = ''
        text = ''
        line_len = 0
        for ch in msg:
            values += ' {:02x}'.format(ord(ch))
            if ch in string.printable:
                text += ch
            else:
                text += '.'

            line_len += 1
            if line_len in [4, 12]:
                values += ' '
            if 8 == line_len:
                values += '  '

            if 16 == line_len:
                stuff['debug_file'].write(u'{:04x}: {}  {}\n'.format(offset, values, text))
                offset += 16
                values = ''
                text = ''
                line_len = 0
        if line_len:
            values += '   '*(16-line_len)
            stuff['debug_file'].write(u'{:04x}: {}  {}\n'.format(offset, values, text))
