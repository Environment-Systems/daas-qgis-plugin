# -*- coding: utf-8 -*-
"""
/****************************************************************************
 This file is part of EnvSys plugin for QGIS
 Integration with Environment Systems Data Services.
                             -------------------
        begin                : 2017-12-12
        copyright            : (C) 2017 by Environment Systems Ltd.
        email                : dataservices@envsys.co.uk
        git sha              : $Format:%H$
 ****************************************************************************/

/****************************************************************************
 *                                                                          *
 *   EnvSys plugin is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 3 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   EnvSys plugin is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           *
 *   GNU General Public License for more details.                           *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with EnvSys plugin. If not, see <https://www.gnu.org/licenses/>. *
 *                                                                          *
 ****************************************************************************/
"""


import os

from qgis.gui import QgsMapToolPan
from qgis.utils import iface
from qgis.core import QgsMapLayerRegistry, QgsRectangle, QgsFeatureRequest, QgsCoordinateTransform

from PyQt4 import uic
from PyQt4.QtCore import Qt, QPoint, pyqtSignal, pyqtSlot
from PyQt4.QtGui import QWidget, QMenu

from translate import tr

from scrape import ScrapeTool

# from log import Log
# debug = Log('Free')


class PointTool(QgsMapToolPan):
    """ Tool for pointing at maps.  Based on
    https://gis.stackexchange.com/questions/45094/how-to-programatically-check-for-a-mouse-click-in-qgis
    """
    GRID_NAME = u'GB_50k_grid_3857'

    download_signal = pyqtSignal(unicode, unicode)

    def __init__(self, canvas, utils, layer_names):
        """ Constructor
        :param canvas: The map that we will operate on.
        :param utils: Common utilities object giving access to network calls etc.
        :param layer_names: List of map layer names in lower case.  Download from URLS using
        upper case versions of these but write the local zip file using these lower case versions.
        Additionally, prefix non-'optical' filenames with 'surf_tex_'.
        """
        QgsMapToolPan.__init__(self, canvas)
        self.canvas = canvas
        self.utils = utils
        self.layer_names = layer_names
        self.press_pos = None

        # Load up the grid.
        grid_shapefile = os.path.expanduser(os.path.join(os.path.dirname(__file__), self.GRID_NAME+'.shp'))
        assert os.path.isfile(grid_shapefile), tr('Grid shapefile not found.')
        self.grid_layer = iface.addVectorLayer(grid_shapefile, self.GRID_NAME, 'ogr')
        if self.grid_layer and self.grid_layer.isValid():
            self.grid_layer.setLayerTransparency(75)
        else:
            self.grid_layer = None

    def close(self):
        """ Destructor-ish. """
        if self.grid_layer and self.grid_layer.isValid():
            QgsMapLayerRegistry.instance().removeMapLayer(self.grid_layer.id())

    def canvasPressEvent(self, event):
        """ Handle button presses on the map.
        :param event: Information about what happened on the canvas.
        """
        # Unfortunately it can be any button, so check first!
        if event.button() == Qt.RightButton:
            # Have we got the grid on display?
            if self.grid_layer and self.grid_layer.isValid():
                # Where do we need to display the menu?
                press_pos = event.pos()     # Screen coordinates.

                # There's probably a way to do this in one step but two steps will have to do for now.
                map_press_point = self.canvas.getCoordinateTransform().toMapCoordinates(
                    press_pos.x(),
                    press_pos.y()
                )   # Map coordinates.
                map_crs = self.canvas.mapRenderer().destinationCrs()
                grid_crs = self.grid_layer.crs()
                xform = QgsCoordinateTransform(map_crs, grid_crs)
                grid_press_point = xform.transform(map_press_point)     # grid coordinates.

                # Which grid feature has been clicked on?
                rect = QgsRectangle(grid_press_point, grid_press_point)   # Construct a one point QgsRectangle.
                req = QgsFeatureRequest()
                req.setFilterRect(rect)
                features = self.grid_layer.getFeatures(req)
                try:
                    square = features.next()
                    tile_ident = square['TILE_ID']
                    # Now pop up a menu at the mouse position.
                    menu = QMenu()
                    title_action = menu.addAction(tr('Tile {}'.format(tile_ident)))
                    menu.addSeparator()
                    for name in self.layer_names:
                        menu.addAction(tr('{}').format(name))
                    action = menu.exec_(self.canvas.mapToGlobal(QPoint(event.pos().x() + 5, event.pos().y()+5)))
                    if action and action != title_action:
                        self.download_signal.emit(action.text(), tile_ident)
                except StopIteration:
                    # Clicking outside the grid does nothing.
                    pass

    # noinspection PyPep8Naming, PyMethodMayBeStatic
    def isZoomTool(self):
        return False

    # noinspection PyPep8Naming
    def isTransient(self):
        return False

    # noinspection PyPep8Naming
    def isEditTool(self):
        return True


class FreeWidget(QWidget):
    PROMPT_MESSAGE = 'Over Great Britain, right - click on the map grid and select a layer to download it.\n' \
                     'Press \'Help\' for more information.'

    def __init__(self, utils,  parent=None):
        """ Constructor.
        :param utils: Common utils object so that individual widgets
            don't all have to download their own version of the lists.
        :param parent: Widget that this widget is a child of.
        """
        QWidget.__init__(self, parent)
        self.ui = uic.loadUi(os.path.expanduser(os.path.join(os.path.dirname(__file__), 'free.ui')), self)
        self.utils = utils
        self.point_tool = None
        self.previous_tool = None
        self.scrape_tool = None
        self.username = self.utils.username     # Only so that we can detect changes.
        self.password = self.utils.password
        self.main_dlg = None
        self.prompt_textEdit.setText(
            tr(self.PROMPT_MESSAGE))

    def tab_shown(self):
        """ Initialization each time the tab is selected. """
        # Try to do a one-time initialization of the ScrapeTool but
        # the user may change username/password.
        self.main_dlg = self.parent().parent().parent().parent()
        self.prompt_textEdit.setText(
            tr(self.PROMPT_MESSAGE))
        if self.point_tool:
            pass
        elif not self.scrape_tool     \
            or self.username != self.utils.username \
            or self.password != self.utils.password \
        :
            if self.scrape_tool:
                self.scrape_tool.logout()
            self.username = self.utils.username
            self.password = self.utils.password
            self.scrape_tool = ScrapeTool(self.utils.downloads_dir, parent=self)
            self.utils.scraper_login(self.scrape_tool, self.login_result)
        else:
            # Make sure we don't get lots of connections.
            try:
                self.utils.download_queue.job_complete_signal.disconnect(self.job_complete)
            except Exception:
                pass  # Just in case we didn't connect in the first place.
            self.setup_point_tool()     # This will make sure we are connected though.
        # We don't expect to log out since we don't get notified
        # about the plugin being removed or QGIS shutting down.

    def tab_hidden(self):
        """ Tidy up each time the tab is hidden. """
        # Don't log out!
        if self.point_tool:
            self.point_tool.close()
            if self.previous_tool:
                iface.mapCanvas().setMapTool(self.previous_tool)
        self.previous_tool = None
        self.point_tool = None

        try:
            self.point_tool.download_signal.disconnect(self.download)
        except Exception:
            pass    # Just in case we didn't connect in the first place.

        try:
            self.utils.download_queue.job_complete_signal.disconnect(self.job_complete)
        except Exception:
            pass    # Just in case we didn't connect in the first place.

    def login_result(self, completed_ok, result):
        """ Set up the point tool.
        :param completed_ok: True for job success.
        :param result: None
        """
        if completed_ok:
            self.setup_point_tool()
        else:
            self.utils.no_auth()

    def setup_point_tool(self):
        """ Create the point tool. """
        if self.scrape_tool.layer_names:
            self.point_tool = PointTool(iface.mapCanvas(), self.utils, self.scrape_tool.layer_names)
            self.point_tool.download_signal.connect(self.download)
            self.previous_tool = iface.mapCanvas().mapTool()
            iface.mapCanvas().setMapTool(self.point_tool)

            self.utils.download_queue.job_complete_signal.connect(self.job_complete)
        self.update_download_list()

    @pyqtSlot()
    def job_complete(self):
        """ When any job is removed from the job queue, update the download list. """
        self.update_download_list()

    def update_download_list(self):
        """ Show the current content of the download list on this tabs listWidget. """
        self.download_listWidget.clear()
        if self.utils.download_queue.job_queue:
            for job in self.utils.download_queue.job_queue:
                if 'product' in job.job_fn.func_name:       # Minimal knowledge about what kind of job is in the queue.
                    filename = job.args[1]
                    item = tr('Product {0}').format(filename)
                    self.download_listWidget.addItem(item)
                elif 'download' == job.job_fn.func_name:     # Minimal knowledge about what kind of job is in the queue.
                    layer_name = job.args[0]
                    tile_id = job.args[1]
                    item = tr('Tile: {1}, Layer: {0}').format(layer_name, tile_id)
                    self.download_listWidget.addItem(item)
        else:
            self.download_listWidget.addItem(tr('No items queued for download.'))

    @pyqtSlot(unicode, unicode)
    def download(self, layer_name, tile_ident):
        """ Handle the user selecting an image from the context-menu on the map.
        :param layer_name: Name of the layer that has been selected from the menu
        :param tile_ident: Identifier for the tile that has been clicked on.
        """
        self.utils.scraper_download(self.scrape_tool, layer_name, tile_ident)
        self.update_download_list()
        self.prompt_textEdit.setText(tr('Extract layer and load to QGis after it\'s been downloaded.'))

        if not self.main_dlg.gui_active:
            self.main_dlg.show()
            self.main_dlg.gui_active = True

    # noinspection PyMethodMayBeStatic
    def help(self):
        """ Show help on this widget. """
        dlg = self.parent().parent().parent().parent()
        return dlg.show_help(tr('Free image help'), 'free.html')
